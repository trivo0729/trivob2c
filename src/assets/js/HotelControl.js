﻿function HotelControl () {
    /* Hotel Travellers and Class */
    $('#HotelTravellersClass').on('click', function () {
        $('#HotelTravellers').slideToggle('fast');
        /* Hide dropdown when clicking outside */
        $(document).on('click', function (event) {
            if (!$(event.target).closest(".travellers-class").length) {
                $(".travellers-dropdown").hide();
            }
            /* Hide dropdown when clicking on Done Button */
            $('.exitroom').on('click', function () {
                $('.travellers-dropdown').fadeOut(function () {
                    $(this).hide();
                });
            });
        });
    });
}

var PriviousSelected, nextSelected;
function SetRoomoccupancy(roomid, roomdesid, o, token, h, rg, rno, oc) {
    var Selected = $("#Room_"+ roomid +"_"+ roomdesid +"_"+ h +"_"+ rg +"_"+ oc +"_"+ o)[0];
    var sRooms = $(Selected).val();
    var otherRooms = $(".Room_" + token +"_"+ h +"_"+ rg +"_"+ rno);
    var noRooms = $(".RoomDetails_" + token +"_"+ oc).val();
    var remainingToBook = noRooms - sRooms
    for (var i = 0; i < otherRooms.length; i++) {
        if (remainingToBook != $(otherRooms)[i].value) {
            if ($(otherRooms)[i].value > 0 && $(otherRooms)[i] != Selected) {
                if ($(otherRooms)[i].value - remainingToBook >= 0) {
                    $(otherRooms)[i].value = remainingToBook
                    remainingToBook = $(otherRooms)[i].value - remainingToBook;
                }
                else {
                    remainingToBook = noRooms - 1
                    continue
                }
            }
            else if ($(otherRooms)[i] != Selected) {
                $(otherRooms)[i].selectedIndex = 0;
            }
        }
        else {
            if ($(otherRooms)[i].selectedIndex != 0 && remainingToBook != sRooms) {
                $(otherRooms)[i].value = remainingToBook - 1
            }
            else {
                if (PriviousSelected != null && PriviousSelected == $(otherRooms)[i]) {
                    if (remainingToBook != 0) {
                        $(otherRooms)[i].value = remainingToBook - 1;
                    }

                }
            }
        }
    }
    PriviousSelected = Selected;
    $(Selected).val(sRooms)
}

