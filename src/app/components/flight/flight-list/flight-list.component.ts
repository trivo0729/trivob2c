import {
  Component,
  OnInit,
  ChangeDetectorRef,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";
import {
  objSearch,
  Destinations,
  Seat
} from "src/app/modals/flight/flight-search.modal";
import {
  Flights,
  DomesticRoundTrip,
  ArrFilter
} from "src/app/modals/flight/flight-response.modal";
import { FlightService } from "src/app/services/flight/flight.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { Router } from "@angular/router";
import {
  NgbModal,
  ModalDismissReasons,
  NgbModalRef,
  NgbPaginationConfig
} from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { ConfirmComponent } from "../../common/confirm/confirm.component";
import { CommonService } from "src/app/services/commons/common.service";
import * as $ from "jquery";

@Component({
  selector: "app-flight-list",
  templateUrl: "./flight-list.component.html",
  styleUrls: ["./flight-list.component.css"],
  providers: [NgbPaginationConfig], // add NgbPaginationConfig to the component providers
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FlightListComponent implements OnInit {
  Seats: Seat[] = [
    { name: "All", id: "1" },
    { name: "Economy", id: "2" },
    { name: "Premium Economy", id: "3" },
    { name: "Business", id: "4" },
    { name: "Premium Business", id: "5" },
    { name: "First", id: "6" }
  ];

  // const for bottom
  JourneyType: string;
  Class: string;
  JourneyDate: any;
  singleDatePicker: boolean;
  DestinationCode: any;

  OriginCode: any;
  Destinations: Destinations[];
  TotelGuest: number;

  Onward = {
    slected: false,
    index: 0
  };

  total: any;

  Return = {
    slected: false,
    index: 0
  };
  // const for end
  Direct: boolean;
  closeResult: string;
  objSearch: objSearch;
  flights: Flights[];
  IsDomestic: boolean;
  RoundTrip: DomesticRoundTrip;
  //origin : any;
  error: boolean = false;
  errorStatus: any;
  destination: any;
  TokenId: any;
  arrRules: [];
  loading: boolean;
  Filter: ArrFilter;
  filterFlights: Flights[];
  filterRoundTrip: DomesticRoundTrip;
  image: "";
  flightlength: number;
  _response: boolean;

  TimeZone: any[];
  page: number = 1;

  pageSize = 20;
  outboundPage: number = 1;
  outpagesize = 20;

  constructor(
    private flightService: FlightService,
    private ngxService: NgxUiLoaderService,
    private router: Router,
    private modalService: NgbModal,
    private common: CommonService,
    private cd: ChangeDetectorRef,
    config: NgbPaginationConfig
  ) {
    config.size = "sm";
    config.boundaryLinks = true;
  }

  ngOnInit() {
    debugger;

    // dropPanel();
    this.flightlength = 0;
    this.objSearch = new objSearch();
    this._response = false;
    this.objSearch = this.flightService.getFlightSerachParams();
    console.log(this.objSearch);
    this.Destinations = [];
    this.RoundTrip = new DomesticRoundTrip();
    this.filterRoundTrip = new DomesticRoundTrip();
    this.RoundTrip.Inbound = [];
    this.RoundTrip.Outbound = [];

    this.IsDomestic = false;
    this.flights = [];
    this.Filter = new ArrFilter();
    // For Modify Search
    this.getflightsearchpara(this.objSearch);
    this.getTimeZone();
    // For Flight Search Response
    this.getFlightResponse(this.objSearch);
  }

  _setFilter(flights: any) {
    try {
      this.ngxService.start();
      debugger;
      if (
        this.JourneyType === "2" &&
        this.IsDomestic &&
        this.flightlength > 1
      ) {
        if (flights.Type == "I" && flights.tempFilter.length != 0)
          this.RoundTrip.Inbound = flights.tempFilter;
        else if (flights.Type == "O" && flights.tempFilter.length != 0)
          this.RoundTrip.Outbound = flights.tempFilter;
        else if (flights.Type == "O" && flights.tempFilter.length == 0)
          this.RoundTrip.Outbound = this.filterRoundTrip.Outbound;
        else if (flights.Type == "I" && flights.tempFilter.length == 0)
          this.RoundTrip.Inbound = this.filterRoundTrip.Inbound;
      } else if (
        this.JourneyType === "1" ||
        (this.JourneyType === "2" && this.IsDomestic) ||
        (this.JourneyType === "2" && !this.IsDomestic) ||
        this.JourneyType === "3"
      ) {
        if (flights.tempFilter.length != 0) this.flights = flights.tempFilter;
        else this.flights = this.filterFlights;
      }
      this.ngxService.stop();
      this.cd.detectChanges();
    } catch {}
  }
  // ngAfterViewInit() {
  //   let chk: any = document.getElementsByClassName('ltr');
  //   chk[0].style.left = '-320px';
  //   dropPanel();
  // }

  getTimeZone() {
    debugger;
    this.common.getTimeZone().subscribe((res: any) => {
      this.TimeZone = res.zones;
    });
  }

  getflightsearchpara(objSearch: any) {
    this.JourneyType = this.objSearch.JourneyType;
    this.Class = this.Seats.find(
      s => s.id === this.objSearch.Segments[0].FlightCabinClass
    ).name;

    if (this.objSearch.DirectFlight === "true") this.Direct = true;
    else this.Direct = false;

    this.objSearch.Segments.forEach((segment, i) => {
      var startDate = moment(segment.PreferredDepartureTime, "DD-MM-YYYY");

      if (this.JourneyType === "2") {
        this.singleDatePicker = false;
        var min = new Date();
        let length = this.objSearch.Segments.length - 1;
        var endDate = moment(
          this.objSearch.Segments[length].PreferredDepartureTime,
          "DD-MM-YYYY"
        );
        if (i == 0) {
          this.Destinations.push({
            Origin: segment.OriginCity,
            OriginCode: segment.Origin,
            Destination: segment.DestinationCity,
            DestinationCode: segment.Destination,
            JourneyDate: {
              start: startDate,
              end: endDate
            },
            MinDate: moment(min),
            OriginAirports: [],
            DestinationAirports: [],
            option: {
              locale: { format: "DD-MM-YYYY" },
              alwaysShowCalendars: false,
              showDropdowns: true,
              autoApply: true,
              minDate: startDate,
              singleDatePicker: false
            }
          });
        }
      } else if (this.JourneyType === "3") {
        this.singleDatePicker = true;
        let min: any;
        min = new Date();
        min = moment(min);
        if (i > 0) min = moment(segment.PreferredDepartureTime, "DD-MM-YYYY");
        this.Destinations.push({
          Origin: segment.OriginCity,
          OriginCode: segment.Origin,
          Destination: segment.DestinationCity,
          DestinationCode: segment.Destination,
          JourneyDate: {
            start: moment(startDate),
            end: moment(segment.PreferredArrivalTime, "DD-MM-YYYY")
          },
          MinDate: min,
          OriginAirports: [],
          DestinationAirports: [],
          option: {
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            autoApply: true,
            minDate: startDate,
            singleDatePicker: true
          }
        });
      } else {
        var endDate = moment(segment.PreferredArrivalTime, "DD-MM-YYYY");
        var min = new Date();
        this.Destinations.push({
          Origin: segment.OriginCity,
          OriginCode: segment.Origin,
          Destination: segment.DestinationCity,
          DestinationCode: segment.Destination,
          JourneyDate: {
            start: moment(startDate),
            end: moment(segment.PreferredArrivalTime, "DD-MM-YYYY")
          },
          MinDate: moment(min),
          OriginAirports: [],
          DestinationAirports: [],
          option: {
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            autoApply: true,
            minDate: startDate,
            singleDatePicker: true
          }
        });
      }
    });

    this.objSearch.AdultCount = this.objSearch.AdultCount;
    this.objSearch.ChildCount = this.objSearch.ChildCount;
    this.objSearch.InfantCount = this.objSearch.InfantCount;
    this.TotelGuest =
      this.objSearch.AdultCount +
      this.objSearch.ChildCount +
      this.objSearch.InfantCount;
    console.log(JSON.stringify(this.objSearch));
    this.cd.detectChanges();
  }

  getFlightResponse(ObjSearch: any) {
    debugger;
    this.ngxService.start();
    this.flightService.searchFlight(ObjSearch).subscribe(
      (res: any) => {
        this.ngxService.stop();
        this._response = true;
        if (res.retCode === 1) {
          this.IsDomestic = res.arrResult.IsDomestic;
          this.TokenId = res.arrResult.objSearch.TokenID;
          this.objSearch.TokenId = this.TokenId;
          this.flightService.SetFligthSearchParam(this.objSearch);
          this.Filter = res.arrResult.arrFilter;
          this.setflightResponse(res);
        }
        this.cd.detectChanges();
      },
      error => {
        this.hendel(error);
      }
    );
  }

  hendel(error: any) {
    this._response = true;
    this.error = true;
    this.errorStatus = error.status;
    this.cd.detectChanges();
  }

  setflightResponse(res: any) {
    debugger;
    this._response = true;
    try {
      res.arrResult.Flights.forEach((flights, i) => {
        flights.forEach(flight => {
          flight.Segments.forEach(segments => {
            segments.TotalDuration = this.calculateFlightTime(
              segments[0].Origin.DepTime,
              this.getZone(segments[0].Origin.Airport.CountryName),
              segments[segments.length - 1].Destination.ArrTime,
              this.getZone(
                segments[segments.length - 1].Destination.Airport.CountryName
              )
            );
            segments.forEach((segment, s) => {
              segment.layeOver = "";
              if (segments.length - 1 != s) {
                /* to check layover Time*/ let arr =
                  segment.Destination.ArrTime;
                let dep = segments[s + 1].Origin.DepTime;
                let timeDifference =
                  new Date(dep).getTime() - new Date(arr).getTime();
                var hours = Math.floor(timeDifference / 3600000);
                var minutes = Math.floor(
                  (timeDifference - hours * 3600000) / 60000
                );
                segment.layeOver = `${hours}h ${minutes}m`;
              }
              var hours = Math.floor(segment.Duration / 60);
              var minutes = segment.Duration % 60;
              segment.Duration = `${hours}h ${minutes}m`;
              segment.Origin.DepTime = {
                time: moment(segment.Origin.DepTime).format("LT"),
                date: moment(segment.Origin.DepTime).format("ll")
              };
              segment.Destination.ArrTime = {
                time: moment(segment.Destination.ArrTime).format("LT"),
                date: moment(segment.Destination.ArrTime).format("ll")
              };
            });
          });
        });
      });
      if (
        res.arrResult.JourneyType === "1" ||
        (res.arrResult.JourneyType === "2" && !res.arrResult.IsDomestic) ||
        res.arrResult.JourneyType === "3"
      ) {
        this.flights = res.arrResult.Flights[0];
        this.filterFlights = res.arrResult.Flights[0];
        this.flightlength = res.arrResult.Flights.length;
      } else if (
        res.arrResult.JourneyType === "2" &&
        res.arrResult.IsDomestic &&
        res.arrResult.Flights.length === 1
      ) {
        this.flights = res.arrResult.Flights[0];
        this.filterFlights = res.arrResult.Flights[0];
        this.flightlength = res.arrResult.Flights.length;
      } else {
        this.flightlength = res.arrResult.Flights.length;
        this.RoundTrip.Inbound = res.arrResult.Flights[0];
        this.filterRoundTrip.Inbound = res.arrResult.Flights[0];
        this.RoundTrip.Outbound = res.arrResult.Flights[1];
        this.filterRoundTrip.Outbound = res.arrResult.Flights[1];
      }
    } catch (error) {
      console.log(JSON.stringify(error));
    }
  }

  setOrder(type: string, event: any) {
    let flights = this.flights;
    debugger;
    this.flights = [];
    this.ngxService.start();
    if (
      this.flightlength > 1 &&
      this.JourneyType === "2" &&
      this.IsDomestic === true
    ) {
      let Inbound = this.RoundTrip.Inbound;
      let Outbound = this.RoundTrip.Outbound;
      this.RoundTrip.Inbound = [];
      this.RoundTrip.Outbound = [];
      if (event.target.value === "lower") {
        this.RoundTrip.Inbound = (
          Inbound || []
        ).sort((a: Flights, b: Flights) =>
          a.Fare.Charge.TotalPrice < b.Fare.Charge.TotalPrice ? -1 : 1
        );
        this.RoundTrip.Outbound = (
          Outbound || []
        ).sort((a: Flights, b: Flights) =>
          a.Fare.Charge.TotalPrice < b.Fare.Charge.TotalPrice ? -1 : 1
        );
      } else if (event.target.value === "higher") {
        this.RoundTrip.Inbound = (
          Inbound || []
        ).sort((a: Flights, b: Flights) =>
          a.Fare.Charge.TotalPrice > b.Fare.Charge.TotalPrice ? -1 : 1
        );
        this.RoundTrip.Outbound = (
          Outbound || []
        ).sort((a: Flights, b: Flights) =>
          a.Fare.Charge.TotalPrice > b.Fare.Charge.TotalPrice ? -1 : 1
        );
      }
    } else if (type === "Price") {
      if (event.target.value === "lower") {
        this.flights = (flights || []).sort((a: Flights, b: Flights) =>
          a.Fare.Charge.TotalPrice < b.Fare.Charge.TotalPrice ? -1 : 1
        );
      } else if (event.target.value === "higher") {
        this.flights = (flights || []).sort((a: Flights, b: Flights) =>
          a.Fare.Charge.TotalPrice > b.Fare.Charge.TotalPrice ? -1 : 1
        );
      }
    }

    this.ngxService.stop();
  }

  getZone(country: any) {
    let zone = this.TimeZone.filter(z => z.countryName === country)[0];
    return zone.gmtOffset / 3600;
  }

  getFareRules(ResultIndex: any) {
    debugger;
    let flight = this.flights.find(f => f.ResultIndex === ResultIndex);
    flight.isloading = true;
    this.flightService
      .getFareRules(ResultIndex, this.TokenId)
      .subscribe((res: any) => {
        flight.FareDetails = res.arrResult[0].FareRuleDetail;
        flight.isloading = false;
      });

    // res.arrResult.Flights.forEach((flights,j)=>{
    //   flights.forEach((flight)=>{
    //    this.ResultIndex = flight.ResultIndex;
    //    this.setFareRules(this.ResultIndex);
    //   })
    // });
  }

  calculateFlightTime(
    departureTime: any,
    departureTimezone: any,
    arrivalTime: any,
    arrivalTimezone: any
  ) {
    debugger;
    // Calculate timezone difference
    let timezoneDifference = departureTimezone - arrivalTimezone;
    // Convert timezone difference to seconds
    let timezoneDifferenceToMiliSeconds = timezoneDifference * 3600000;
    // Math.abs
    // Calculate time difference
    let timeDifference =
      new Date(arrivalTime).getTime() - new Date(departureTime).getTime();
    // Calculate real flight time
    let flightTime =
      timezoneDifference < 0
        ? timeDifference - timezoneDifferenceToMiliSeconds
        : timeDifference + timezoneDifferenceToMiliSeconds;
    // Total hours

    var hours = Math.floor(flightTime / 3600000);
    var minutes = Math.floor((flightTime - hours * 3600000) / 60000);
    return `${hours}h ${minutes}m`;
  }

  getFareRulesRDomestic(ResultIndex: any, type: any) {
    debugger;
    let flight;
    this.loading = true;

    if (type == "inbound") {
      flight = this.RoundTrip.Inbound.find(f => f.ResultIndex === ResultIndex);
      flight.isloading = true;
    } else if (type == "outbound") {
      flight = this.RoundTrip.Outbound.find(f => f.ResultIndex === ResultIndex);
      flight.isloading = true;
    }
    this.flightService
      .getFareRules(ResultIndex, this.TokenId)
      .subscribe((res: any) => {
        flight.isloading = false;

        if (res.retCode === 1) {
          flight.FareDetails = res.arrResult[0].FareRuleDetail;
          console.log(this.arrRules);
          this.cd.detectChanges();
        }
      });
  }

  onSetActiveClass(classname: string, index: any, no: any) {
    debugger;
    $("." + classname + "").removeClass("activeroundblock");
    $("." + no + "-" + index).addClass("activeroundblock");
    if (classname === "roubdbk1") {
      this.Onward.index = index;
      this.Onward.slected = true;
    } else if (classname === "roubdbk2") {
      this.Return.index = index;
      this.Return.slected = true;
    }
    this.total = (
      this.RoundTrip.Inbound[this.Onward.index].Fare.Charge.TotalPrice +
      this.RoundTrip.Outbound[this.Return.index].Fare.Charge.TotalPrice
    ).toFixed(2);
  }

  GetFareQuote(ResultIndex: any) {
    debugger;
    let Rindex = [];
    Rindex.push(ResultIndex);
    this.ngxService.start();
    this.flightService
      .GetFareQuote(Rindex, this.TokenId)
      .subscribe((res: any) => {
        this.ngxService.stop();
        if (res.retCode === 1) {
          this.router.navigate(["/flight-booking"], {
            queryParams: { Index: ResultIndex, Id: this.TokenId }
          });
        } else if (res.retCode === 2) {
          this.confirmationModel(res.error);
        }
        console.log(res);
      });
  }

  GetFareQuoteRDomestic(InboundResultIndex: any, OutboundResultIndex: any) {
    debugger;
    let OnwardIndex = [];
    OnwardIndex.push(InboundResultIndex, OutboundResultIndex);
    this.ngxService.start();
    this.flightService
      .GetFareQuote(OnwardIndex, this.TokenId)
      .subscribe((res: any) => {
        this.ngxService.stop();
        if (res.retCode === 1) {
          this.router.navigate(["/flight-booking"], {
            queryParams: {
              Index: InboundResultIndex,
              Index2: OutboundResultIndex,
              Id: this.TokenId
            }
          });
        } else if (res.retCode === 2) {
          this.confirmationModel(res.error);
        }
        console.log(res);
      });
  }

  confirmationModel(message: any) {
    let data = {
      title: "Price Change",
      message: message,
      ShowCancel: false,
      cancel: "Try another",
      ok: "Ok"
    };
    const modal: NgbModalRef = this.modalService.open(ConfirmComponent, {
      centered: true
    });
    modal.componentInstance.ModelData = data;
    modal.result.then(
      result => {
        debugger;
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    debugger;
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }

  // getTotalTime(originT: any, originZone: any, destinationT: any, destinationZone: any) {
  //   debugger
  //   // let zoneDiff = originZone - destinationZone;
  //   let milliseconds = new Date(destinationT).getTime() - new Date(originT).getTime();
  //   // let milliseconds = zoneDiff < 0 ? milliseconds - zoneDiff : milliseconds + zoneDiff;
  //   let hours = Math.floor(milliseconds / 1000 / 60 / 60);
  //   let minutes = Math.floor((milliseconds / 1000 / 60 / 60 - hours) * 60);
  //   return `${hours}h ${minutes}m`;
  // }

  // calcTime(Country: any, offset: any) {
  //   // create Date object for current location
  //   var d = new Date();
  //   // convert to msec
  //   // add local time zone offset
  //   // get UTC time in msec
  //   var utc = d.getTime() + (d.getTimezoneOffset() * 60000);

  //   // create new Date object for different city
  //   // using supplied offset
  //   var nd = new Date(utc + (3600000 * offset));

  //   // return time as a string
  //   return "The local time in " + Country + " is " + nd.toLocaleString();
  // }
}
