import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  NgForm,
  FormArray,
} from "@angular/forms";
import { FlightService } from "src/app/services/flight/flight.service";
import { ActivatedRoute, Router } from "@angular/router";
import {
  arrResult,
  PaxDetail,
  FlightDetails,
  SSR,
  Passengers,
  RateBreack,
  Breackdowns,
} from "src/app/modals/flight/FlightBooking.modal";
import * as moment from "moment";
import { GenralService } from "src/app/services/genral.service";
import {
  BookingRequest,
  ObjFlightBooking,
  Fare,
} from "src/app/modals/flight/flight-booking-request.modal";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { CommonService } from "src/app/services/commons/common.service";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { Seat } from "src/app/modals/flight/flight-search.modal";
import { UserComponent } from "../../user/user.component";
import { Userdetails } from "src/app/modals/user/userdetails";
import { MatDialog } from "@angular/material";
import { AlertService } from "src/app/services/alert.service";
declare var Razorpay: any;
declare var paypal: any;
declare function StickySidebar(): any;
declare function Showtimepicker(): any;
@Component({
  selector: "app-flight-booking",
  templateUrl: "./flight-booking.component.html",
  styleUrls: ["./flight-booking.component.css"],
})
export class FlightBookingComponent implements OnInit {
  isB2B: boolean;
  bCardPayment: boolean;
  Seats: Seat[] = [
    { name: "All", id: "1" },
    { name: "Economy", id: "2" },
    { name: "Premium Economy", id: "3" },
    { name: "Business", id: "4" },
    { name: "Premium Business", id: "5" },
    { name: "First", id: "6" },
  ];
  PassengerDetails: Passengers[];
  bookingform: FormGroup;
  formarray: FormArray;
  panelOpenState = false;
  isloaded: boolean;
  isloading: boolean = false;
  submitted: boolean;
  opens: string;
  drops: string;
  singleDatePicker: boolean;
  autoApply: boolean;
  closeOnAutoApply: boolean;
  showDropdowns: boolean;
  lockStartDate: boolean;
  finalbooking: boolean;
  MinDate: any;
  MaxDate: any;
  ResultIndex: any;
  ResultIndex2: any;
  TokenID: any;
  Passengers: PaxDetail[];
  FlightDetail: FlightDetails[];
  arrFlightDetails: arrResult;
  SSR: SSR[];
  Breackdowns: Breackdowns[];
  RateBreack: RateBreack[];
  Baggages: Baggages[];
  MealDynamic: MealDynamic[];
  Meal: Meal[];
  Country: any[] = [];
  BookingRequest: BookingRequest;
  objFlightBooking: ObjFlightBooking;
  Fare: Fare;
  step = 0;
  TimeZone: any[];
  GrandTotal: any;
  login: Userdetails = new Userdetails();
  params: any[];
  PayGateways: any[];
  rzp1: any;
  options: any;
  Doboption: any;
  Expiryoption: any;
  constructor(
    private flightService: FlightService,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef,
    private fb: FormBuilder,
    private genralService: GenralService,
    private cookie: CommonCookieService,
    public dialog: MatDialog,
    private common: CommonService,
    private ngxService: NgxUiLoaderService,
    private Alert: AlertService,
    private router: Router,
    private apiUrl: ApiUrlService
  ) {
    this.opens = "down";
    this.drops = "down";
    this.singleDatePicker = true;
  }

  ngOnInit() {
    debugger;
    //StickySidebar();
    //Showtimepicker();
    this.GrandTotal = "0";
    this.singleDatePicker = true;
    this.isB2B = false;
    this.isloaded = false;
    this.submitted = false;
    this.finalbooking = false;
    this.Passengers = [];
    this.FlightDetail = [];
    this.PayGateways = [];
    this.SSR = [];
    this.Baggages = [];
    this.MealDynamic = [];
    this.Meal = [];
    this.Breackdowns = [];
    this.PassengerDetails = [];
    this.ResultIndex = this.route.snapshot.queryParamMap.get("Index");
    this.ResultIndex2 = this.route.snapshot.queryParamMap.get("Index2");
    this.TokenID = this.route.snapshot.queryParamMap.get("Id");
    this.arrFlightDetails = new arrResult();
    this.getFlightDetails();
    this.getCountry();
    var start = new Date();
    this.MinDate = moment(start);
    this.MaxDate = moment(start);
    this.BookingRequest = new BookingRequest();
    this.BookingRequest.objFlightBooking = new ObjFlightBooking();
    this.BookingRequest.objFlightBooking.Passengers = [];
    this.Fare = new Fare();
    this.getUserType();
    this.Doboption = {
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      autoApply: true,
      MaxDate: moment(new Date()),
      singleDatePicker: true,
    };
    this.Expiryoption = {
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      autoApply: true,
      minDate: moment(new Date()),
      singleDatePicker: true,
    };
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  getFlightDetails() {
    debugger;
    this.isloading = true;
    this.ngxService.start();
    this.getTimeZone();
    let RIndex = [];
    RIndex.push(this.ResultIndex);
    if (this.ResultIndex2 != null) {
      RIndex.push(this.ResultIndex2);
    }
    this.flightService
      .GetFlightDetails(RIndex, this.TokenID)
      .subscribe((res: any) => {
        this.isloading = true;
        this.ngxService.stop();
        console.log(JSON.stringify(res));
        if (res.retCode == 1) {
          this.isloaded = true;
          this.arrFlightDetails = res.arrResult;
          this.GrandTotal = this.arrFlightDetails.TotalPrice.replace(",", "");
          this.Passengers = res.arrResult.Validate.PaxDetails;
          this.setSpecialService();
          this.setPassenger();
          console.log(JSON.stringify(this.Passengers));
          this.onBreackups(res.arrResult.Breackdowns);
          this.formInit();
          this.onDetails(res.arrResult);
          this.cd.detectChanges();
        }
      });
  }

  setPassenger() {
    this.Passengers.forEach((p) => {
      p.Baggage = [];
      this.Baggages.forEach((bag) => {
        p.Baggage.push(bag.baggage[0]);
      });
      p.MealDynamic = [];
      this.MealDynamic.forEach((meal) => {
        p.MealDynamic.push(meal.mealdynamic[0]);
      });
      if (this.Meal.length > 0) p.Meal = this.Meal[0];
      else p.Meal = new Object();
    });
  }

  getTimeZone() {
    debugger;
    this.common.getTimeZone().subscribe((res: any) => {
      this.TimeZone = res.zones;
    });
  }

  getZone(country: any) {
    let zone = this.TimeZone.filter((z) => z.countryName === country)[0];
    return zone.gmtOffset / 3600;
  }

  onDetails(arrResult: any) {
    debugger;
    arrResult.FlightDetails.forEach((flight) => {
      flight.Segments.forEach((Segments) => {
        Segments.TotalDuration = this.calculateFlightTime(
          Segments[0].Origin.DepTime,
          this.getZone(Segments[0].Origin.Airport.CountryName),
          Segments[Segments.length - 1].Destination.ArrTime,
          this.getZone(
            Segments[Segments.length - 1].Destination.Airport.CountryName
          )
        );
        Segments.forEach((segment, s) => {
          segment.layeOver = "";
          if (Segments.length - 1 != s) {
            /* to check layover Time*/ let arr = segment.Destination.ArrTime;
            let dep = Segments[s + 1].Origin.DepTime;
            let timeDifference =
              new Date(dep).getTime() - new Date(arr).getTime();
            var hours = Math.floor(timeDifference / 3600000);
            var minutes = Math.floor(
              (timeDifference - hours * 3600000) / 60000
            );
            segment.layeOver = `${hours}h ${minutes}m`;
          }
          var hours = Math.floor(segment.Duration / 60);
          var minutes = segment.Duration % 60;
          segment.Duration = `${hours}h ${minutes}m`;
          segment.Origin.DepTime = moment(segment.Origin.DepTime).format(
            "llll"
          );
          segment.Destination.ArrTime = moment(
            segment.Destination.ArrTime
          ).format("llll");
        });
      });
    });
    this.FlightDetail = this.arrFlightDetails.FlightDetails;
    this.getFareRules(0);
  }

  getUserType() {
    if (this.cookie.checkcookie("login")) {
      let data = JSON.parse(this.cookie.getcookie("login"));
      if (data.LoginDetail.UserType === "B2B") {
        this.isB2B = true;
      }
    }
  }

  calculateFlightTime(
    departureTime: any,
    departureTimezone: any,
    arrivalTime: any,
    arrivalTimezone: any
  ) {
    debugger;
    // Calculate timezone difference
    let timezoneDifference = departureTimezone - arrivalTimezone;
    // Convert timezone difference to seconds
    let timezoneDifferenceToMiliSeconds = timezoneDifference * 3600000;
    // Math.abs
    // Calculate time difference
    let timeDifference =
      new Date(arrivalTime).getTime() - new Date(departureTime).getTime();
    // Calculate real flight time
    let flightTime =
      timezoneDifference < 0
        ? timeDifference - timezoneDifferenceToMiliSeconds
        : timeDifference + timezoneDifferenceToMiliSeconds;
    // Total hours

    var hours = Math.floor(flightTime / 3600000);
    var minutes = Math.floor((flightTime - hours * 3600000) / 60000);
    return `${hours}h ${minutes}m`;
  }

  getFareRules(i: any) {
    let ResultIndex = this.FlightDetail[i].ResultIndex;
    let flight = this.FlightDetail.find((f) => f.ResultIndex === ResultIndex);
    this.flightService
      .getFareRules(ResultIndex, this.TokenID)
      .subscribe((res: any) => {
        flight.FareDetails = res.arrResult[0].FareRuleDetail;
        if (this.FlightDetail.length !== i + 1) this.getFareRules(i + 1);
      });
  }

  setSpecialService() {
    debugger;
    this.arrFlightDetails.SSR.forEach((ssr) => {
      // for baggage

      if (ssr.Baggage) {
        ssr.Baggage.forEach((baggage) => {
          this.Baggages.push({
            from: baggage[0].Origin,
            to: baggage[0].Destination,
            baggage: baggage,
          });
        });
      }
      // for Meal Dynamic

      if (ssr.MealDynamic) {
        ssr.MealDynamic.forEach((MealDynamic) => {
          this.MealDynamic.push({
            from: MealDynamic[0].Origin,
            to: MealDynamic[0].Destination,
            mealdynamic: MealDynamic,
          });
        });
      }

      // for Meal

      if (ssr.Meal) {
        this.Meal = ssr.Meal;
      }
    });
  }

  choosedDOB(event: any, i: any) {}

  getCountry() {
    this.genralService.getCountry().subscribe((res: any) => {
      this.Country = res;
    });
  }

  onBreackups(Breackdowns: any) {
    debugger;
    this.Breackdowns = [];
    Breackdowns.forEach((Breackdown) => {
      this.RateBreack = [];
      Breackdown.forEach((breackdown) => {
        this.RateBreack.push({
          Currency: breackdown.Currency,
          PassengerType: breackdown.PassengerType,
          PassengerCount: breackdown.PassengerCount,
          Fare: breackdown.Fare,
          Fees: breackdown.Fees,
          Taxes: breackdown.Taxes,
          TaxComponent: breackdown.TaxComponent,
          Total: breackdown.Total,
          SubTotal: breackdown.Total.replace(",", ""),
          Baggage: 0,
          Meal: 0,
        });
      });
      this.Breackdowns.push({ Breackdown: this.RateBreack });
    });
  }

  onBaggage(event: any, p: any, bg: any) {
    for (let i = 0; i < bg + 1; i++) {
      if (i === bg) this.Passengers[p].Baggage[i] = event;
    }
    this.onSubTotal();
  }

  ongetBaggagePrice(PassengerType: any, index: any) {
    let price = 0;
    this.Passengers.forEach((p) => {
      if (p.PaxType === PassengerType) {
        p.Baggage.forEach((baggage, b) => {
          if (
            this.arrFlightDetails.JourneyType === "2" &&
            this.arrFlightDetails.IsDomestic &&
            this.FlightDetail.length > 1
          ) {
            if (b === index) price += baggage.Price;
          } else price += baggage.Price;
        });
      }
    });
    return price;
  }

  onMealDynamic(event: any, p: any, md: any) {
    for (let i = 0; i < md + 1; i++) {
      if (i === md) this.Passengers[p].MealDynamic[i] = event;
    }
    this.onSubTotal();
  }

  ongetMealPrice(PassengerType: any, index: any) {
    let price = 0;
    this.Passengers.forEach((p) => {
      if (p.PaxType === PassengerType) {
        p.MealDynamic.forEach((meal, m) => {
          if (
            this.arrFlightDetails.JourneyType === "2" &&
            this.arrFlightDetails.IsDomestic &&
            this.FlightDetail.length > 1
          ) {
            if (m === index) price += meal.Price;
          } else price += meal.Price;
        });
      }
    });
    return price;
  }

  onSubTotal() {
    debugger;
    this.GrandTotal = 0;
    this.Breackdowns.forEach((Breackdowns, i) => {
      Breackdowns.Breackdown.forEach((breackdown, b) => {
        breackdown.SubTotal = breackdown.Total.replace(",", "");
        if (breackdown.PassengerType === "Adults") {
          breackdown.Baggage = this.ongetBaggagePrice("AD", i);
          breackdown.Meal = this.ongetMealPrice("AD", i);
          breackdown.SubTotal = parseFloat(
            parseFloat(breackdown.SubTotal) +
              breackdown.Meal +
              breackdown.Baggage
          ).toFixed(2);
        }
        if (breackdown.PassengerType === "Child") {
          breackdown.Meal = this.ongetMealPrice("CH", i);
          breackdown.Baggage = this.ongetBaggagePrice("CH", i);
          breackdown.SubTotal = parseFloat(
            parseFloat(breackdown.SubTotal) +
              breackdown.Meal +
              breackdown.Baggage
          ).toFixed(2);
        }
        this.GrandTotal += parseFloat(breackdown.SubTotal);
      });
    });
    this.GrandTotal = this.GrandTotal.toFixed(2);
  }

  onMeal(event: any, p: any) {
    debugger;
    this.Passengers[p].Meal = event;
  }

  /* #region   form validation */

  formInit() {
    this.bookingform = this.fb.group({
      email: ["", Validators.compose([Validators.email, Validators.required])],
      mobile: [
        "",
        Validators.compose([
          Validators.pattern("^[0-9]*$"),
          Validators.required,
        ]),
      ],
      country: ["", Validators.compose([Validators.required])],
      city: ["", Validators.compose([Validators.required])],
      address: ["", Validators.compose([Validators.required])],
      customer: this.fb.array([this.create_Customer(0)]),
      gstnumber: ["", Validators.compose([])],
      gstcompanyname: ["", Validators.compose([])],
      gstcompanycontactno: ["", Validators.compose([])],
      gstcompanyaddress: ["", Validators.compose([])],
      gstcompanyemail: ["", Validators.compose([])],
    });
    this.formarray = this.bookingform.get("customer") as FormArray;
    this.addCustomer();
  }

  addCustomer() {
    debugger;
    this.Passengers.forEach((passenger, i) => {
      if (i > 0) this.formarray.push(this.create_Customer(i));
    });
    console.log(this.bookingform);
  }

  create_Customer(i: any): FormGroup {
    let title;
    let name;
    let lastName;
    let dob;
    let passportno;
    let expirydate;

    if (this.Passengers[i].Title.IsRequired) title = [Validators.required];
    else title = [];
    if (this.Passengers[i].FirstName.IsRequired)
      name = [Validators.required, Validators.pattern("^[a-zA-Z ]*$")];
    else name = [];
    if (this.Passengers[i].LastName.IsRequired)
      lastName = [Validators.required, Validators.pattern("^[a-zA-Z ]*$")];
    else lastName = [];
    if (this.Passengers[i].Passport.IsRequired)
      passportno = [Validators.required];
    else passportno = [];
    if (this.Passengers[i].Passport.IsRequired)
      expirydate = [Validators.required];
    else expirydate = [];
    if (this.Passengers[i].DOB.IsRequired) dob = [Validators.required];
    else dob = [];

    return this.fb.group({
      title: ["", Validators.compose(title)],
      name: ["", Validators.compose(name)],
      lastName: ["", Validators.compose(lastName)],
      dob: ["", Validators.compose(dob)],
      passportno: ["", Validators.compose(passportno)],
      expirydate: ["", Validators.compose(expirydate)],
    });
  }

  getcustomerFormGroup(index): FormGroup {
    const formGroup = this.formarray.controls[index] as FormGroup;
    return formGroup;
  }

  get customerFormGroup() {
    return this.bookingform.get("customer") as FormArray;
  }
  /* #endregion */

  onSubmit(card: boolean) {
    debugger;
    this.bCardPayment = card;
    this.submitted = true;
    if (this.bookingform.invalid) return;
    this.checkUser();
    //this.onConfirm();
  }

  onConfirm() {
    this.isloading = true;
    this.ngxService.start();
    let ResultIndex = [];
    ResultIndex.push(this.ResultIndex);
    if (this.ResultIndex2 != null) ResultIndex.push(this.ResultIndex2);
    if (this.login.LoginDetail.UserType === "B2B") {
      this.bCardPayment = this.bCardPayment;
    }
    if (this.login.LoginDetail.UserType === "B2C") {
      this.bCardPayment = true;
    }
    this.flightService
      .ConfirmFlights(
        ResultIndex,
        this.getBaggagePrice(),
        this.getMealPrice(),
        this.TokenID,
        this.bCardPayment
      )
      .subscribe((res: any) => {
        console.log(JSON.stringify(res));
        this.ngxService.stop();
        this.isloading = false;
        if (res.retCode === 1) {
          this.finalbooking = true;

          this.onBooking();
          this.onScroll();
          if (this.bCardPayment) this.getPaymentgateway();
        }
        if (res.retCode === 2) {
          if (this.login.LoginDetail.UserType === "B2B") {
            this.Alert.succsess("Sorry", res.error, "warning", function () {});
          } else {
            this.Alert.succsess(
              "Sorry",
              "Due to some technical issue could not complete this request",
              "warning",
              function () {}
            );
          }
        }
        if (res.retCode === 3) {
          this.Alert.succsess(
            "Sorry",
            "Due to some technical issue could not complete this request",
            "warning",
            function () {}
          );
        }
      });
  }

  getBaggagePrice() {
    let price = 0;
    this.Passengers.forEach((p) => {
      p.Baggage.forEach((b) => {
        price += b.Price;
      });
    });
    return price;
  }

  getMealPrice() {
    let price = 0;
    this.Passengers.forEach((p) => {
      p.MealDynamic.forEach((m) => {
        price += m.Price;
      });
    });
    return price;
  }

  onBooking() {
    const controls = this.bookingform.controls;
    this.BookingRequest = new BookingRequest();
    this.BookingRequest.objFlightBooking = new ObjFlightBooking();
    this.BookingRequest.objFlightBooking.Passengers = [];
    this.Fare = new Fare();
    // this.Fare.BaseFare = this.FlightDetail[0].Fare.BaseFare;
    // this.Fare.Tax = this.FlightDetail[0].Fare.Tax;
    // this.Fare.YqTax = this.FlightDetail[0].Fare.YqTax;
    // this.Fare.AdditionalTxnFeeOfrd = this.FlightDetail[0].Fare.AdditionalTxnFeeOfrd;
    // this.Fare.AdditionalTxnFeePub = this.FlightDetail[0].Fare.AdditionalTxnFeePub;
    // this.Fare.OtherCharges = this.FlightDetail[0].Fare.OtherCharges;
    for (let i = 0; i < this.Passengers.length; i++) {
      let formGroup = this.formarray.controls[i] as FormGroup;
      let City = "",
        CountryCode = "",
        CountryName = "",
        Email = "",
        PassportExpiry = "",
        ContactNo = "",
        AddressLine1 = "",
        DateOfBirth = "",
        Type = 0,
        Gender = 0;

      if (i === 0) {
        City = controls.city.value;
        CountryCode = controls.country.value.Country;
        CountryName = controls.country.value.Countryname;
        Email = controls.email.value;
        ContactNo = controls.mobile.value;
        AddressLine1 = controls.address.value;
      }

      if (this.Passengers[i].PaxType === "AD") {
        Gender = 2;
        Type = 1;
      } else if (this.Passengers[i].PaxType === "CH") {
        Gender = 1;
        Type = 2;
      } else if (this.Passengers[i].PaxType === "In") {
        Gender = 1;
        Type = 3;
      }

      if (formGroup.controls["dob"].value !== "") {
        DateOfBirth = moment(formGroup.controls["dob"].value.start).format(
          "DD-MM-YYYY"
        );
      }

      if (formGroup.controls["expirydate"].value !== "") {
        PassportExpiry = moment(
          formGroup.controls["expirydate"].value.start
        ).format("DD-MM-YYYY");
      }

      let Passenger = {
        Title: formGroup.controls["title"].value,
        FirstName: formGroup.controls["name"].value,
        LastName: formGroup.controls["lastName"].value,
        PaxType: Type,
        DateOfBirth: DateOfBirth,
        Gender: Gender,
        PassportNo: formGroup.controls["passportno"].value,
        PassportExpiry: PassportExpiry,
        AddressLine1: AddressLine1,
        AddressLine2: "",
        City: City,
        CountryCode: CountryCode,
        CountryName: CountryName,
        Nationality: CountryName,
        ContactNo: ContactNo,
        Email: Email,
        IsLeadPax: this.Passengers[i].IsLeading,
        Fare: this.Fare,
        FfAirlineCode: "",
        FfNumber: "",
        GstCompanyAddress: "",
        GstCompanyContactNumber: "",
        GstCompanyName: null,
        GstNumber: null,
        GstCompanyEmail: null,
        Baggage: this.Passengers[i].Baggage,
        MealDynamic: this.Passengers[i].MealDynamic,
        Meal: this.Passengers[i].Meal,
      };

      this.BookingRequest.objFlightBooking.Passengers.push(Passenger);
    }

    // this.BookingRequest.objFlightBooking.PreferredCurrency = null;
    let ResultIndex = [];
    ResultIndex.push(this.ResultIndex);
    if (this.ResultIndex2 != null) ResultIndex.push(this.ResultIndex2);
    this.BookingRequest.objFlightBooking.ResultIndexes = ResultIndex;

    // if (this.cookie.checkcookie('login')) {
    //   let data = JSON.parse(this.cookie.getcookie('login'));
    //   if (data.LoginDetail.UserType === "B2B") {
    //     this.BookingRequest.objFlightBooking.AgentReferenceNo = data.LoginDetail.sid;
    //   }
    //   if (data.LoginDetail.UserType === "B2C") {
    //     this.BookingRequest.objFlightBooking.AgentReferenceNo = this.ServiceUrl.UserId.toString();
    //   }
    // }
    // else
    //   return

    this.BookingRequest.objFlightBooking.EndUserIp = "203.192.219.93";
    this.BookingRequest.objFlightBooking.TokenId = "";
    this.BookingRequest.objFlightBooking.TraceId = "";
    this.BookingRequest.GstNo = controls.gstnumber.value;
    this.BookingRequest.GstCompanyName = controls.gstcompanyname.value;
    this.BookingRequest.GstCompanyContact = controls.gstcompanycontactno.value;
    this.BookingRequest.GstCompanyAddress = controls.gstcompanyaddress.value;
    this.BookingRequest.GstCompanyEmail = controls.gstcompanyemail.value;
    this.BookingRequest.TokenId = this.TokenID;
    this.BookingRequest.arrParams = [];
    console.log(JSON.stringify(this.BookingRequest));
    let BookingParams = {
      objFlightBooking: this.BookingRequest.objFlightBooking,
      TokenID: this.TokenID,
      gstnumber: controls.gstnumber.value,
      gstcompanyname: controls.gstcompanyname.value,
      gstcompanycontactno: controls.gstcompanycontactno.value,
      gstcompanyaddress: controls.gstcompanyaddress.value,
      gstcompanyemail: controls.gstcompanyemail.value,
      Service: "Flight",
    };
    var current = new Date();
    current.setTime(current.getTime() + 30 * 60 * 1000);
    this.cookie.setcookie(
      "_booking_params",
      JSON.stringify(BookingParams),
      current
    );
    this.params = [];
  }

  Book() {
    if (!this.bCardPayment) this.params = [];
    this.isloading = true;
    this.ngxService.start();
    const controls = this.bookingform.controls;
    this.flightService
      .BookingFlights(
        this.BookingRequest.objFlightBooking,
        controls.gstnumber.value,
        controls.gstcompanyname.value,
        controls.gstcompanycontactno.value,
        controls.gstcompanyaddress.value,
        controls.gstcompanyemail.value,
        this.TokenID,
        this.params
      )
      .subscribe((res: any) => {
        this.isloading = false;
        this.ngxService.stop();
        if (res.retCode === 1) {
          this.flightService.SetFligthBookingParams(res);
          this.router.navigate(["/flight-confirm"]);
        }
      });
  }

  showHide() {
    debugger;
    let position: HTMLElement = document.getElementById(
      "final_booking"
    ) as HTMLElement;
    let paypalbtn: HTMLElement = document.getElementById(
      "paypal-button-container"
    ) as HTMLElement;
    if (paypalbtn) {
      if (this.bCardPayment) paypalbtn.style.display = "block";
      else {
        paypalbtn.style.display = "none";
      }
      paypalbtn.innerHTML = "";
    }

    if (this.finalbooking) {
      position.style.display = "block";
    } else {
      position.style.display = "none";
      if (paypalbtn) paypalbtn.style.display = "none";
    }
  }

  checkUser() {
    /*Check User logedIn */
    if (this.cookie.checkcookie("login")) {
      let data = JSON.parse(this.cookie.getcookie("login"));
      this.login = data;
      this.onConfirm();
    } else {
      const dialogRef = this.dialog.open(UserComponent);
      dialogRef.afterClosed().subscribe((result) => {
        if (this.cookie.checkcookie("login")) {
          let userdata = this.cookie.getcookie("login");
          //this.login = userdata;
          this.onConfirm();
        }
      });
    }
  }

  onEdit() {
    this.isloaded = true;
    this.finalbooking = false;
    this.showHide();
  }

  onScroll() {
    $("html, body").animate({ scrollTop: 200 }, 600);
  }

  onlinePay(payment_gateway: any) {
    debugger;
    if (payment_gateway === "Razorpay") this.RazorPay();
    else if (payment_gateway === "Insta Mojo") this.InstaMojo();
  }

  InstaMojo() {
    debugger;
    this.ngxService.start();
    var total = this.GrandTotal;
    total = parseFloat(total).toFixed(2).toString();
    let name = this.BookingRequest.objFlightBooking.Passengers[0].FirstName;
    let email = this.BookingRequest.objFlightBooking.Passengers[0].Email;
    let mobile = this.BookingRequest.objFlightBooking.Passengers[0].ContactNo;
    this.common
      .PaymentInstamojo(name, email, mobile, total)
      .subscribe((res: any) => {
        debugger;
        this.ngxService.stop();
        console.log(JSON.parse(res.d));
        let response = JSON.parse(res.d);
        if (response.retCode === 1) window.open(response.URL);
      });
  }

  getPaymentgateway() {
    debugger;
    this.common.getPaymentgateway().subscribe((res: any) => {
      if (res.retCode === 1) {
        this.PayGateways = res.arrResult;
        setTimeout(() => {
          this.showHide();
          this.PayPAl();
        }, 1000);
      }
    });
  }

  RazorPay() {
    const _self = this;
    var total = this.GrandTotal;
    total = (parseFloat(total) * 100).toFixed(2).toString();
    total = parseInt(total);
    this.options = {
      key: this.apiUrl.RazorpayKey,
      currency: "INR",
      amount: total, // 2000 paise = INR 20
      name: this.apiUrl.CompanyName,
      description: "Flight Booking",
      image: this.apiUrl.logo,
      handler: function (response) {
        this.params = [
          "{'Email':'" +
            this.login.LoginDetail.Email +
            "','PurchaseTocken':'" +
            response.razorpay_payment_id +
            "','PayementGetWay':'Razorpay','UserType':'" +
            this.login.LoginDetail.UserType +
            "'}",
        ];
        _self.Book();
      },
      theme: {
        color: "#FF8000",
      },
    };
    this.rzp1 = new Razorpay(this.options);
    this.rzp1.open();
  }

  PayPAl() {
    debugger;
    const _self = this;
    var total = this.GrandTotal;
    // Render the PayPal button into #paypal-button-container
    paypal
      .Buttons({
        // Set up the transaction
        createOrder: function (data, actions) {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: "" + total + "",
                },
              },
            ],
          });
        },

        // Finalize the transaction
        onApprove: function (data, actions) {
          return actions.order.capture().then(function (details) {
            // Show a success message to the buyer
            debugger;
            this.params = [
              "{'Email':'" +
                this.login.LoginDetail.Email +
                "','PurchaseTocken':'" +
                details.id +
                "','PayementGetWay':'Razorpay','UserType':'" +
                this.login.LoginDetail.UserType +
                "'}",
            ];
            _self.Book();
            alert(
              "Transaction completed by " + details.payer.name.given_name + "!"
            );
          });
        },
      })
      .render("#paypal-button-container");
  }
}

export class Baggages {
  from: string;
  to: string;
  baggage: any[];
}

export class MealDynamic {
  from: string;
  to: string;
  mealdynamic: any[];
}

export class Meal {
  from: string;
  to: string;
  meal: any[];
}
