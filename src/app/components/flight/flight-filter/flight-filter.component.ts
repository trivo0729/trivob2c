import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import {
  ArrFilter,
  Flights,
  DomesticRoundTrip
} from "src/app/modals/flight/flight-response.modal";

@Component({
  selector: "app-flight-filter",
  templateUrl: "./flight-filter.component.html",
  styleUrls: ["./flight-filter.component.css"]
})
export class FlightFilterComponent implements OnInit {
  @Input() Filter: ArrFilter;
  @Input() JourneyType: any;
  @Input() IsDomestic: boolean;
  @Input() Flightlength: number;
  @Input() flights: Flights[];
  @Input() filterRoundTrip: DomesticRoundTrip;
  @Output() valueChange = new EventEmitter();
  @Output() filterFlights: Flights[];
  tempFilter: Flights[];
  Journey: string = "";
  constructor() {}

  ngOnInit() {
    this.tempFilter = [];
    if (this.Filter.arrOutBounds.length != 0) {
      debugger;
      this.Filter.arrOutBounds[0].LaysOvers = [];
      this.Filter.arrOutBounds[0].Layover.forEach(d => {
        this.Filter.arrOutBounds[0].LaysOvers.push({ Name: d, Checked: false });
      });
    } else this.Filter.arrOutBounds = [];

    if (this.Filter.arrInbounds.length != 0) {
      debugger;
      this.Filter.arrInbounds[0].LaysOvers = [];
      this.Filter.arrInbounds[0].Layover.forEach(d => {
        this.Filter.arrInbounds[0].LaysOvers.push({ Name: d, Checked: false });
      });
    } else this.Filter.arrInbounds = [];
  }

  Commonfilter(
    bcheck: boolean,
    value: string,
    Category: string,
    JourneyType: string
  ) {
    try {
      debugger;
      console.log(this.Filter);
      switch (Category) {
        case "airline":
          this._getFlightByAirline(JourneyType, value, bcheck);
          break;
        case "depTime":
          this._getFlightByDept(JourneyType, value, bcheck);
          break;
        case "layover":
          this._getFlightByLayover(JourneyType, value, bcheck);
          break;
        case "stop":
          this._getFlightByStops(JourneyType, value, bcheck);
          break;
      }
      var Data: any = { tempFilter: this.tempFilter, Type: JourneyType };
      this.valueChange.emit(Data);
    } catch (e) {
      console.log(e);
    }
  }

  clearAll(JourneyType: string) {
    try {
      if (this.JourneyType == "I") {
        this.Filter.arrInbounds[0].LaysOvers.forEach(d => (d.Checked = false));
        this.Filter.arrInbounds[0].DepartureTime.forEach(
          d => (d.Checked = false)
        );
        this.Filter.arrInbounds[0].arrStops.forEach(d => (d.Checked = false));
        this.Filter.arrInbounds[0].Airlines.forEach(d => (d.Checked = false));
        setTimeout(d => {
          document
            .getElementById("collapseOnward")
            .classList.remove("collapse");
        }, 2000);
      } else {
        this.Filter.arrOutBounds[0].LaysOvers.forEach(d => (d.Checked = false));
        this.Filter.arrOutBounds[0].DepartureTime.forEach(
          d => (d.Checked = false)
        );
        this.Filter.arrOutBounds[0].arrStops.forEach(d => (d.Checked = false));
        this.Filter.arrOutBounds[0].Airlines.forEach(d => (d.Checked = false));
        setTimeout(d => {
          document
            .getElementById("collapseReturn")
            .classList.remove("collapse");
        }, 2000);
      }
      var Data: any = { tempFilter: [], Type: JourneyType };
      this.valueChange.emit(Data);
    } catch (e) {
      console.log(e);
    }
  }

  _getFlightByAirline(Type: string, AirlineName: string, bcheck: boolean) {
    try {
      debugger;
      var flights: Flights[] = [];
      //  var uncheck : boolean = this._ckeckFilter(Type,"airline")
      flights = this._getfilterData(Type);
      flights.forEach(d => {
        for (var s = 0; s < d.Segments.length; s++) {
          var match = d.Segments[s].find(
            a => a.Airline.AirlineName == AirlineName
          );
          if (match != undefined && match.length != 0) {
            if (!this.tempFilter.includes(d) && bcheck) {
              this.tempFilter.push(d);
              break;
            } else if (this.tempFilter.includes(d) && bcheck == false) {
              const index = this.tempFilter.indexOf(d);
              if (index > -1) {
                this.tempFilter.splice(index, 1);
              }
            }
          }
        }
      });
      console.log(this.tempFilter);
    } catch (e) {
      console.log(e);
    }
  }

  _getFlightByStops(Type: string, Stops: string, bcheck: boolean) {
    try {
      var stop = 1;
      if (Stops == "Direct") stop = 1;
      else if (Stops == "One Stops") stop = 2;
      else if (Stops == "Two Stops & More") stop = 3;
      var flights: Flights[] = [];
      flights = this._getfilterData(Type);
      flights.forEach(d => {
        var match = d.Segments[0].length;
        if (match == stop) {
          if (!this.tempFilter.includes(d) && bcheck) {
            this.tempFilter.push(d);
          } else if (this.tempFilter.includes(d) && bcheck == false) {
            const index = this.tempFilter.indexOf(d);
            if (index > -1) {
              this.tempFilter.splice(index, 1);
            }
          }
        } else if (match >= 2 && stop >= 2) {
          if (!this.tempFilter.includes(d) && bcheck) {
            this.tempFilter.push(d);
          } else if (this.tempFilter.includes(d) && bcheck == false) {
            const index = this.tempFilter.indexOf(d);
            if (index > -1) {
              this.tempFilter.splice(index, 1);
            }
          }
        }
      });
      console.log(this.tempFilter);
    } catch (e) {
      console.log(e);
    }
  }

  _getFlightByLayover(Type: string, Layover: string, bcheck: boolean) {
    try {
      var flights: Flights[] = [];
      flights = this._getfilterData(Type);
      this.flights.forEach(d => {
        for (var s = 0; s < d.Segments.length; s++) {
          var match = d.Segments[s].find(
            a => a.layeOver != "" && a.Destination.Airport.CityName == Layover
          );
          if (match != undefined && match.length != 0) {
            if (!this.tempFilter.includes(d) && bcheck) {
              this.tempFilter.push(d);
              break;
            } else if (this.tempFilter.includes(d) && bcheck == false) {
              const index = this.tempFilter.indexOf(d);
              if (index > -1) {
                this.tempFilter.splice(index, 1);
              }
            }
          }
        }
      });
      console.log(this.tempFilter);
    } catch (e) {
      console.log(e);
    }
  }

  _getFlightByDept(Type: string, DepTime: string, bcheck: boolean) {
    try {
      var flights = this._getfilterData(Type);
      flights.forEach(d => {
        var match = d.Segments[0][0].Destination.ArrTime.time;
        var startTime = this._returnTime(DepTime, true); // or 12:34
        var endTime = this._returnTime(DepTime, false); // or 1:34
        var dt = new Date(0, 0, 0, this.convertTo24Hour(match.toString()));
        var st = new Date(0, 0, 0, startTime);
        var et = new Date(1, 0, 0, endTime);
        if (
          dt.getTime() > st.getTime() &&
          dt.getTime() < et.getTime() &&
          !this.tempFilter.includes(d) &&
          bcheck
        ) {
          this.tempFilter.push(d);
        } else if (this.tempFilter.includes(d) && bcheck == false) {
          const index = this.tempFilter.indexOf(d);
          if (index > -1) {
            this.tempFilter.splice(index, 1);
          }
        }
      });
      console.log(this.tempFilter);
    } catch (e) {
      console.log(e);
    }
  }
  _returnTime(Time: string, bStart: boolean) {
    var sTime = 0.0;
    try {
      if (Time.includes("Afternoon")) {
        if (bStart) sTime = 11.0;
        else sTime = 16.0;
      } else if (Time.includes("Morning")) {
        if (bStart) sTime = 4.0;
        else sTime = 11.0;
      } else if (Time.includes("Evening")) {
        if (bStart) sTime = 16.0;
        else sTime = 21.0;
      } else if (Time.includes("Night")) {
        if (bStart) sTime = 21.0;
        else sTime = 4.0;
      }
    } catch {}
    return sTime;
  }
  convertTo24Hour(time: any) {
    debugger;
    const hours = parseInt(time.substr(0, 2));
    if (time.indexOf("AM") != -1 && hours == 12) {
      time = time.replace("12", "0");
    }
    if (time.indexOf("PM") != -1 && hours < 12) {
      time = (hours + 12).toString();
    }
    if (time.indexOf("AM") != -1 && hours < 12) {
      time = hours.toString();
    }
    return time.replace(/(AM|PM)/, "");
  }
  _getfilterData(Type: string) {
    var flights: Flights[] = [];
    if (this.JourneyType === "2" && this.IsDomestic && this.Flightlength > 1) {
      if (Type == "I") flights = this.filterRoundTrip.Inbound;
      else flights = this.filterRoundTrip.Outbound;
    } else if (
      this.JourneyType === "1" ||
      (this.JourneyType === "2" && this.IsDomestic) ||
      (this.JourneyType === "2" && !this.IsDomestic) ||
      this.JourneyType === "3"
    ) {
      flights = this.flights;
    }
    return flights;
  }

  _ckeckFilter(Type: string, sCategory: string) {
    var bcheck = false;
    var arrFilters: any;
    if (Type == "I") arrFilters = this.Filter.arrInbounds[0];
    else arrFilters = this.Filter.arrOutBounds[0];
    try {
      switch (sCategory) {
        case "airline":
          if (arrFilters.Airlines.find(d => d.Checked == true).length != 0)
            bcheck = true;
          else bcheck = false;
          break;
        case "depTime":
          if (arrFilters.depTime.find(d => d.Checked == true).length != 0)
            bcheck = true;
          else bcheck = false;
          break;
        case "layover":
          if (arrFilters.LaysOvers.find(d => d.Checked == true).length != 0)
            bcheck = true;
          else bcheck = false;
          break;
        case "stop":
          if (arrFilters.Stops.find(d => d.Checked == true).length != 0)
            bcheck = true;
          else bcheck = false;
          break;
      }
    } catch (e) {}
    return bcheck;
  }
}
