import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
} from "@angular/core";
import { FlightService } from "src/app/services/flight/flight.service";
import * as moment from "moment";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { Userdetails } from "src/app/modals/user/userdetails";
@Component({
  selector: "app-flight-ticket",
  templateUrl: "./flight-ticket.component.html",
  styleUrls: ["./flight-ticket.component.css"],
})
export class FlightTicketComponent implements OnInit {
  @Input() InvoiceDetail: any;
  @Output() bookingAction = new EventEmitter();
  TicketData: any;
  arrResrvation: any;
  AdminUser: any;
  AgentDetails: any;
  Segment: any;
  Pax: any;
  Baggage: any;
  MealDynamic: any;
  fareDetails: any;
  Airlines: any[];
  userdetails: Userdetails;
  isb2c: boolean;
  public scale = 0.6;
  constructor(
    private flightService: FlightService,
    private cd: ChangeDetectorRef,
    private cookie: CommonCookieService
  ) {}

  ngOnInit() {
    this.Segment = [];
    this.Pax = [];
    this.Baggage = [];
    this.MealDynamic = [];
    this.fareDetails = [];
    this.AgentDetails = new Object();
    this.AdminUser = new Object();
    this.arrResrvation = new Object();
    this.TicketData = new Object();
    this.Airlines = [];
    this.isb2c = false;
    this.arrResrvation = this.InvoiceDetail.arrResrvation;
    this.Pax = this.InvoiceDetail.Pax;
    this.Baggage = this.InvoiceDetail.Baggage;
    this.MealDynamic = this.InvoiceDetail.MealDynamic;
    this.fareDetails = this.InvoiceDetail.fareDetails;
    this.AdminUser = this.InvoiceDetail.AdminUser;
    this.AgentDetails = this.InvoiceDetail.AgentDetails;
    this.Segment = this.InvoiceDetail.Segment;
    this.getUserdata();
    this.bookingAction.emit("Ticket");
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let user = JSON.parse(this.cookie.getcookie("login"));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B") this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
    this.cd.detectChanges();
  }
}
