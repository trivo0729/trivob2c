import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  EventEmitter,
  Output,
} from "@angular/core";

import { Userdetails } from "src/app/modals/user/userdetails";
import {
  FlightInvoice,
  AdminUser,
  AgentDetails,
  ArrResrvation,
} from "src/app/modals/flight/flight-booking-report.modal";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { ActivatedRoute } from "@angular/router";
import { ReportService } from "src/app/services/commons/report.service";
import { FlightService } from "src/app/services/flight/flight.service";
import * as moment from "moment";
@Component({
  selector: "app-flight-invoice",
  templateUrl: "./flight-invoice.component.html",
  styleUrls: ["./flight-invoice.component.css"],
})
export class FlightInvoiceComponent implements OnInit {
  @Input() FlightInvoice: any;
  @Output() bookingAction = new EventEmitter();
  userdetails: Userdetails;
  InvoiceData: FlightInvoice;
  isb2c: boolean;
  arrResrvation: any;
  AdminUser: any;
  AgentDetails: any;
  Segment: any;
  Pax: any;
  Baggage: any;
  MealDynamic: any;
  fareDetails: any;
  BaseFare: any;
  Total: any;
  public scale = 0.6;
  constructor(
    private cookie: CommonCookieService,
    private cd: ChangeDetectorRef
  ) {}

  ngOnInit() {
    debugger;
    this.Segment = [];
    this.arrResrvation = new Object();
    this.getUserdata();
    this.setDetails();
    this.bookingAction.emit("Invoice");
  }
  setDetails() {
    debugger;
    this.arrResrvation = this.FlightInvoice.arrResrvation;
    this.AdminUser = this.FlightInvoice.AdminUser;
    this.AgentDetails = this.FlightInvoice.AgentDetails;
    this.Pax = this.FlightInvoice.Pax;
    this.Baggage = this.FlightInvoice.Baggage;
    this.MealDynamic = this.FlightInvoice.MealDynamic;
    this.fareDetails = this.FlightInvoice.fareDetails;
    this.Segment = this.FlightInvoice.Segment;
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let user = JSON.parse(this.cookie.getcookie("login"));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B") this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
    this.cd.detectChanges();
  }
}
