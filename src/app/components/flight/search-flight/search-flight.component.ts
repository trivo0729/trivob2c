import {
  Component,
  OnInit,
  ChangeDetectorRef,
  HostListener,
} from "@angular/core";
import * as moment from "moment";
import { FlightService } from "src/app/services/flight/flight.service";
import { objSearch } from "src/app/modals/flight/flight-search.modal";
import { FormControl } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { Router } from "@angular/router";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { ApiUrlService } from "src/app/services/api-url.service";
@Component({
  selector: "app-search-flight",
  templateUrl: "./search-flight.component.html",
  styleUrls: ["./search-flight.component.css"],
})
export class SearchFlightComponent implements OnInit {
  @HostListener("window:mouseup", ["$event"])
  onMouseUp() {
    if (!this.mouse_is_inside) {
      this.show = false;
      this.cd.detectChanges();
    }
  }
  show: boolean;
  mouse_is_inside: boolean = false;
  public daterange: any = {};
  singleDatePicker: boolean;
  Direct: boolean;
  Destinations: Destinations[];
  JourneyType: string;
  OriginCode: any;
  DestinationCode: any;
  userdata: any;
  invalid: boolean = false;
  frmAirports = new FormControl();
  isLoading = false;
  errorMsg: string;
  Search: objSearch;
  Guests: string;
  TotelGuest: number;
  Class: string;
  JourneyDate: any;
  Journey: any[] = [
    { name: "One Way", id: "1" },
    { name: "Round trip", id: "2" },
    { name: "Multi City", id: "3" },
  ];
  Seats: any[] = [
    { name: "All", id: "1" },
    { name: "Economy", id: "2" },
    { name: "Premium Economy", id: "3" },
    { name: "Business", id: "4" },
    { name: "Premium Business", id: "5" },
    { name: "First", id: "6" },
  ];

  constructor(
    private cd: ChangeDetectorRef,
    private flightService: FlightService,
    private toastr: ToastrService,
    private router: Router,
    private cookie: CommonCookieService,
    private ServiceUrl: ApiUrlService
  ) {}

  ngOnInit() {
    this.Destinations = [];
    this.JourneyType = "1";
    this.Class = "All";
    var start = new Date();
    this.JourneyDate = moment(start);
    this.TotelGuest = 1;
    this.Destinations.push({
      Origin: "",
      OriginCode: "",
      Destination: "",
      DestinationCode: "",
      JourneyDate: this.JourneyDate,
      MinDate: moment(start),
      OriginAirports: [],
      DestinationAirports: [],
      option: {
        locale: { format: "DD-MM-YYYY" },
        alwaysShowCalendars: false,
        showDropdowns: true,
        autoApply: true,
        minDate: moment(new Date()),
        singleDatePicker: true,
      },
    });
    this.Search = new objSearch();
    this.Search.AdultCount = 1;
    this.Search.ChildCount = 0;
    this.Search.InfantCount = 0;
    this.Guests = `${this.TotelGuest} Guests`;
    this.getUserdata();
    this.cd.detectChanges();
  }

  // see original project for full list of options
  // can also be setup using the config service to apply to multiple pickers
  public options: any = {
    locale: { format: "DD-MM-YYYY" },
    alwaysShowCalendars: false,
    showDropdowns: true,
    autoApply: true,
    minDate: moment(new Date()),
  };

  public selectedDate(value: any, datepicker?: any) {
    // this is the date  selected
    console.log(value);

    // any object can be passed to the selected event and it will be passed back here
    datepicker.start = value.start;
    datepicker.end = value.end;

    // use passed valuable to update state
    this.daterange.start = value.start;
    this.daterange.end = value.end;
    this.daterange.label = value.label;
  }

  onJourneyType(JourneyType: any) {
    debugger;
    if (JourneyType === "1") {
      var start = new Date();
      if (this.Destinations.length > 1) {
        let data = this.Destinations[0];
        this.Destinations = [];
        this.Destinations.push({
          Origin: data.Origin,
          OriginCode: "",
          Destination: data.Destination,
          DestinationCode: "",
          JourneyDate: moment(start),
          MinDate: moment(start),
          OriginAirports: [],
          DestinationAirports: [],
          option: {
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            autoApply: true,
            minDate: moment(new Date()),
            singleDatePicker: true,
          },
        });
      } else {
        this.Destinations[0].JourneyDate = moment(start);
        this.Destinations[0].option = {
          locale: { format: "DD-MM-YYYY" },
          alwaysShowCalendars: false,
          showDropdowns: true,
          autoApply: true,
          minDate: moment(new Date()),
          singleDatePicker: true,
        };
      }
    } else if (JourneyType === "2") {
      var start = new Date();
      var end = new Date();
      end.setDate(end.getDate() + 1);
      if (this.Destinations.length > 1) {
        let data = this.Destinations[0];
        this.Destinations = [];
        this.Destinations.push({
          Origin: data.Origin,
          OriginCode: data.OriginCode,
          Destination: data.Destination,
          DestinationCode: data.DestinationCode,
          JourneyDate: {
            start: moment(start),
            end: moment(end),
          },
          MinDate: moment(start),
          OriginAirports: [],
          DestinationAirports: [],
          option: {
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            autoApply: true,
            minDate: moment(start),
            singleDatePicker: false,
          },
        });
      } else {
        this.Destinations[0].JourneyDate = {
          start: moment(start),
          end: moment(end),
        };
        this.Destinations[0].option = {
          locale: { format: "DD-MM-YYYY" },
          alwaysShowCalendars: false,
          showDropdowns: true,
          autoApply: true,
          minDate: moment(start),
          singleDatePicker: false,
        };
      }
    } else if (JourneyType === "3") {
      this.singleDatePicker = true;
      let i = this.Destinations.length - 1;
      var start = new Date();
      this.Destinations[0].JourneyDate = {
        start: moment(start),
        end: moment(end),
      };
      this.Destinations.push({
        Origin: this.Destinations[i].Destination,
        OriginCode: this.Destinations[i].DestinationCode,
        Destination: "",
        DestinationCode: "",
        JourneyDate: {
          start: moment(start),
          end: moment(start),
        },
        MinDate: moment(start),
        OriginAirports: [],
        DestinationAirports: [],
        option: {
          locale: { format: "DD-MM-YYYY" },
          alwaysShowCalendars: false,
          showDropdowns: true,
          autoApply: true,
          minDate: moment(start),
          singleDatePicker: true,
        },
      });
    }
    this.cd.detectChanges();
  }

  onDirect(event: any) {
    debugger;
    this.Direct = event;
    this.cd.detectChanges();
  }

  onSwap(i: any, a: string, b: string) {
    debugger;
    this.Destinations[i].Origin = b;
    this.Destinations[i].Destination = a;
    console.log(this.Destinations);
  }

  onAddcity() {
    debugger;
    let i = this.Destinations.length - 1;
    let date = this.Destinations[i].JourneyDate;
    this.Destinations.push({
      Origin: this.Destinations[i].Destination,
      OriginCode: this.Destinations[i].DestinationCode,
      Destination: "",
      DestinationCode: "",
      JourneyDate: {
        start: moment(date.start),
        end: moment(date.start),
      },
      MinDate: moment(date.start),
      OriginAirports: [],
      DestinationAirports: [],
      option: {
        locale: { format: "DD-MM-YYYY" },
        alwaysShowCalendars: false,
        showDropdowns: true,
        autoApply: true,
        minDate: moment(date.start),
        singleDatePicker: false,
      },
    });
    this.cd.detectChanges();
  }

  choosedDate(event: any, index: any) {
    debugger;
    if (event.start !== null && this.JourneyType === "3") {
      this.Destinations.forEach((d, i) => {
        if (index < i) {
          d.MinDate = moment(event.start);
          d.JourneyDate = {
            start: moment(event.start),
            end: moment(event.end),
          };
          d.option = {
            locale: { format: "DD-MM-YYYY" },
            alwaysShowCalendars: false,
            showDropdowns: true,
            autoApply: true,
            minDate: moment(event.start),
            singleDatePicker: true,
          };
        }
      });
    }
    this.Destinations[index].JourneyDate = event;
    this.cd.detectChanges();
  }

  onRemovecity() {
    this.Destinations.pop();
    this.cd.detectChanges();
  }

  getAirports(event: any, i: any, airport: string) {
    debugger;
    let value = event.target.value;
    if (value.length < 3) return;
    this.flightService.getAirports(value).subscribe((res: any) => {
      if (res.retCode === 1) {
        switch (airport) {
          case "origin":
            this.Destinations[i].OriginAirports = res.arrResult;
            break;

          case "destination":
            this.Destinations[i].DestinationAirports = res.arrResult;
            break;
        }
      }
    });
  }

  onAirportselect(value: any, i: any, airport: string) {
    debugger;
    switch (airport) {
      case "origin":
        this.Destinations[i].OriginCode = value;
        this.OriginCode = this.Destinations[i].OriginCode;
        break;

      case "destination":
        this.Destinations[i].DestinationCode = value;
        this.DestinationCode = this.Destinations[i].DestinationCode;
        break;
    }
    this.cd.detectChanges();
  }

  onTraveller(paxType: any, action: any) {
    debugger;
    switch (action) {
      case "plus":
        // * add pax * //
        this.add(paxType);
        break;
      case "minus":
        // * remove pax * //
        this.remove(paxType);
        break;
    }
    this.TotelGuest =
      this.Search.AdultCount + this.Search.ChildCount + this.Search.InfantCount;
    this.Guests = `${this.TotelGuest} Guests`;
    this.cd.detectChanges();
  }

  add(paxType: any) {
    if (this.TotelGuest >= 9) {
      alert("Maximum of 9 travellers allowed");
      return;
    } else {
      switch (paxType) {
        case "ad":
          this.Search.AdultCount = this.Search.AdultCount + 1;
          break;
        case "ch":
          this.Search.ChildCount = this.Search.ChildCount + 1;
          break;
        case "in":
          if (this.Search.InfantCount >= this.Search.AdultCount)
            alert("Number of infants cannot be more than adults");
          else this.Search.InfantCount = this.Search.InfantCount + 1;
          break;
      }
    }
  }

  remove(paxType: any) {
    switch (paxType) {
      case "ad":
        if (this.Search.AdultCount > 1)
          this.Search.AdultCount = this.Search.AdultCount - 1;
        break;
      case "ch":
        if (this.Search.ChildCount > 0)
          this.Search.ChildCount = this.Search.ChildCount - 1;
        break;
      case "in":
        if (this.Search.InfantCount > 0)
          this.Search.InfantCount = this.Search.InfantCount - 1;
        break;
    }
  }

  onSearch() {
    debugger;
    this.checkValidation();
    if (this.invalid) return;
    this.Search.username = this.userdata.userName;
    this.Search.Password = this.userdata.password;
    this.Search.EndUserIp = "203.192.219.93";
    this.Search.OneStopFlight = "false";
    if (this.Direct) this.Search.DirectFlight = "true";
    else this.Search.DirectFlight = "false";
    this.Search.PreferredAirlines = null;
    this.Search.Sources = null;
    this.Search.TokenId = "";
    this.Search.JourneyType = this.JourneyType;
    this.Search.Segments = [];
    if (this.JourneyType !== "2") {
      this.Destinations.forEach((d) => {
        this.Search.Segments.push({
          Origin: d.OriginCode,
          OriginCity: d.Origin,
          Destination: d.DestinationCode,
          DestinationCity: d.Destination,
          FlightCabinClass: this.Seats.find((s) => s.name === this.Class).id,
          PreferredDepartureTime: moment(d.JourneyDate.start).format(
            "DD-MM-YYYY"
          ),
          PreferredArrivalTime: moment(d.JourneyDate.start).format(
            "DD-MM-YYYY"
          ),
        });
      });
    } else {
      for (let i = 0; i < 2; i++) {
        let Origin = this.Destinations[0].OriginCode;
        let OriginCity = this.Destinations[0].Origin;
        let Destination = this.Destinations[0].DestinationCode;
        let DestinationCity = this.Destinations[0].Destination;
        let start = moment(this.Destinations[0].JourneyDate.start).format(
          "DD-MM-YYYY"
        );
        let end = moment(this.Destinations[0].JourneyDate.start).format(
          "DD-MM-YYYY"
        );
        let city = "";
        let cityname = "";
        if (i !== 0) {
          city = Origin;
          Origin = Destination;
          Destination = city;
          cityname = OriginCity;
          OriginCity = DestinationCity;
          DestinationCity = cityname;
          start = moment(this.Destinations[0].JourneyDate.end).format(
            "DD-MM-YYYY"
          );
          end = moment(this.Destinations[0].JourneyDate.end).format(
            "DD-MM-YYYY"
          );
        }
        this.Search.Segments.push({
          Origin: Origin,
          OriginCity: OriginCity,
          Destination: Destination,
          DestinationCity: DestinationCity,
          FlightCabinClass: this.Seats.find((s) => s.name === this.Class).id,
          PreferredDepartureTime: start,
          PreferredArrivalTime: end,
        });
      }
    }
    ///Flight Work
    this.flightService.SetFligthSearchParam(this.Search);
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    let currentUrl = this.router.url;

    if (currentUrl == "/flight-list") {
      this.router.navigateByUrl(currentUrl).then(() => {
        this.router.navigated = false;
        this.router.navigate([this.router.url]);
      });
    } else this.router.navigate(["/flight-list"]);
    console.log(JSON.stringify(this.Search));
  }

  checkValidation() {
    this.Destinations.forEach((d, i) => {
      debugger;
      if (
        d.OriginCode === "" ||
        d.DestinationCode === "" ||
        d.JourneyDate === ""
      )
        this.invalid = true;
      else this.invalid = false;
    });
    if (this.invalid) alert("input fields cannot be blank");
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data.LoginDetail.UserType === "B2B") {
        this.userdata = {
          userName: data.LoginDetail.Email,
          password: data.LoginDetail.Password,
          parentID: this.ServiceUrl.AdminID,
        };
      }
      if (data.LoginDetail.UserType === "B2C") {
        this.userdata = {
          userName: this.ServiceUrl.UserName,
          password: this.ServiceUrl.Password,
          parentID: this.ServiceUrl.AdminID,
        };
      }
    } else {
      this.userdata = {
        userName: this.ServiceUrl.UserName,
        password: this.ServiceUrl.Password,
        parentID: this.ServiceUrl.AdminID,
      };
    }
  }

  onShow() {
    debugger;
    if (this.show) this.show = false;
    else this.show = true;
    this.cd.detectChanges();
  }
  onMouseover(event: boolean) {
    this.mouse_is_inside = event;
  }
}

export class FlightAutocomplete {
  id: string;
  value: string;
  AirportName: string;
}

export class Destinations {
  Origin: string;
  OriginCode: string;
  Destination: string;
  DestinationCode: string;
  JourneyDate: any;
  MinDate: any;
  OriginAirports: FlightAutocomplete[];
  DestinationAirports: FlightAutocomplete[];
  option: any;
}
