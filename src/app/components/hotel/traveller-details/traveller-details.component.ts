import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { arrConfirm } from "src/app/modals/hotel/booking.modal";
import { loginDetails, User } from "src/app/modals/user.model";
import { UserService } from "src/app/services/user.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { CommonService } from "src/app/services/commons/common.service";
declare var Razorpay: any;
declare var paypal: any;

@Component({
  selector: "app-traveller-details",
  templateUrl: "./traveller-details.component.html",
  styleUrls: ["./traveller-details.component.css"],
})
export class TravellerDetailsComponent implements OnInit {
  @Input() CustumerDetail: arrConfirm;
  @Input() Reservationdata: any;
  @Input() usePaymentGetway: boolean;
  @Output() editCustomer = new EventEmitter();
  @Output() Pay = new EventEmitter();
  PayGateways: any[] = [];
  show: boolean;
  login: loginDetails = new loginDetails();
  User: User = new User();
  rzp1: any;
  options: any;
  CompanyName: string;
  constructor(
    public userService: UserService,
    private cookie: CommonCookieService,
    private commonService: CommonService,
    private apiUrl: ApiUrlService
  ) {
    this.CompanyName = apiUrl.CompanyName;
  }

  ngOnInit() {
    console.log(this.CustumerDetail);
    this.PayGateways = [];
    this.ChekForPayPal();
  }

  updateStatus() {
    debugger;
    this.show = false;
    this.editCustomer.emit(this.show);
  }

  onlinePay(payment_gateway: any) {
    debugger;
    if (payment_gateway === "Razorpay") this.RazorPay();
    else if (payment_gateway === "Insta Mojo") this.InstaMojo();
  }

  InstaMojo() {
    var total = this.Reservationdata.objCharge.TotalPrice;
    total = parseFloat(total).toFixed(2).toString();
    let name = this.CustumerDetail.guestList[0].sCustomer[0].name;
    this.commonService
      .PaymentInstamojo(
        name,
        this.CustumerDetail.email,
        this.CustumerDetail.mobile,
        total
      )
      .subscribe((res: any) => {
        debugger;
        console.log(JSON.parse(res.d));
        let response = JSON.parse(res.d);
        if (response.retCode === 1) window.open(response.URL);
      });
  }

  ChekForPayPal() {
    if (this.usePaymentGetway) {
      this.getPaymentgateway();
    }
  }

  getPaymentgateway() {
    debugger;
    this.commonService.getPaymentgateway().subscribe((res: any) => {
      if (res.retCode === 1) {
        this.PayGateways = res.arrResult;
        setTimeout(() => {
          this.PayPAl();
        }, 1000);
      }
    });
  }

  RazorPay() {
    debugger;
    if (this.getUser() === "B2C" || this.getUser() === "") {
      this.RazorPayModel();
    } else if (this.getUser() === "B2B") {
      if (this.usePaymentGetway) {
        this.RazorPayModel();
      }
      if (!this.usePaymentGetway) {
        this.Book("", "");
      }
    }
  }

  RazorPayModel() {
    const _self = this;
    var total = this.Reservationdata.objCharge.TotalPrice;
    //total = (parseFloat(total) * 100).toFixed(2);
    total = (parseFloat(total) * 100).toFixed(2).toString();
    total = parseInt(total);
    this.options = {
      key: this.apiUrl.RazorpayKey,
      currency: this.Reservationdata.Currency,
      amount: total, // 2000 paise = INR 20
      name: this.CompanyName,
      description: "Hotel Booking",
      image: this.apiUrl.logo,
      handler: function (response) {
        _self.Book(response.razorpay_payment_id, "Razorpay");
      },
      theme: {
        color: "#e44049",
      },
    };
    this.rzp1 = new Razorpay(this.options);
    this.rzp1.open();
  }

  PayPAl() {
    debugger;
    const _self = this;
    var total = this.Reservationdata.objCharge.TotalPrice.toFixed(2);
    // Render the PayPal button into #paypal-button-container
    paypal
      .Buttons({
        // Set up the transaction
        createOrder: function (data, actions) {
          return actions.order.create({
            purchase_units: [
              {
                amount: {
                  value: total,
                },
              },
            ],
          });
        },

        // Finalize the transaction
        onApprove: function (data, actions) {
          return actions.order.capture().then(function (details) {
            // Show a success message to the buyer
            debugger;
            console.log(JSON.stringify(details));
            if (details.status === "COMPLETED") {
              _self.Book(details.id, "PayPal");
              alert(
                "Transaction completed by " +
                  details.payer.name.given_name +
                  "!"
              );
            } else {
              alert("Your Payment is not done! kindly try again.");
            }
          });
        },
      })
      .render("#paypal-button-container");
  }

  Book(payment_id: string, PayementGetWay: string) {
    debugger;
    var payment = {
      payment_id,
      PayementGetWay,
    };
    this.Pay.emit(payment);
  }

  getUser() {
    let userType = "";
    if (this.cookie.checkcookie("login")) {
      let userdata = JSON.parse(this.cookie.getcookie("login"));
      userType = userdata.LoginDetail.UserType;
    }
    return userType;
  }
}
