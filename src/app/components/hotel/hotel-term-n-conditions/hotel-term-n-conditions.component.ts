import { Component, OnInit } from "@angular/core";
import { ApiUrlService } from "src/app/services/api-url.service";

@Component({
  selector: "app-hotel-term-n-conditions",
  templateUrl: "./hotel-term-n-conditions.component.html",
  styleUrls: ["./hotel-term-n-conditions.component.css"],
})
export class HotelTermNConditionsComponent implements OnInit {
  domain: string;
  CompanyName: string;
  constructor(private apiUrl: ApiUrlService) {
    this.domain = apiUrl.domain;
    this.CompanyName = apiUrl.CompanyName;
  }

  ngOnInit() {}
}
