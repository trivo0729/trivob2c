import { Component, OnInit, Input, Output, EventEmitter, NgZone, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Facility, Category } from 'src/app/modals/hotel/filter.modal';
import { Hotels } from 'src/app/modals/hotel/hotel.modal';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
//import { GooglePlaceDirective } from "src/ngx-google-places-autocomplete.directive";
//import { ComponentRestrictions } from "src/objects/options/componentRestrictions";
// import { Address } from "src/objects/address";
import { HotelService } from 'src/app/services/hotel/hotel.service';
declare const google: any;
declare function _Price_Range(min: any, max: any, currency: string): any;
@Component({
  selector: 'app-hotel-filter',
  templateUrl: './hotel-filter.component.html',
  styleUrls: ['./hotel-filter.component.css']
})
export class HotelFilterComponent implements OnInit {
  @Input() Category: Category[];
  @Input() Facility: Facility[];
  @Input() Location: any;
  @Input() min: any;
  @Input() max: any;
  @Input() Currency: string;
  @Input() HotelData: Hotels[];
  @Input() search: any;
  @Output() filterHotel = new EventEmitter();
  _tempCategory: Category[];
  _tempFacility: Facility[];
  Price: Array<any>;
  Hotel: Hotels[];
  filterHotels: Hotels[];
  _hotels: string[];
  filtered: Observable<string[]>;
  hotelControl = new FormControl();
  _filtered: boolean;
  _filteredType: string;

  hotelName: string;
  nearBy: string;

  //@ViewChild('places') places: GooglePlaceDirective;

  constructor(private cd: ChangeDetectorRef,
    private zone: NgZone, private hotelService: HotelService, ) { }

  ngOnInit() {
    debugger;
    this._filtered = false;
    this.Hotel = this.HotelData;
    this._tempCategory = [];
    this._tempFacility = [];
    this.updateCategory();
    //_Price_Range(this.min, this.max, this.Currency);
    const self = this;
    this.zone.run(() => {
      // $('.price_range').on('change', function (data) {
      //   self._Price(data, 'p');
      // });
    });
    this.setHotels(this.HotelData);
  }

  ngAfterViewInit() {
    //this.getRestictions(this.search)

  }

  updateCategory() {
    this.Category.forEach(s => {
      s.Checked = false;
    });
    this.Category = (this.Category || []).sort((a: Category, b: Category) => a.Name > b.Name ? -1 : 1)
  }

  _Price(data: any, type: string) {
    //if (!this._filtered)
    this.PriceFilter(data, type);
  }


  Commonfilter(checked: any, name: any, type: any) {
    debugger;
    this.filterHotels = [];
    if (!this._filtered) {
      this._filtered = true
      this._filteredType = type;
    }

    this.CategoryFilter(checked, name, type, this.HotelData);

    this.FacilityFilter(checked, name, type, this.HotelData);



    if (this.filterHotels.length != 0) {
      this.updateFilterParams(this.filterHotels, type);
      this.filterHotel.emit(this.filterHotels);
    }

    else {
      this._filtered = false;
      this.updateAllFilterParams(this.Hotel, type);
      this.filterHotel.emit(this.Hotel)
    }
  }


  _sFilter(type: any, ftype: any) {
    this.HotelData.forEach(hotel => {
      this.Category.forEach(s => {
        if (hotel.Category === s.Name && s.Checked) {
          if (this.filterHotels.indexOf(hotel) == -1)
            this.filterHotels.push(hotel)
        }
      });
    });

    if (type != 's') {
      this._tempCategory = this.Category;
      this.Category = [];
      this.filterHotels.forEach(h => {
        if (this.Category.filter(d => d.Name == h.Category).length == 0) {
          var _s = this._tempCategory.filter(s => s.Name == h.Category)[0];
          this.Category.push({ Name: h.Category, Count: 1, HotelCode: h.HotelId, Supplier: h.Supplier, Checked: _s.Checked });
        }
        else {
          this.Category.filter(d => d.Name == h.Category).forEach(r => {
            r.Count = r.Count + 1;
          });
        }
      });
      this.Category = (this.Category || []).sort((a: Category, b: Category) => a.Name > b.Name ? -1 : 1)
    }
  }


  CategoryFilter(checked: any, name: any, type: any, data: Hotels[]) {
    if (type == 's') {
      this.Category.forEach(c => {
        if (c.Name === name)
          c.Checked = checked
      });
    }
    data.forEach(hotel => {
      this.Category.forEach(s => {
        if (hotel.Category === s.Name && s.Checked) {
          if (this.filterHotels.indexOf(hotel) == -1)
            this.filterHotels.push(hotel)
        }
      });
    });
  }

  FacilityFilter(checked: any, name: any, type: any, data: Hotels[]) {

    if (type == 'f') {
      this.Facility.forEach(c => {
        if (c.icon == name)
          c.Checked = checked
      });
    }

    data.forEach(hotel => {
      this.Facility.forEach((facility: { icon: string; Checked: any; }) => {
        if (hotel.Facility.filter(f => f.icon == facility.icon).length != 0 && facility.Checked) {
          if (this.filterHotels.indexOf(hotel) == -1)
            this.filterHotels.push(hotel)
        }
      });
    });
  }

  hotelNamefilter(name: string, type: any) {
    debugger;
    this._filtered = true;
    this._filteredType = type;
    this.filterHotels = this.HotelData.filter(h => h.HotelName.toLowerCase().includes(name));
    this.updateFilterParams(this.filterHotels, type);
    this.filterHotel.emit(this.filterHotels);
    // this.filterHotels = this.HotelData.filter(h => h.HotelName === name);
    // if (this.filterHotels.length != 0) {
    //   this.filterHotels = (this.filterHotels || []).sort((a: Hotels, b: Hotels) => a.Charge.TotalPrice < b.Charge.TotalPrice ? -1 : 1);
    //   //this.HotelData = this.filterHotels;
    //   this.updateFilterParams(this.filterHotels, type);
    //   this.filterHotel.emit(this.filterHotels);
    // }

    // else {
    //   this.Hotel = (this.Hotel || []).sort((a: Hotels, b: Hotels) => a.Charge.TotalPrice < b.Charge.TotalPrice ? -1 : 1);
    //   this.HotelData = this.Hotel;
    //   this.updateFilterParams(this.Hotel, type);
    //   this.filterHotel.emit(this.Hotel)
    // }
  }

  Clearhotel(name: string, type: any) {
    this.hotelName = name;
    this.hotelNamefilter(this.hotelName, type)
  }


  PriceFilter(data: any, type: any) {
    debugger;
    var TempPrice = data.currentTarget.value.split(';');
    this.min = parseFloat(TempPrice[0]);
    this.max = parseFloat(TempPrice[1]);
    this.filterHotels = [];
    this.HotelData.forEach(hotel => {
      if (parseFloat(hotel.Charge.TotalPrice.toString()) >= this.min && parseFloat(hotel.Charge.TotalPrice.toString()) <= this.max) {
        this.filterHotels.push(hotel);
      }
    });
    if (this.filterHotels.length > 0) {
      this.filterHotel.emit(this.filterHotels);
      this.updateFilterParams(this.filterHotels, type);
    }
  }

  updateFilterParams(data: Hotels[], type: any) {
    debugger;
    /* for price update*/
    if (type != 'p') {
      this.Price = [];
      data.forEach(h => {
        this.Price.push(h.Charge.TotalPrice);
      });
      this.max = this.Price.reduce((a, b) => Math.max(a, b)).toFixed(2);
      this.min = this.Price.reduce((a, b) => Math.min(a, b)).toFixed(2);
      this.cd.detectChanges();
      //this.updateRange(this.min, this.max, this.Currency)
    }

    /* for star categoray update*/
    if (type != 's') {
      this._tempCategory = this.Category;
      this.Category = [];
      data.forEach(h => {
        if (this.Category.filter(d => d.Name == h.Category).length == 0) {
          //var _s = this._tempCategory.filter(s => s.Name == h.Category)[0];
          this.Category.push({ Name: h.Category, Count: 1, HotelCode: h.HotelId, Supplier: h.Supplier, Checked: false });
        }
        else {
          this.Category.filter(d => d.Name == h.Category).forEach(r => {
            r.Count = r.Count + 1;
          });
        }
      });
      this.Category = (this.Category || []).sort((a: Category, b: Category) => a.Name > b.Name ? -1 : 1)
    }

    /* for star facility update*/
    if (type != 'f') {
      this.Facility = [];
      data.forEach(h => {
        h.Facility.forEach(facility => {
          if (this.Facility.filter(d => d.icon == facility.icon).length == 0) {
            this.Facility.push({ Name: facility.Name, Count: 1, Priority: facility.Priority, icon: facility.icon, toolip: facility.toolip, Checked: facility.Checked });
          }
          else {
            this.Facility.filter(d => d.icon == facility.icon).forEach(r => {
              r.Count = r.Count + 1;
            });
          }
        });
      });
    }

    /* for hotel name update*/
    if (type != 'h') {
      this.setHotels(data);
    }

    //this.HotelData = data;
  }


  updateAllFilterParams(data: Hotels[], type: any) {

    this.Price = [];
    data.forEach(h => {
      this.Price.push(h.Charge.TotalPrice);
    });
    this.max = this.Price.reduce((a, b) => Math.max(a, b)).toFixed(2);
    this.min = this.Price.reduce((a, b) => Math.min(a, b)).toFixed(2);
    this.cd.detectChanges();
    //this.updateRange(this.min, this.max, this.Currency)

    this.Category = [];
    data.forEach(h => {
      if (this.Category.filter(d => d.Name == h.Category).length == 0) {
        this.Category.push({ Name: h.Category, Count: 1, HotelCode: h.HotelId, Supplier: h.Supplier, Checked: false });
      }
      else {
        this.Category.filter(d => d.Name == h.Category).forEach(r => {
          r.Count = r.Count + 1;
        });
      }
    });
    this.Category = (this.Category || []).sort((a: Category, b: Category) => a.Name > b.Name ? -1 : 1)

    this.Facility = [];
    data.forEach(h => {
      h.Facility.forEach(facility => {
        if (this.Facility.filter(d => d.icon == facility.icon).length == 0) {
          this.Facility.push({ Name: facility.Name, Count: 1, Priority: facility.Priority, icon: facility.icon, toolip: facility.toolip, Checked: facility.Checked });
        }
        else {
          this.Facility.filter(d => d.icon == facility.icon).forEach(r => {
            r.Count = r.Count + 1;
          });
        }
      });
    });
    this.setHotels(data);
    this.HotelData = data;
  }

  updateRange(min: string, max: string, currency: string) {
    // Save slider instance to var
    var slider =""// $(".price_range").data("ionRangeSlider");
    // Change slider, by calling it's update method
    // slider.update({
    //   type: "double",
    //   //grid: true,
    //   min: min,
    //   max: max,
    //   from: min,
    //   to: max,
    //   prefix: ' ' + currency + ' '
    // });
  }

  setHotels(data) {
    this._hotels = [];
    data.forEach(h => {
      this._hotels.push(h.HotelName)
    });

    this.filtered = this.hotelControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this._hotels.filter(
      option => option.toLowerCase().includes(filterValue)
    );
  }

  // NearByfilter(address: Address, type: any) {
  //   this.filterHotels = [];
  //   const lat = address.geometry.location.lat();
  //   const lng = address.geometry.location.lng();
  //   this.hotelService.setSelectedlocation(address);
  //   this.HotelData.forEach(hotel => {
  //     hotel.Distance = this.hotelService.GetDistance(hotel.Latitude, hotel.Langitude, lat, lng, 'K');
  //     this.filterHotels.push(hotel);
  //   });
  //   this.filterHotels = (this.filterHotels || []).sort((a: Hotels, b: Hotels) => a.Distance < b.Distance ? -1 : 1);
  //   if (this.filterHotels.length != 0) {
  //     this.HotelData = this.filterHotels;
  //     this.updateFilterParams(this.filterHotels, type);
  //     this.filterHotel.emit(this.filterHotels);
  //   }

  //   else {
  //     this.Hotel = (this.Hotel || []).sort((a: Hotels, b: Hotels) => a.Charge.TotalPrice < b.Charge.TotalPrice ? -1 : 1);
  //     this.HotelData = this.Hotel;
  //     this.updateFilterParams(this.Hotel, type);
  //     this.filterHotel.emit(this.Hotel)
  //   }
  // }


  // public onChange(address: Address, type: any) {
  //   debugger;
  //   this.NearByfilter(address, type);
  // }

  getRestictions(address: any) {
    const _self = this;
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address.City + ', ' + address.Country }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var _bounds = results[0].geometry.bounds;
        for (var i = 0; i < results[0].address_components.length; i++) {
          if (results[0].address_components[i].long_name == address.Country) {
            var _country = results[0].address_components[i].short_name;
            //_self.changeConfig(_bounds, _country);
          }
        }
      }
    });
  }

  // public changeConfig(bounds: any, country: any) {
  //   this.places.options.bounds = bounds;
  //   this.places.options.componentRestrictions = new ComponentRestrictions({
  //     country: country
  //   });
  //   this.places.reset();
  // }
}
