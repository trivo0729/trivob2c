import { Component, OnInit, ViewChild } from "@angular/core";
import { HotelService } from "src/app/services/hotel/hotel.service";
import { BookingDetailsComponent } from "../../user/booking-details/booking-details.component";
import { CommonCookieService } from "../../../services/commons/common-cookie.service";
import { Router } from "@angular/router";
import { ApiUrlService } from "../../../services/api-url.service";

@Component({
  selector: "app-hotel-confirmation",
  templateUrl: "./hotel-confirmation.component.html",
  styleUrls: ["./hotel-confirmation.component.css"],
})
export class HotelConfirmationComponent implements OnInit {
  bookingDetail: any;
  userId: any;
  @ViewChild(BookingDetailsComponent) bDetails: BookingDetailsComponent;

  constructor(
    private hotelService: HotelService,
    private router: Router,
    private commonobj: ApiUrlService,
    private cookie: CommonCookieService
  ) {}

  ngOnInit() {
    this.bookingDetail = this.hotelService.getBooking();
    this.userId = this.GetLoginDetails();
    this.bDetails.getBookingDetails(
      this.userId,
      this.bookingDetail.ReservationId
    );
    console.log(this.bookingDetail);
  }

  GetLoginDetails() {
    if (this.cookie.checkcookie("login")) {
      let login = JSON.parse(this.cookie.getcookie("login"));
      if (login.LoginDetail.UserType === "B2B") {
        return login.LoginDetail.sid;
      } else {
        return this.commonobj.UserId;
      }
    } else {
      this.router.navigate["/home"];
    }
  }

  ngAfterViewInit() {
    // let position: HTMLElement = document.getElementById(
    //   "position"
    // ) as HTMLElement;
    // position.style.display = "none";
    // let mainbooking: HTMLElement = document.getElementById(
    //   "main-booking"
    // ) as HTMLElement;
    // mainbooking.style.paddingRight = "0px";
    // mainbooking.style.paddingLeft = "0px";
  }
}
