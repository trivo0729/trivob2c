import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { objTraveller, Childs } from 'src/app/modals/hotel/search.modal';
declare function HotelControl(): any;

@Component({
  selector: 'app-traveller',
  templateUrl: './traveller.component.html',
  styleUrls: ['./traveller.component.css']
})
export class TravellerComponent implements OnInit {
  @Input() Traveller: any;
  @Output() setGuests = new EventEmitter();
  TotalGuest: number;
  public Ages: Array<any>;
  Childs: Childs[];
  plus: string = 'plus';
  minus: string = 'minus';
  constructor(private cd: ChangeDetectorRef) {
    this.TotalGuest = 0;
  }

  ngOnInit() {
    debugger;
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    HotelControl();
    this.getTravellers();
  }


  Addrooms() {
    this.Childs = [];
    this.Traveller.push({ adults: 1, childs: this.Childs });
    this.getTravellers();
  }

  RemoveRoom() {
    this.Traveller.pop();
    this.getTravellers();
  }

  public getTravellers() {
    this.TotalGuest = 0;
    this.Traveller.forEach(element => {
      this.TotalGuest += element.adults;
      this.TotalGuest += element.childs.length;
    });
    this.setGuests.emit(this.Traveller.length + ' Room, ' + this.TotalGuest + ' Guests')
  }

  public changeTravellers(count: any, action: any, paxtype: string) {
    debugger;
    for (let i = 0; i < this.Traveller.length; i++) {
      if (i == count) {
        if (paxtype == 'ad') {
          if (action == 'plus') {
            if (this.Traveller[i].adults < 4) {
              this.Traveller[i].adults = this.Traveller[i].adults + 1
            }
          }
          if (action == 'minus') {
            if (this.Traveller[i].adults > 1) {
              this.Traveller[i].adults = this.Traveller[i].adults - 1
            }
          }
        }
        if (paxtype == 'ch') {
          if (action == 'plus') {
            if (this.Traveller[i].childs.length < 4) {
              this.Traveller[i].childs.push({ count: 1, childAge: 2, ages: this.Ages });
            }
          }
          if (action == 'minus') {
            if (this.Traveller[i].childs.length != 0) {
              this.Traveller[i].childs.pop();
            }
          }
        }
      }
    }
    this.cd.detectChanges();
    this.getTravellers();
  }

}
