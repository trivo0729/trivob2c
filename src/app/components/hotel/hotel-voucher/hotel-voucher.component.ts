import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  Output,
  ChangeDetectorRef,
} from "@angular/core";
import { ReportService } from "src/app/services/commons/report.service";
import {
  ArrResrvation,
  AgentDetails,
  GuestList,
  Rooms,
  HotelDetails,
} from "src/app/modals/hotel/voucher.modal";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { Userdetails } from "src/app/modals/user/userdetails";

@Component({
  selector: "app-hotel-voucher",
  templateUrl: "./hotel-voucher.component.html",
  styleUrls: ["./hotel-voucher.component.css"],
})
export class HotelVoucherComponent implements OnInit {
  @Input() UserId: any;
  @Input() ReservationID: any;
  @Output() bookingAction = new EventEmitter();
  public scale = 0.6;
  Resrvation: ArrResrvation;
  AgentDetails: AgentDetails;
  Hotel: HotelDetails;
  GuestList: GuestList[];
  Rooms: Rooms[];
  CancellationPolicy: any[];
  isb2c: boolean;
  userdetails: Userdetails;

  constructor(
    private reportService: ReportService,
    private cd: ChangeDetectorRef,
    private cookie: CommonCookieService
  ) {}

  ngOnInit() {
    debugger;
    this.userdetails = new Userdetails();
    this.Resrvation = new ArrResrvation();
    this.AgentDetails = new AgentDetails();
    this.Hotel = new HotelDetails();
    this.GuestList = [];
    this.Rooms = [];
    this.CancellationPolicy = [];
    this.reportService
      .GetHotelVoucher(this.UserId, this.ReservationID)
      .subscribe(
        (res: any) => {
          //res = decompressResponse(res);
          if (res.retCode === 1) {
            this.Hotel = res.dtHotelAdd;
            this.Resrvation = res.arrResrvation;
            this.AgentDetails = res.AgentDetails;
            this.GuestList = res.arrPaxes;
            this.Rooms = res.arrRooms;
            this.CancellationPolicy = res.CancellationPolicy;
            this.bookingAction.emit("Voucher");
          }
          console.log(res);
        },
        (error) => {
          console.log(error);
        }
      );
    this.getUserdata();
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let user = JSON.parse(this.cookie.getcookie("login"));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B") this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
    this.cd.detectChanges();
  }
}
