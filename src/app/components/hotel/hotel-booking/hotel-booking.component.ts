import { Component, OnInit } from '@angular/core';
import { HotelService } from 'src/app/services/hotel/hotel.service';
import * as moment from 'moment';
import { sCustomer, guestList, _Customers, _rooms, arrConfirm, arrParams } from 'src/app/modals/hotel/booking.modal';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { HotelTermNConditionsComponent } from '../hotel-term-n-conditions/hotel-term-n-conditions.component';
import { AlertService } from 'src/app/services/alert.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import Swal from 'sweetalert2';
import { UserService } from 'src/app/services/user.service';
import { UserComponent } from '../../user/user.component';
import { Router } from '@angular/router';
import { Userdetails } from 'src/app/modals/user/userdetails';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';
declare function StickySidebar(): any;


@Component({
  selector: 'app-hotel-booking',
  templateUrl: './hotel-booking.component.html',
  styleUrls: ['./hotel-booking.component.css']
})
export class HotelBookingComponent implements OnInit {
  paypal: any;
  isB2B: boolean;
  r: number = 0;
  Data: any;
  checkin: any;
  checkout: any;
  guestList: guestList[];
  rooms: _rooms[];
  sCustomer: sCustomer[];
  _Customers: _Customers[]
  AdultTitle: any;
  ChildTitle: any;
  remark: string;
  company: string;
  mobile: any;
  email: string;
  public form: FormGroup;
  public _customerList: FormArray;
  submitted: boolean;
  termsncondition: boolean;
  arrConfirm: arrConfirm;
  selectedadult: string;
  selectechild: string;
  showDetails: boolean;
  login: Userdetails = new Userdetails();
  ConfirmationID: string;
  arrParams: any;
  userParams: any;
  usePaymentGetway: boolean;
  userDetails: Userdetails;

  constructor(private hotelService: HotelService,
    private fb: FormBuilder, public dialog: MatDialog,
    private Alert: AlertService,
    private cookie: CommonCookieService,
    public userService: UserService,
    private router: Router,
    private ngxService: NgxUiLoaderService
  ) { }

  ngOnInit() {
    this.isB2B = false;
    this.usePaymentGetway = false;
    this.termsncondition = false;
    this.showDetails = false;
    this.arrConfirm = new arrConfirm();
    StickySidebar();
    this.submitted = false;
    this.remark = '';
    this.company = '';
    this.mobile = '';
    this.email = '';
    this.rooms = [];
    this.AdultTitle = ['Mr', 'Mrs', 'Miss'];
    this.ChildTitle = ['Master', 'Miss'];
    this.selectedadult = 'Mr';
    this.selectechild = 'Master';
    const _Details = this.hotelService.getBlockRoomDetails();
    console.log(_Details);
    this.Data = _Details.arrReservation;
    this.form = this.fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required])],
      company: [],
      remark: [],
      phone: ['', Validators.compose([Validators.pattern("^[0-9]*$"), Validators.required])],
      customer: this.fb.array([this.create_Customer(1)])
    });
    this._customerList = this.form.get('customer') as FormArray;
    this.setDates();
    this.setTravellers();
    this.getUserType();
  }




  getUserType() {
    if (this.cookie.checkcookie('login')) {
      this.login = JSON.parse(this.cookie.getcookie('login'));
      if (this.login.LoginDetail.UserType === 'B2B') {
        this.isB2B = true;
      }
    }
  }


  setDates() {
    this.checkin = {
      day: moment(this.Data.Search.Checkin, 'DD-MM-YYYY').format('ddd'),
      month: moment(this.Data.Search.Checkin, 'DD-MM-YYYY').format('MMM'),
      date: moment(this.Data.Search.Checkin, 'DD-MM-YYYY').format('DD'),
    }
    this.checkout = {
      day: moment(this.Data.Search.Checkout, 'DD-MM-YYYY').format('ddd'),
      month: moment(this.Data.Search.Checkout, 'DD-MM-YYYY').format('MMM'),
      date: moment(this.Data.Search.Checkout, 'DD-MM-YYYY').format('DD'),
    }
  }

  setTravellers() {
    this.Data.Search.Rooms.forEach((list, index) => {
      this._Customers = [];
      for (let a = 0; a < list.AdultCount; a++) {
        this._Customers.push({ age: 35, type: 1, name: '', lastName: '', title: '', roomNo: list.RoomCount, srno: this.r });
        this.r++;
        if (index === 0 && a === 0) {
          continue;
        }
        else
          this.addCustomer(1);
      }
      if (list.ChildCount !== 0) {
        for (let c = 0; c < list.ChildCount; c++) {
          this._Customers.push({ age: list.ChildAges[c], type: 2, name: '', lastName: '', title: '', roomNo: list.RoomCount, srno: this.r });
          this.r++;
          this.addCustomer(2);
        }
      }
      this.rooms.push({ id: this.Data.listRate[index].RoomTypeId, roomNumber: list.RoomCount, sCustomer: this._Customers });

    });
  }

  create_Customer(type): FormGroup {
    if (type === 1) {
      return this.fb.group({
        title: ['', Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
        lastName: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      });
    }

    else if (type === 2) {
      return this.fb.group({
        title: ['', Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z ]*$')])],
      });
    }
  }

  addCustomer(type) {
    this._customerList.push(this.create_Customer(type));
  }

  getcustomerFormGroup(index): FormGroup {
    const formGroup = this._customerList.controls[index] as FormGroup;
    return formGroup;
  }

  get customerFormGroup() {
    return this.form.get('customer') as FormArray;
  }

  public Book(payType: any): void {
    this.usePaymentGetway = payType;
    console.log(this.usePaymentGetway);
    debugger
    this.submitted = true;
    // stop here if form is invalid
    if (this.form.invalid && !this.termsncondition) {
      return;
    }
    else if (this.form.valid && this.termsncondition) {
      this.checkUser();
    }
  }

  termCondition() {
    const dialogRef = this.dialog.open(HotelTermNConditionsComponent, {
      height: 'auto',
      width: 'auto'
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  changeTermsStatus() {
    if (this.termsncondition === false) {
      this.termsncondition = true;
    } else {
      this.termsncondition = false;
    }
  }

  makeConfirmation() {
    debugger
    this.ngxService.start();
    //const blockData = this.hotelService.getarrBlock();
    const blockData = this.hotelService.getBlockRoomDetails();
    this.guestList = [];
    this.rooms.forEach((r, index) => {
      this.sCustomer = [];
      for (let c of r.sCustomer) {
        this.sCustomer.push({ age: c.age, type: c.type, name: c.name, lastName: c.lastName, title: c.title, roomNo: c.roomNo })
      }
      this.guestList.push({ id: r.id, roomNumber: r.roomNumber, sCustomer: this.sCustomer });
    });
    this.arrConfirm = new arrConfirm();
    this.arrConfirm.bookingDetails = blockData.arrReservation.BookingDetails;
    this.arrConfirm.tokenId = blockData.arrReservation.TokenID;
    this.arrConfirm.guestList = this.guestList;
    this.arrConfirm.resultIndex = blockData.arrReservation.ResultIndex;
    this.arrConfirm.groupIndex = blockData.arrReservation.GroupIndex;
    this.arrConfirm.remark = this.remark;
    this.arrConfirm.company = this.company;
    this.arrConfirm.mobile = this.mobile;
    this.arrConfirm.email = this.email;
    if (this.login.LoginDetail.UserType === "B2B") {
      this.arrConfirm.bCardPayment = this.usePaymentGetway;
    }
    if (this.login.LoginDetail.UserType === "B2C") {
      this.arrConfirm.bCardPayment = true;
    }
    console.log(JSON.stringify(this.arrConfirm));
    this.hotelService.BookingConfirmation(this.arrConfirm).subscribe((res: any) => {
      this.ngxService.stop();
      console.log(res);
      if (res.retCode === 1) {
        this.ConfirmationID = res.ConfirmationID;
        let BookingParams = {
          ConfirmationID: this.ConfirmationID,
          TokenID: this.arrConfirm.tokenId,
          Service: 'Hotel'
        }
        var current = new Date();
        current.setTime(current.getTime() + (30 * 60 * 1000));
        this.cookie.setcookie('_booking_params', JSON.stringify(BookingParams), current);
        this.showDetails = true;
        this.onScroll();
      }

      if (res.retCode === 2) {
        this.htmlpopup('room price get changed', 'warning',
          '<p>' +
          '<span>Your Old Booking Price was ' + this.Data.Currency + '. ' + this.Data.objCharge.TotalPrice.toFixed(2) + '</span><br/>' +
          '<span>New Booking Price is ' + this.Data.Currency + '. ' + this.Data.objCharge.TotalPrice.toFixed(2) + '</span><br/>' +
          '<span>Please Click Ok to Continue or Click Cancel.</span>' +
          // "<span>Please Click Ok to Continue or Select another Room Option.</span>" +
          '</p>');
      }
      if (res.retCode === 3) {
        this.Alert.succsess('Oops', res.Massage, 'warning', function () {
          window.location.href = 'hotel';
        })
      }
      if (res.retCode === 4) {
        if (this.login.LoginDetail.UserType === "B2B") {
          this.Alert.succsess('Sorry', res.error, 'warning', function () {
          });
        }
        else {
          this.Alert.succsess('Sorry', 'Due to some technical issue could not complete this request', 'warning', function () {
          });
        }
      }
      if (res.retCode === 5) {
        this.Alert.succsess('Sorry', 'Due to some technical issue could not complete this request', 'warning', function () {
        });
      }
      if (res.retCode === 0) {
        this.Alert.succsess('Sorry', 'Room is not available', 'warning', function () {
        });
      }
    });
  }

  onScroll() {
    $("html, body").animate({ scrollTop: 250 }, 600);
  }

  changepayType(event: any) {
    debugger;
    this.usePaymentGetway = event.checked;
  }

  ConfirmBooking(payment_id: string, PayementGetWay: string) {
    debugger
    this.ngxService.start();
    const TokenID = this.arrConfirm.tokenId
    if (payment_id !== "")
      this.arrParams = ["{'Email':'" + this.login.LoginDetail.Email + "','PurchaseTocken':'" + payment_id + "','PayementGetWay':'" + PayementGetWay + "', 'UserType':'" + this.login.LoginDetail.UserType + "'}"];
    else
      this.arrParams = []
    // this.userParams.Email = this.login.LoginDetail.Email;
    // this.userParams.PurchaseTocken = payment_id;
    // this.userParams.PayementGetWay = PayementGetWay;

    // this.arrParams.push(JSON.stringify(this.userParams));

    this.hotelService.ConfirmBooking(this.ConfirmationID, TokenID, this.arrParams).subscribe((res: any) => {
      this.ngxService.stop();
      console.log(res);
      if (res.retCode === 1) {
        this.hotelService.setBooking(res);
        this.router.navigate(['/hotel-confirm']);
      }
      else if (res.retCode === 2) {
        alert('' + res.CurrentStatus + '');
      }
      else if (res.retCode === 0) {
        this.Alert.succsess('Sorry', 'Due to some technical issue this booking can not be completed.', 'warning', function () {

        });
      }

    });
  }

  changeStatus(show: any) {
    this.showDetails = show;
  }

  htmlpopup(title: string, type: string, html: any) {
    //if (type == 'success' || type == 'error' || type == 'warning' || type == 'info' || type == 'question')
      // Swal.fire({
      //   title: title,
      //   type: type,
      //   html: html,
      //   showCancelButton: true,
      //   confirmButtonColor: '#3085d6',
      //   cancelButtonColor: '#d33',
      //   confirmButtonText: 'Ok',
      //   reverseButtons: true
      // }).then((result) => {
      //   if (result.value) {
      //     this.showDetails = true;
      //   }
      // })
  }

  checkUser() {
    /*Check User logedIn */
    if (this.cookie.checkcookie('login')) {
      let userdata = JSON.parse(this.cookie.getcookie('login'));
      this.login = userdata;
      this.makeConfirmation();
    }
    else {
      const dialogRef = this.dialog.open(UserComponent);
      dialogRef.afterClosed().subscribe(result => {
        if (this.cookie.checkcookie('login')) {
          let userdata = JSON.parse(this.cookie.getcookie('login'));
          this.login = userdata;
          this.makeConfirmation();
        }
      });
    }
  }
  Payment(payment) {
    debugger
    this.ConfirmBooking(payment.payment_id, payment.PayementGetWay)
  }
}

