import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Hotels, _arrRoom } from 'src/app/modals/hotel/hotel.modal';
declare function Slider():any;
declare function setMap():any;
import { HotelService } from 'src/app/services/hotel/hotel.service';
import { arrBlock, BookingDetails } from 'src/app/modals/hotel/block.modal';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.css']
})
export class HotelDetailsComponent implements OnInit {
@Input() Hotel:Hotels;
@Input() objSearch:any;
@Input() TockenID:string;
@Output() valueChange = new EventEmitter();
_response:boolean=false;
_arrRoom: _arrRoom;
PriviousSelected: any;
nextSelected: any;
arrBlock: arrBlock;
BookingDetails: BookingDetails[];
  constructor(
    private cd: ChangeDetectorRef,
    private hotelService: HotelService,
    private ngxService: NgxUiLoaderService,
    private router: Router,
  ) { }

  ngOnInit() {
    console.log(this.Hotel);
    setInterval(d=>{
      Slider();
      setMap();
    },1000)
    this.getImages();
  }
  
  ngViewInit(){
    Slider();
  }

  changeHotel(){
    this.valueChange.emit(this.Hotel)
    this.cd.detectChanges();
  }

  blockRoomParams(data: any, noRooms: any, ResultIndex: any, GroupIndex: any) {
    
    this.ngxService.start();
    this.arrBlock = new arrBlock();
    this.BookingDetails = [];
    this.BookingDetails.push({
      RoomTypeID: data.RoomTypeId, RoomDescription: data.RoomDescriptionId, Total: data.Total.toFixed(2), noRooms: noRooms, AdultCount: 0, ChildCount: 0, ChildAges: "",
    });
    this.arrBlock.BookingDetails = this.BookingDetails;
    this.arrBlock.TokenId = this.TockenID;
    this.arrBlock.CustomerList = [];
    this.arrBlock.ResultIndex = ResultIndex;
    this.arrBlock.GroupIndex = GroupIndex;
    this.arrBlock.Supplier = 0;
    this.arrBlock.Remark = '';
    this.arrBlock.Company = '';
    this.arrBlock.Mobile = '';
    this.arrBlock.Email = ''
    this.blockRoomRequest(this.arrBlock)
  }

  blockMultipleRoom(o: any, ResultIndex: any, GroupIndex: any) {
    debugger
   this.ngxService.start();
    this.arrBlock = new arrBlock();
    this.BookingDetails = [];

    o.forEach(occupancy => {
      var room = occupancy.Rooms.filter(r => r._roomCount._selected !== 0);
      if (occupancy.RoomCount == room.length) {
        for (let r = 0; r < occupancy.RoomNo.length; r++) {
          this.BookingDetails.push({
            RoomTypeID: room[r].RoomTypeId, RoomDescription: room[r].RoomDescriptionId, Total: room[r].Total.toFixed(2), noRooms: occupancy.RoomNo[r], AdultCount: 0, ChildCount: 0, ChildAges: "",
          });
        }
      }
      else if (room.length == 1) {
        for (let r = 0; r < occupancy.RoomCount; r++) {
          this.BookingDetails.push({
            RoomTypeID: room[0].RoomTypeId, RoomDescription: room[0].RoomDescriptionId, Total: room[0].Total.toFixed(2), noRooms: r + 1, AdultCount: 0, ChildCount: 0, ChildAges: "",
          });
        }
      }
    });
    this.arrBlock.BookingDetails = this.BookingDetails;
    this.arrBlock.TokenId = this.TockenID;
    this.arrBlock.CustomerList = [];
    this.arrBlock.ResultIndex = ResultIndex;
    this.arrBlock.GroupIndex = GroupIndex;
    this.arrBlock.Supplier = 0;
    this.arrBlock.Remark = '';
    this.arrBlock.Company = '';
    this.arrBlock.Mobile = '';
    this.arrBlock.Email = ''
    this.blockRoomRequest(this.arrBlock)
  }
  public SetRoomoccupancy(event: any, occupancy: any, room: any, rate: any) {
    var Selected = rate.RoomOccupancy[occupancy].Rooms[room];
    var sRooms = parseInt(rate.RoomOccupancy[occupancy].Rooms[room]._roomCount._selected);
    var otherRooms = rate.RoomOccupancy[occupancy].Rooms;
    var noRooms = rate.RoomOccupancy[occupancy].RoomCount;
    var remainingToBook = noRooms - sRooms;
    for (let r of otherRooms) {
      if (remainingToBook != r._roomCount._selected) {
        if (r._roomCount._selected > 0 && r != Selected) {
          if (r._roomCount._selected - remainingToBook >= 0) {
            r._roomCount._selected = remainingToBook
            remainingToBook = r._roomCount._selected - remainingToBook;
          }
          else {
            remainingToBook = noRooms - 1
            continue
          }
        }
        else if (r != Selected) {
          r._roomCount._selected = 0;
        }
      }
      else {
        if (r._roomCount._selected != 0 && remainingToBook != sRooms) {
          r._roomCount._selected = remainingToBook - 1
        }
        else {
          if (this.PriviousSelected != null && this.PriviousSelected == r) {
            if (remainingToBook != 0) {
              r._roomCount._selected = remainingToBook - 1;
            }

          }
        }
      }
    }
    rate._commonPrice = this.updatePrice(rate.RoomOccupancy);
    this.PriviousSelected = Selected;
  }


  updatePrice(rooms: any) {
    var Price = 0;
    rooms.forEach(occupancy => {
      occupancy.Rooms.forEach(element => {
        Price += (element.objCharges.TotalPrice) * (element._roomCount._selected);
      });
    });

    return Price.toFixed(2);
  }

  setPrice(no: any, list: any) {
    for (let i = 0; i < list.length; i++) {
      var Price = 0;
      for (let j = 0; j < list[i].RoomOccupancy.length; j++) {
        if (list[i].RoomOccupancy[j].Rooms.length)
          Price += (list[i].RoomOccupancy[j].Rooms[0].Total) * (list[i].RoomOccupancy[j].RoomCount);
        for (let r = 0; r < list[i].RoomOccupancy[j].Rooms.length; r++) {
          this._arrRoom = new _arrRoom();
          this._arrRoom._selected = 0;
          this._arrRoom._rooms = [];
          for (let l = 0; l <= this.objSearch.Rooms.length; l++) {
            if (r === 0)
              this._arrRoom._selected += l;
            this._arrRoom._rooms.push();
          }
          list[i].RoomOccupancy[j].Rooms._roomCount = new _arrRoom();
          list[i].RoomOccupancy[j].Rooms._roomCount._selected = this._arrRoom._selected;
          list[i].RoomOccupancy[j].Rooms._roomCount._rooms = this._arrRoom._rooms;
        }
      }
      //$("#Price_" + no + "_" + i).text(list[i].RoomOccupancy[0].Rooms[0].RoomRateTypeCurrency + ". " + Price.toFixed(2));
      this.cd.detectChanges();
    }
  }
//#endregion

blockRoomRequest(reqData: any) {
  debugger;
  this.hotelService.setarrBlock(reqData);
  console.log(JSON.stringify(reqData));
  this.hotelService._blockRoom(reqData).subscribe((res: any) => {
    this.ngxService.stop();
    if (res.retCode === 1) {
      this.hotelService.setBlockRoomDetails(res);
       this.router.navigate(['/hotel-booking']);
    }
    else {
      //this.Alert.succsess('Sorry!', res.ex, 'warning', function () {
     // })
    }
  });
}
//#endregion
  getImages(){
    try{
      debugger
      this.hotelService.getHotelImages(this.TockenID,this.Hotel.ResultIndex).subscribe((res: any) => {
        this._response = true;
        console.log(res);
        if (res.retCode === 1) {
          this.Hotel.Image= res.Images;
        }
        this.cd.detectChanges();
      }, error => {
        console.log(error)
      });
    }catch(e){console.log(e)}
  }
}
