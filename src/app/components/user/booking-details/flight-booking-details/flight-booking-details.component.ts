import { Component, OnInit } from "@angular/core";
import {
  ArrResrvation,
  Segment,
  Pax,
  FareDetail,
  FlightInvoice,
} from "src/app/modals/flight/flight-booking-report.modal";
import { AdminUser, AgentDetails } from "src/app/modals/hotel/report.modal";
import { ApiUrlService } from "src/app/services/api-url.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import * as moment from "moment";
import { FlightService } from "src/app/services/flight/flight.service";
import { loginDetails } from "src/app/modals/user.model";
import { Seat } from "src/app/modals/flight/flight-search.modal";
import { AlertService } from "src/app/services/alert.service";
import { DialogService } from "src/app/services/commons/dialog.service";
import { NgxUiLoaderService } from "ngx-ui-loader";

@Component({
  selector: "app-flight-booking-details",
  templateUrl: "./flight-booking-details.component.html",
  styleUrls: ["./flight-booking-details.component.css"],
})
export class FlightBookingDetailsComponent implements OnInit {
  Seats: Seat[] = [
    { name: "All", id: "1" },
    { name: "Economy", id: "2" },
    { name: "Premium Economy", id: "3" },
    { name: "Business", id: "4" },
    { name: "Premium Business", id: "5" },
    { name: "First", id: "6" },
  ];
  UserId: any;
  ReservationID: any;
  BookingId: any;
  action: string;
  _display: string;
  InvoiceDetail: FlightInvoice;
  AdminUser: AdminUser;
  AgentDetails: AgentDetails;
  userdetails: loginDetails;
  segment: Segment[];
  pax: Pax[];
  isb2c: boolean;
  Airlines: any[];
  faredetails: FareDetail[];
  constructor(
    private ServiceUrl: ApiUrlService,
    private route: ActivatedRoute,
    private cookie: CommonCookieService,
    private Alert: AlertService,
    private router: Router,
    private ngxService: NgxUiLoaderService,
    private dialogService: DialogService,
    private flightService: FlightService
  ) {}

  ngOnInit() {
    debugger;
    this.InvoiceDetail = new FlightInvoice();
    this.InvoiceDetail.arrResrvation = new ArrResrvation();
    this.InvoiceDetail.Segment = [];
    this.InvoiceDetail.Pax = [];
    this.InvoiceDetail.fareDetails = [];
    this.InvoiceDetail.Baggage = [];
    this.InvoiceDetail.MealDynamic = [];
    this.AdminUser = new AdminUser();
    this.AgentDetails = new AgentDetails();
    this.userdetails = new loginDetails();
    this.Airlines = [];
    this._display = "Detail";
    if (
      this.route.snapshot.queryParamMap.get("parm_1") !== null &&
      this.route.snapshot.queryParamMap.get("parm_2") !== null
    ) {
      this.getAirlines(
        this.route.snapshot.queryParamMap.get("parm_1"),
        this.route.snapshot.queryParamMap.get("parm_2")
      );
    }
    this.getUserdata();
  }

  getAirlines(UserId: any, ReservationID: any) {
    this.flightService.getAirlines().subscribe((res: any) => {
      this.Airlines = res;
      this.getBookingDetails(UserId, ReservationID);
    });
  }

  getBookingDetails(UserId: any, ReservationID: any) {
    this.ngxService.start();
    this.flightService
      .getFlightBookingDetails(UserId, ReservationID)
      .subscribe((res: any) => {
        debugger;
        this.ngxService.stop();
        this.InvoiceDetail = res;
        this.setDetails();
      });
  }

  setDetails() {
    debugger;
    this.InvoiceDetail.arrResrvation = this.InvoiceDetail.arrResrvation[0];
    this.InvoiceDetail.arrResrvation.Date = this.InvoiceDetail.arrResrvation.Date.split(
      " "
    )[0];
    this.InvoiceDetail.arrResrvation.DepartureDate = moment(
      this.InvoiceDetail.arrResrvation.DepartureDate
    ).format("llll");
    this.InvoiceDetail.arrResrvation.AirlineName = this.Airlines.filter(
      (a) => a.code === this.InvoiceDetail.arrResrvation.AirlineCode
    )[0];
    this.InvoiceDetail.arrResrvation.AirlineName = this.InvoiceDetail.arrResrvation.AirlineName.name;
    this.InvoiceDetail.AdminUser = this.InvoiceDetail.AdminUser;
    this.InvoiceDetail.AgentDetails = this.InvoiceDetail.AgentDetails;
    this.setSegment();
    this.InvoiceDetail.Pax = this.InvoiceDetail.Pax;
    this.InvoiceDetail.Baggage = this.InvoiceDetail.Baggage;
    this.InvoiceDetail.MealDynamic = this.InvoiceDetail.MealDynamic;
    this.InvoiceDetail.fareDetails = this.InvoiceDetail.fareDetails;
  }

  setSegment() {
    debugger;
    this.InvoiceDetail.Segment.forEach((segment) => {
      segment.OriginAirport = {
        airport: segment.OriginAirport.split(",")[0].split("(")[0],
        terminal: segment.OriginAirport.split(",")[1],
        city: segment.OriginAirport.split(",")[2],
        country: segment.OriginAirport.split(",")[3],
        code: segment.OriginAirport.split(",")[0].split("(")[1].split(")")[0],
      };
      segment.DestinationAirport = {
        airport: segment.DestinationAirport.split(",")[0].split("(")[0],
        terminal: segment.DestinationAirport.split(",")[1],
        city: segment.DestinationAirport.split(",")[2],
        country: segment.DestinationAirport.split(",")[3],
        code: segment.DestinationAirport.split(",")[0]
          .split("(")[1]
          .split(")")[0],
      };
      segment.Airline = this.Airlines.filter(
        (a) => a.code === segment.AirlineCode
      )[0];
      segment.Airline = segment.Airline.name;
      segment.seat = this.Seats.filter(
        (s) => s.id === segment.Craft.split("-")[1]
      )[0];
      segment.seat = segment.seat.name;
      var hours = Math.floor(segment.Duration / 60);
      var minutes = segment.Duration % 60;
      segment.Duration = `${hours}h ${minutes}m`;
      segment.DepTime = {
        time: moment(segment.DepTime).format("LT"),
        date: moment(segment.DepTime).format("ll"),
      };
      segment.ArrTime = {
        time: moment(segment.ArrTime).format("LT"),
        date: moment(segment.ArrTime).format("ll"),
      };
    });
    this.InvoiceDetail.Segment = this.InvoiceDetail.Segment;
    console.log(JSON.stringify(this.InvoiceDetail));
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let user = JSON.parse(this.cookie.getcookie("login"));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B") this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
  }

  downloadPdf(service: string) {
    debugger;
    this._display = service;
    this.action = "download";
  }

  print(service: string) {
    debugger;
    this._display = service;
    this.action = "print";
  }

  Back() {
    debugger;
    this._display = "Detail";
    this.action = "";
  }

  CancelBooking(BookingId: any) {
    debugger;
    BookingId = +BookingId;
    this.dialogService
      .openConfirmDialog("are you sure want to cancel this booking ?")
      .afterClosed()
      .subscribe((response) => {
        if (response !== undefined) {
          if (response === true) {
            this.ngxService.start();
            this.flightService.CancelTiketedBooking(BookingId).subscribe(
              (res: any) => {
                console.log(res);
                this.ngxService.stop();
                if (res.retCode === 1) {
                  this.Alert.confirm(
                    "Your Booking is Cancelled Successfully",
                    "success"
                  );
                  setTimeout(() => {
                    this.router.navigate(["/booking-report"]);
                  }, 4000);
                } else {
                  this.Alert.confirm("something wents wrong", "warning");
                }
              },
              (error) => {
                console.log(error);
              }
            );
          }
        }
        console.log(response);
      });
  }

  Action(service: any) {
    if (this.action === "download") {
      /* download invoice */
      if (service === "Invoice") {
        setTimeout(() => {
          $("#btn_Invoice_pdf").click();
        }, 1000);
      }

      /* download voucher */
      if (service === "Ticket") {
        setTimeout(() => {
          $("#btn_Ticket_pdf").click();
        }, 1000);
      }
    }

    if (this.action === "print") {
      setTimeout(() => {
        window.print();
      }, 1000);
    }
  }
}
