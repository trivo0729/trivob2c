import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ReportService } from "src/app/services/commons/report.service";
import {
  ArrResrvation,
  Invoice,
  HotelDetails,
} from "src/app/modals/hotel/report.modal";
import { HotelService } from "src/app/services/hotel/hotel.service";
import { arrCancel, UserDetail } from "src/app/modals/hotel/booking.modal";
import { ApiUrlService } from "src/app/services/api-url.service";
import { AlertService } from "src/app/services/alert.service";
import { DialogService } from "src/app/services/commons/dialog.service";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { loginDetails } from "src/app/modals/user.model";
declare function StickySidebar(): any;

@Component({
  selector: "app-booking-details",
  templateUrl: "./booking-details.component.html",
  styleUrls: ["./booking-details.component.css"],
})
export class BookingDetailsComponent implements OnInit {
  panelOpenState = false;
  UserId: any;
  ReservationID: any;
  InvoiceDetails: Invoice;
  arrCancel: arrCancel;
  UserDetail: UserDetail;
  _display: string;
  action: string;
  userdetails: loginDetails;
  isb2c: boolean;

  constructor(
    private route: ActivatedRoute,
    private reportService: ReportService,
    private router: Router,
    private dialogService: DialogService,
    private hotelService: HotelService,
    private Alert: AlertService,
    private cookie: CommonCookieService,
    private apiUrl: ApiUrlService
  ) {}

  ngOnInit() {
    debugger;
    this.action = "";
    this.InvoiceDetails = new Invoice();
    this.InvoiceDetails.arrResrvation = new ArrResrvation();
    this.InvoiceDetails.arrPaxes = [];
    this.InvoiceDetails.arrRooms = [];
    this.InvoiceDetails.RateBreakups = [];
    this.InvoiceDetails.CancellationPolicy = [];
    this.InvoiceDetails.dtHotelAdd = new HotelDetails();
    this._display = "Detail";
    //this.UserId = this.route.snapshot.queryParamMap.get('parm_1');
    //this.ReservationID = this.route.snapshot.queryParamMap.get('parm_2');
    //StickySidebar();

    if (
      this.route.snapshot.queryParamMap.get("parm_1") !== null &&
      this.route.snapshot.queryParamMap.get("parm_2") !== null
    ) {
      this.getBookingDetails(
        this.route.snapshot.queryParamMap.get("parm_1"),
        this.route.snapshot.queryParamMap.get("parm_2")
      );
    }
    this.getUserdata();
  }

  getBookingDetails(UserId: any, ReservationID: any) {
    debugger;
    this.UserId = UserId;
    this.ReservationID = ReservationID;
    this.reportService
      .GetHotelInvoice(UserId, ReservationID)
      .subscribe((res: any) => {
        if (res.retCode === 1) this.InvoiceDetails = res;
      });
    // this.reportService.getHotels_details().subscribe((res: any) => {
    //   if (res.retCode === 1) {
    //     this.InvoiceDetails = res;
    //     this.Resrvation = res.arrResrvation;
    //     this.Guestlist = res.arrPaxes;
    //     this.Roomlist = res.arrRooms;
    //     console.log(res)
    //   }
    // });
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let user = JSON.parse(this.cookie.getcookie("login"));
      this.userdetails = user;
      if (this.userdetails.LoginDetail.UserType === "B2B") this.isb2c = false;
      if (this.userdetails.LoginDetail.UserType === "B2C") {
        this.isb2c = true;
      }
    }
  }

  downloadPdf(service: string) {
    debugger;
    this._display = service;
    this.action = "download";
  }

  print(service: string) {
    debugger;
    this._display = service;
    this.action = "print";
  }

  Back() {
    debugger;
    this._display = "Detail";
    this.action = "";
  }

  CancelBooking() {
    debugger;
    this.arrCancel = new arrCancel();
    this.UserDetail = new UserDetail();
    if (this.isb2c) {
      this.UserDetail.ParentID = this.apiUrl.AdminID;
      this.UserDetail.username = this.apiUrl.UserName;
      this.UserDetail.Password = this.apiUrl.Password;
    } else if (!this.isb2c) {
      this.UserDetail.ParentID = this.userdetails.LoginDetail.ParentId;
      this.UserDetail.username = this.userdetails.LoginDetail.Email;
      this.UserDetail.Password = this.userdetails.LoginDetail.Password;
    }

    this.arrCancel.UserDetail = this.UserDetail;
    this.arrCancel.ReservationID = this.ReservationID;
    console.log(JSON.stringify(this.arrCancel));
    this.dialogService
      .openConfirmDialog("are you sure want to cancel this booking ?")
      .afterClosed()
      .subscribe((response) => {
        if (response !== undefined) {
          if (response === true) {
            this.hotelService.BookingCancel(this.arrCancel).subscribe(
              (res: any) => {
                console.log(res);
                if (res.retCode === 1) {
                  this.Alert.confirm(
                    "Your Booking is Cancelled Successfully",
                    "success"
                  );
                  setTimeout(() => {
                    this.router.navigate(["/booking-report"]);
                  }, 4000);
                } else {
                  this.Alert.confirm("something wents wrong", "warning");
                }
              },
              (error) => {
                console.log(error);
              }
            );
          }
        }
        console.log(response);
      });
  }

  Action(service: any) {
    if (this.action === "download") {
      /* download invoice */
      if (service === "Invoice") {
        setTimeout(() => {
          $("#btn_Invoice_pdf").click();
        }, 1000);
      }

      /* download voucher */
      if (service === "Voucher") {
        setTimeout(() => {
          $("#btn_Voucher_pdf").click();
        }, 1000);
      }
    }

    if (this.action === "print") {
      setTimeout(() => {
        window.print();
      }, 1000);
    }
  }
}
