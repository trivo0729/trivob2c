import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import {
  Userdetails,
  LoginDetail,
  ContacDetails,
} from "src/app/modals/user/userdetails";
import { GenralService } from "src/app/services/genral.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { AlertService } from "src/app/services/alert.service";
import { Observable } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { FormControl } from "@angular/forms";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { objSearch } from "src/app/modals/hotel/search.modal";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"],
})
export class ProfileComponent implements OnInit {
  AdminID: number;
  public Userdetails: Userdetails = new Userdetails();
  LoginDetails: LoginDetail;
  loading: boolean;
  Country: any[] = [];
  arrCity: any[] = [];
  city: any;
  arrCountry: Observable<any[]>;
  nationalityCtrl = new FormControl();

  constructor(
    private userService: UserService,
    private genralService: GenralService,
    private service: ApiUrlService,
    private Alert: AlertService,
    private cookie: CommonCookieService,
    private cd: ChangeDetectorRef
  ) {
    this.AdminID = service.AdminID;
  }

  ngOnInit() {
    debugger;
    this.loading = false;
    this.Userdetails.sParentID = this.service.AdminID;
    this.arrCity = [];
    this.LoginDetails = new LoginDetail();
    this.LoginDetails.ContacDetails = new ContacDetails();
    this.getCountry();
  }

  UpdateProfile() {
    debugger;
    let city = this.LoginDetails.ContacDetails.CitiesID;
    let country = this.LoginDetails.ContacDetails.CountryID;
    if (country === "" || country === undefined || country === null) {
      alert("Please Select Country");
      return;
    }
    if (city === "" || city === undefined || city === null) {
      alert("Please Select City");
      return;
    }
    this.loading = true;
    let citycode = this.arrCity.filter((c) => c.Description === city)[0];
    this.userService.UpdateProfile(this.LoginDetails, citycode.Code).subscribe(
      (res: any) => {
        this.loading = false;
        if (res.retCode == 1) {
          this.LoginDetails.ContacDetails.CitiesID = citycode.Description;
          this.Userdetails.LoginDetail = this.LoginDetails;
          this.Userdetails.Message = "";
          this.Userdetails.retCode = 1;
          this.Userdetails.sParentID = this.service.AdminID;
          var current = new Date();
          current.setTime(current.getTime() + 30 * 60 * 1000);
          this.cookie.setcookie(
            "login",
            JSON.stringify(this.Userdetails),
            current
          );
          //this.userService.setLogedIn(JSON.stringify(this.Userdetails));
          this.Alert.confirm("User Profile Updated Successfully", "success");
          //alert('User Profile Updated Successfully');
          setTimeout(() => {
            window.location.href = "/home";
          }, 3000);
        } else {
          this.Alert.confirm(res.Message, "warning");
          //alert(res.Message);
        }
      },
      (error) => {
        console.log(error);
        this.loading = false;
      }
    );
  }

  getCountry() {
    this.genralService.getCountry().subscribe((res: any) => {
      this.Country = res;
      if (this.cookie.checkcookie("login")) {
        debugger;
        let userdetails = JSON.parse(this.cookie.getcookie("login"));
        if (userdetails !== null) {
          let country = this.Country.filter(
            (country) =>
              country.Countryname ===
              userdetails.LoginDetail.ContacDetails.CountryID
          )[0];
          if (country) this.getCity(country.Country);
          this.LoginDetails = userdetails.LoginDetail;
          this.LoginDetails.ParentId = this.service.AdminID;
        }
      }
      this.arrCountry = this.nationalityCtrl.valueChanges.pipe(
        startWith(""),
        map((Country) =>
          Country ? this._filterCountry(Country) : this.Country.slice()
        )
      );
      console.log(res);
    });
  }

  selectedCountry(event: any) {
    this.getCity(event.Country);
  }

  selectedCity(event: any) {
    this.city = event.Code;
  }

  getCity(country: any) {
    this.genralService.getCity(country).subscribe(
      (response: any) => {
        this.arrCity = response;
        this.cd.detectChanges();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private _filterCountry(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.Country.filter(
      (Country) => Country.Countryname.toLowerCase().indexOf(filterValue) === 0
    );
  }
}
