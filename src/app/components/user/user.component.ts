import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/services/user.service";
import { User, loginDetails, ContacDetails } from "src/app/modals/user.model";
import { Router } from "@angular/router";
import { MatDialog, MatDialogRef } from "@angular/material";
import { SignUpComponent } from "src/app/components/user/sign-up/sign-up.component";
import {
  GoogleLoginProvider,
  FacebookLoginProvider,
  AuthService,
} from "angular-6-social-login";
import { ToastrService } from "ngx-toastr";
import { ApiUrlService } from "src/app/services/api-url.service";
import { Userdetails, LoginDetail } from "src/app/modals/user/userdetails";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ForgetPasswordComponent } from "./forget-password/forget-password.component";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"],
  providers: [UserService],
})
export class UserComponent implements OnInit {
  AdminID: number;
  UserType: string[];
  _userTforget: string;
  checked: boolean;
  loading: boolean;
  submitted: boolean;
  signin: FormGroup;
  LoginDetail: LoginDetail = new LoginDetail();
  constructor(
    private userService: UserService,
    private router: Router,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<UserComponent>,
    private socialAuthService: AuthService,
    private toastr: ToastrService,
    private cookie: CommonCookieService,
    public UrlService: ApiUrlService,
    private formBuilder: FormBuilder
  ) {
    this.AdminID = UrlService.AdminID;
    this.UserType = UrlService.UserType;
    this.checked = false;
  }

  public login: Userdetails = new Userdetails();
  Response: any;
  sUserName: string;
  sPassword: string;
  UserDetails: User = new User();

  ngOnInit() {
    this._userTforget = "B2C";
    this.loading = false;
    this.submitted = false;
    this.signin = this.formBuilder.group({
      UserName: ["", Validators.required],
      Password: ["", [Validators.required]],
    });
  }

  get f() {
    return this.signin.controls;
  }

  SignUpDialog() {
    this.dialogRef.close();
    this.dialog.open(SignUpComponent, {
      height: "auto",
      width: "600px",
    });
  }

  ForgotDialog() {
    this.dialog.closeAll();
    this.dialog.open(ForgetPasswordComponent, {
      height: "auto",
      width: "600px",
      data: {
        userType: this._userTforget,
      },
    });
  }

  getUserlogin() {
    debugger;
    this.loading = true;
    this.submitted = true;
    if (this.signin.invalid) {
      this.loading = false;
      return;
    }
    this.login = new Userdetails();
    this.userService
      .UserLogin(this.sUserName, this.sPassword, this.AdminID, this.UserType)
      .subscribe(
        (res: any) => {
          this.loading = false;
          if (res.retCode == 1) {
            this.login = res;
            // this.userService.setLogedIn(JSON.stringify(res));
            this.dialogRef.close();
            this.login = res;
            this.userService.setLogedIn(JSON.stringify(res));
            if (this.cookie.checkcookie("login"))
              this.cookie.deletecookie("login");
            this.toastr.success("Login Success");
            var current = new Date();
            current.setTime(current.getTime() + 120 * 60 * 1000);
            this.cookie.setcookie("login", JSON.stringify(res), current);
            window.location.reload();
            if (this.login.LoginDetail.UserType === "B2B") this.redirect();
          } else {
            this.toastr.error(res.Message.toString());
            this.login.retCode = res.retCode;
          }
        },
        (error) => {
          console.log(error);
          this.loading = false;
        }
      );
  }

  changeUser(event: any) {
    if (event.checked) {
      this.UserType = ["B2B"];
      this._userTforget = "B2B";
    }
    if (!event.checked) {
      this.UserType = ["B2C"];
      this._userTforget = "B2C";
    }

    this.checked = event.checked;
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.socialAuthService
      .signIn(socialPlatformProvider)
      .then((userData: any) => {
        this.toastr.success("Login Success");
        this.UserDetails = new User();
        this.UserDetails.ContacDetails = new ContacDetails();
        if (socialPlatformProvider == "google") {
          var arrName = userData.name.toString().split(" ");
          this.UserDetails.Name = arrName[0];
          this.UserDetails.LastName = arrName[arrName.length - 1];
          this.UserDetails.Email = userData.email;
        } else if (socialPlatform == "facebook") {
          this.UserDetails.Name = userData.first_name;
          this.UserDetails.LastName = userData.last_name;
          if (userData.email != undefined)
            this.UserDetails.Email = userData.email;
        }
        this.userService.signup(this.UserDetails).subscribe((res: any) => {
          if (res.retCode == 1) {
            this.dialogRef.close();
            this.setUserdata(1);
            this.updateUser();
          } else {
            this.dialogRef.close();
            this.setUserdata(1);
            this.updateUser();
          }
        });
      });
  }

  setUserdata(retCode: any) {
    this.login = new Userdetails();
    this.LoginDetail = new LoginDetail();
    this.LoginDetail.Name = this.UserDetails.Name;
    this.LoginDetail.LastName = this.UserDetails.LastName;
    this.LoginDetail.Email = this.UserDetails.Email;
    this.LoginDetail.UserType = "B2C";
    this.login.LoginDetail = this.LoginDetail;
    this.login.retCode = retCode;
    var current = new Date();
    current.setTime(current.getTime() + 30 * 60 * 1000);
    this.cookie.setcookie("login", JSON.stringify(this.login), current);
  }

  updateUser() {
    this.login = new Userdetails();
    if (this.cookie.checkcookie("login")) {
      let userData =  JSON.parse(this.cookie.getcookie("login"));
      if (userData !== null) {
        this.login = userData;
        if (this.login.LoginDetail.UserType === "B2B") this.redirect();
        else window.location.reload();
      }
    } else this.login.retCode == 0;
  }

  redirect() {
    window.location.href = "/home";
  }

  clickRegister() {
    this.dialog.closeAll();
    this.router.navigate(["/agent-register"]);
  }
}
