import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User, ContacDetails, loginDetails } from 'src/app/modals/user.model';
import { MatDialog, MatDialogRef } from '@angular/material';
import { UserComponent } from '../user.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { equalValueValidator } from 'src/app/app.component';
import { AlertService } from 'src/app/services/alert.service';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Router } from '@angular/router';
import { CommonCookieService } from 'src/app/services/commons/common-cookie.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  user: User = new User();
  checked: boolean;
  AdminId: number;
  public login: loginDetails = new loginDetails();
  submitted = false;
  ContacDetails = new ContacDetails();
  register: FormGroup;
  constructor(private userService: UserService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<SignUpComponent>,
    private formBuilder: FormBuilder,
    public service: ApiUrlService,
    private router: Router,
    private Alert: AlertService,
    private cookie: CommonCookieService
  ) { this.AdminId = service.AdminID }
  ngOnInit() {
    this.user = new User();
    this.user.ContacDetails = new ContacDetails();
    this.register = this.formBuilder.group({
      Name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      confirm: ['', [Validators.required]],
    },
      { validator: equalValueValidator('password', 'confirm') }
    );
  }

  get f() { return this.register.controls; }

  public Validate(): void {

    this.submitted = true;
    // stop here if form is invalid
    if (this.register.invalid) {
      return;
    }
    else {
      this.Save();
    }
  }
  Save() {
    this.user.ContacDetails = new ContacDetails();
    this.user.ParentId = this.AdminId;
    debugger
    this.userService.signup(this.user).subscribe(
      (res: any) => {
        console.log(res);
        if (res.retCode == 1) {
          this.dialogRef.close();
          this.userService.setLogedIn(JSON.stringify({ retCode: 1, LoginDetail: this.user }));
          this.Alert.confirm('Registered Successfully', 'success');
          if (this.cookie.checkcookie('login')) {
            this.login = JSON.parse(this.cookie.getcookie('login'));
          }
        }
        else {
          this.Alert.confirm(res.Message, 'warning')
        }
      });
  }



  SignDialog() {
    this.dialog.closeAll()
    this.dialogRef.close();
    const dialogRef = this.dialog.open(UserComponent);
  }

  onAgent(checked: boolean) {
    if (checked) {
      this.dialog.closeAll();
      this.router.navigate(['/agent-register']);
    }
  }
}
