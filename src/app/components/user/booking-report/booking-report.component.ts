import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { Services } from "src/app/modals/common/services.modal";
import { UserService } from "src/app/services/user.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { Userdetails } from "src/app/modals/user/userdetails";
import { AlertService } from "src/app/services/alert.service";

@Component({
  selector: "app-booking-report",
  templateUrl: "./booking-report.component.html",
  styleUrls: ["./booking-report.component.css"],
})
export class BookingReportComponent implements OnInit {
  _hotel: boolean;
  _package: boolean;
  _tour: boolean;
  _flight: boolean;

  _activate: string;
  serviceList: Services[];
  userdata: any;
  _userinfo: any;

  user: Userdetails;

  constructor(
    private cd: ChangeDetectorRef,
    private Alert: AlertService,
    private cookie: CommonCookieService,
    public ServiceUrl: ApiUrlService
  ) {}

  ngOnInit() {
    this.serviceList = [];
    this._hotel = false;
    this._package = false;
    this._tour = false;
    this._flight = false;
    this._activate = "flight";
    this.user = new Userdetails();
    this.getUserdata();
  }

  changeTabs(service: string) {
    debugger;
    this._activate = service;
  }

  getServices(event: any) {
    debugger;
    this.serviceList.push({
      name: event.name,
      icon: event.icon,
    });
  }

  getUserdata() {
    debugger;
    if (this.cookie.checkcookie("login")) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.userdata = {
            UserId: data.LoginDetail.sid,
            Email: null,
          };
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.userdata = {
            UserId: 0,
            Email: ["" + data.LoginDetail.Email + ""],
          };
        }
        this._userinfo = {
          UserId: data.LoginDetail.sid,
          Email: data.LoginDetail.Email,
          UserType: data.LoginDetail.UserType,
        };
      }
    } else {
      this.userdata = null;
      this._userinfo = null;
      // this.Alert.confirm(
      //   "your session has expired please login again",
      //   "warning"
      // );
      // setTimeout(() => {
      //   window.location.href = "/home";
      // }, 4000);
    }
  }
}
