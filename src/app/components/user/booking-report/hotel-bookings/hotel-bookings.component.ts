import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Output,
  EventEmitter,
  Input,
} from "@angular/core";
import { ReportService } from "src/app/services/commons/report.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { bookingReport } from "src/app/modals/hotel/report.modal";
import * as moment from "moment";
import { loginDetails, User } from "src/app/modals/user.model";
import { UserService } from "src/app/services/user.service";
import { MatDialog } from "@angular/material";
import { Router } from "@angular/router";

@Component({
  selector: "app-hotel-bookings",
  templateUrl: "./hotel-bookings.component.html",
  styleUrls: ["./hotel-bookings.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HotelBookingsComponent implements OnInit {
  @Input() service: string;
  @Output() Services = new EventEmitter();
  @Input() userdata: any;
  step = 0;
  UserId: number;
  BookingList: bookingReport[];
  Bookings: bookingReport[];
  autoApply: boolean;
  closeOnAutoApply: boolean;
  maxDate: any;
  showDropdowns: boolean;
  lockStartDate: boolean;
  singleDatePicker: boolean;
  opens: string;
  drops: string;
  Checkin: any;
  Checkout: any;
  BookingDate: any;
  GuestName: string = "";
  ReferenceNo: string = "";
  HotelName: string = "";
  Location: string = "";
  allStatus: string[];
  Status: any;
  selected: string;
  login: loginDetails = new loginDetails();
  User: User = new User();
  response: boolean;
  page: number = 1;
  pageSize = 20;
  outboundPage: number = 1;
  outpagesize = 20;
  option: any;

  constructor(
    private reportService: ReportService,
    public dialog: MatDialog,
    private router: Router,
    public userService: UserService,
    private apiUrl: ApiUrlService,
    private cd: ChangeDetectorRef
  ) {
    this.UserId = this.apiUrl.UserId;
    this.opens = "right";
    this.drops = "down";
    this.singleDatePicker = true;
  }

  ngOnInit() {
    this.response = false;
    this.option = {
      locale: { format: "DD-MM-YYYY" },
      alwaysShowCalendars: false,
      showDropdowns: true,
      autoApply: true,
      singleDatePicker: true,
    };
    this.BookingList = [];
    this.getHotelBooking();
    this.allStatus = ["All", "Vouchered", "Cancelled"];
    this.Status = "All";
  }

  _filter(Checkin: string, Checkout: string, BookingDate: string) {
    this.BookingList = this.Bookings;
    if (Checkin !== "" && Checkout !== "")
      this.BookingList = this.BookingList.filter(
        (b) => b.CheckIn === Checkin && b.CheckOut === Checkout
      );

    if (BookingDate !== "")
      this.BookingList = this.BookingList.filter(
        (b) => b.ReservationDate === BookingDate
      );

    if (this.GuestName !== "")
      this.BookingList = this.BookingList.filter((b) =>
        b.bookingname.toLowerCase().includes(this.GuestName.toLowerCase())
      );

    if (this.ReferenceNo !== "")
      this.BookingList = this.BookingList.filter(
        (b) => b.ReservationID === this.ReferenceNo
      );

    if (this.HotelName !== "")
      this.BookingList = this.BookingList.filter((b) =>
        b.HotelName.toLowerCase().includes(this.HotelName.toLowerCase())
      );

    if (this.Location !== "")
      this.BookingList = this.BookingList.filter((b) =>
        b.City.toLowerCase().includes(this.Location.toLowerCase())
      );

    if (this.Status !== "All")
      this.BookingList = this.BookingList.filter(
        (b) => b.Status === this.Status
      );
  }

  clear() {
    this.GuestName = "";
    this.ReferenceNo = "";
    this.HotelName = "";
    this.Location = "";
    this.Status = "All";
    this.Checkin = "";
    this.Checkout = "";
    this.BookingDate = "";

    this.BookingList = this.Bookings;
  }

  /* hotel booking */
  getHotelBooking() {
    debugger;
    if (this.userdata !== null) {
      this.reportService
        .getHotelBooking(this.userdata.UserId, this.userdata.Email)
        .subscribe((res: any) => {
          if (res.retCode === 1) {
            this.Services.emit({
              name: "Hotel",
              icon: "icon_set_1_icon-6 font15",
            });
            this.BookingList = res.arrBookingList;
            this.Bookings = res.arrBookingList;
            this.response = true;
            this.cd.detectChanges();
          }
          console.log(JSON.stringify(res));
        });
    }
    // this.reportService.getHotelBooking().subscribe((res: any) => {
    //   if (res.retCode === 1) {
    //     this.Services.emit({
    //       name: "Hotel",
    //       icon: "icon_set_1_icon-6 font15",
    //     });
    //     this.BookingList = res.arrBookingList;
    //     this.Bookings = res.arrBookingList;
    //     this.response = true;
    //     this.cd.detectChanges();
    //   }
    //   console.log(res);
    // });
  }

  onselected() {}

  showBookingDetails(UserId: any, ReservationID: any) {
    this.router.navigate(["/booking-details"], {
      queryParams: { parm_1: UserId, parm_2: ReservationID },
    });
  }
}
