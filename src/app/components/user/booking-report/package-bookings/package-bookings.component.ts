import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { ReportService } from 'src/app/services/commons/report.service';
import { PakageReport } from 'src/app/modals/packages/pakage-report.modal';

@Component({
  selector: 'app-package-bookings',
  templateUrl: './package-bookings.component.html',
  styleUrls: ['./package-bookings.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PackageBookingsComponent implements OnInit {
  UserId: number;
  PakageBookingList: PakageReport[];
  page: number = 1;
  response: boolean;
  @Input() service: string;
  @Output() Services = new EventEmitter();
  @Input() userinfo: any;
  constructor(private reportService: ReportService,
    private cd: ChangeDetectorRef) { }

  ngOnInit() {
    this.response = false;
    this.PakageBookingList = [];
    this.getPackageBooking();
  }

  /* package booking */
  getPackageBooking() {
    debugger;
    if (this.userinfo !== null) {
      this.reportService.getPackageBooking(this.userinfo.UserId).subscribe((res: any) => {
        this.response = true;
        if (res.retCode === 1) {
          this.PakageBookingList = res.arrBookingList;
          this.Services.emit({
            name: "Package",
            icon: "icon_set_1_icon-8 font15"
          });
        }
        this.cd.detectChanges();
      }, (error: any) => {
        this.response = true;
        console.log(error);
      })
    }
  }

}
