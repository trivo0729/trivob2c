import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
} from "@angular/core";
import { Package } from "src/app/modals/packages/package.modal";
import { PackageService } from "src/app/services/packages/package.service";
import { MatDialog } from "@angular/material/dialog";
import { ActivatedRoute, Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { ApiUrlService } from "src/app/services/api-url.service";
import { UserComponent } from "../../user/user.component";
import { UserService } from "src/app/services/user.service";
import { AlertService } from "src/app/services/alert.service";
import { Userdetails } from "src/app/modals/user/userdetails";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { NgxUiLoaderService } from 'ngx-ui-loader';
declare function HotelCarousel(): any;
declare function ChangeStyle(): any;
declare function StickySidebar(): any;
declare function setimage(img: string): any;
declare function set_class(): any;

@Component({
  selector: "app-package-details",
  templateUrl: "./package-details.component.html",
  styleUrls: ["./package-details.component.css"],
})
export class PackageDetailsComponent implements OnInit {
  public login: Userdetails = new Userdetails();
  ID: string;
  AdminID: number;
  Packages: Package;
  img: string = "";
  Images: any[];
  minimum: Array<number>;
  Share: any;
  Bit: any;
  minrate: Array<number>;
  template: SafeHtml;
  constructor(
    private packageService: PackageService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private cd: ChangeDetectorRef,
    public userService: UserService,
    public dialog: MatDialog,
    private ServiceUrl: ApiUrlService,
    public objGlobal: ApiUrlService,
    private alert: AlertService,
    private cookie: CommonCookieService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private ngxService: NgxUiLoaderService,
  ) {}
  ngOnInit() {
    this.ngxService.start();
    this.Share = { url: "", details: "", img: "", title: "" };
    this.Packages = new Package();
    this.Packages.Category = [];
    this.Packages.arrInclusions = [];
    this.Packages.arrExclusions = [];
    this.Images = [];
    this.ID = this.route.snapshot.queryParamMap.get("ID");
    this.Bit = 1;
    this.packageService.getPackageDetail(this.ID).subscribe((res: any) => {
      this.Packages = res[0];
      this.ngxService.stop();
      this.Packages.ValidFrom = this.datePipe.transform(
        this.Packages.ValidFrom,
        "dd-MM-yy"
      );
      this.Packages.ValidTo = this.datePipe.transform(
        this.Packages.ValidTo,
        "dd-MM-yy"
      );
      if (this.Packages.arrInclusions.length == 1)
        this.Packages.arrInclusions[0] = this.sanitizer.bypassSecurityTrustHtml(
          this.Packages.arrInclusions[0]
        );
      if (this.Packages.arrExclusions.length == 1)
        this.Packages.arrExclusions[0] = this.sanitizer.bypassSecurityTrustHtml(
          this.Packages.arrExclusions[0]
        );

      console.log(this.Packages);
      if (this.Packages.Images.length != 0) {
        this.img = this.Packages.Images[0].Url;
        this.Share.img = this.img;
        this.Images = this.Packages.Images;
        //   this.SetImages();
      }
      this.Share.title = this.Packages.PackageName;
      this.Share.details = this.Packages.Description;
      this.Share.url =
        this.objGlobal.website + "/package-details?ID=" + this.ID;
      this.SetMinimumPrice();
      this.cd.detectChanges();
      StickySidebar();
      console.log(this.Packages);
    });
    //this.GetTemplate();
  }
  // ngAfterViewInit() {
  //   var elem = document.getElementsByClassName('sb-icon')[0];
  //   elem.className = "sb-icon icon-facebook";
  // }

  // GetTemplate() {
  //   debugger;
  //   this.packageService.GetPackageTemplate(this.ID).subscribe((res: any) => {
  //     if (res.retCode == 1) {
  //       $("#Package_frame").contents().find("html").html(res.sMail);
  //       let html = "";
  //       html +=
  //         '<kendo-pdf-export #pdf paperSize="A4" margin="0.5cm" [scale]="0.6" >';
  //       //this.template = this.sanitizer.bypassSecurityTrustHtml(res.sMail);
  //       html += "" + res.sMail + "";
  //       html +=
  //         '<button kendo-button (click)="pdf.saveAs(' +
  //         this.Packages.PackageName +
  //         '.pdf)" class="btn_full" id="btn_pdf" style="display:none">Download </button>';
  //       html += "</kendo-pdf-export>";
  //       $("#Package_frame").contents().find("html").html(html);
  //       this.cd.detectChanges();
  //     }
  //   });
  // }

  onDownload() {
    $("#Package_frame").show();
    setTimeout(() => {
      $("#btn_pdf").click();
    }, 1000);
    setTimeout(() => {
      $("#Package_frame").hide();
    }, 2000);
  }

  SetMinimumPrice() {
    debugger;
    this.minimum = [];
    if (this.Packages.Category.length != 0) {
      if (this.Packages.Category[0].Double != 0) {
        this.minimum.push(this.Packages.Category[0].Double);
      }
      if (this.Packages.Category[0].Single != 0) {
        this.minimum.push(this.Packages.Category[0].Single);
      }
      if (this.Packages.Category[0].Triple != 0) {
        this.minimum.push(this.Packages.Category[0].Triple);
      }
      if (this.Packages.Category[0].Quad != 0) {
        this.minimum.push(this.Packages.Category[0].Quad);
      }
      if (this.Packages.Category[0].Quint != 0) {
        this.minimum.push(this.Packages.Category[0].Quint);
      }
      this.Packages.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
    } else {
      this.Packages.MinPrice = 0;
    }
  }

  // SetImages() {
  //   setTimeout(() => {
  //     owl();
  //     setimage(this.img);
  //     set_class();
  //   }, 20);
  //   setInterval(() => {
  //     set_class();
  //   }, 100);
  //   setInterval(() => {
  //     HotelCarousel();
  //   }, 5000);
  //   setInterval(() => {
  //     ChangeStyle();
  //   }, 1000);
  // }

  public getLowestPriceEachCategory(category: any) {
    this.minrate = [];
    if (category.length != 0) {
      if (category.Double != 0) {
        this.minrate.push(category.Double);
      }
      if (category.Single != 0) {
        this.minrate.push(category.Single);
      }
      if (category.Triple != 0) {
        this.minrate.push(category.Triple);
      }
      if (category.Quad != 0) {
        this.minrate.push(category.Quad);
      }
      if (category.Quint != 0) {
        this.minrate.push(category.Quint);
      }
      this.Packages.MinPrice = this.minrate.reduce((a, b) => Math.min(a, b));
    } else {
      this.Packages.MinPrice = 0;
    }
  }

  public Booking(Id: number) {
    debugger;
    /*Check User logedIn */
    this.login = new Userdetails();
    if (this.cookie.checkcookie("login")) {
      let userData = this.cookie.getcookie("login");
      if (userData !== null) {
        window.location.href = "packagebooking?ID=" + Id;
      }
    } else {
      const dialogRef = this.dialog.open(UserComponent);
      dialogRef.afterClosed().subscribe((result) => {
        console.log(result);
      });
    }
  }

  public SendEnquiry(Id: number) {
    debugger;
    //window.location.href = 'package-booking?ID=' + Id + '&bit='+this.Bit;
    this.router.navigate(["/packagebooking"], {
      queryParams: { ID: Id, bit: this.Bit },
    });
  }

  GetLoginDetails() {
    debugger;
    this.login = new Userdetails();

    if (this.cookie.checkcookie("login")) {
      let userData = this.cookie.getcookie("login");
      if (userData !== null) {
        //this.login = userData.;
      }
    } else this.login.retCode == 0;
  }
}
