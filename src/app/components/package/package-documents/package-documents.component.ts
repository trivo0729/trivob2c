import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from "@angular/core";
import { ApiUrlService } from "src/app/services/api-url.service";
import { Package } from "src/app/modals/packages/package.modal";
import { PackageService } from "src/app/services/packages/package.service";
@Component({
  selector: "app-package-documents",
  templateUrl: "./package-documents.component.html",
  styleUrls: ["./package-documents.component.css"],
})
export class PackageDocumentsComponent implements OnInit {
  @Input() Packages: Package;
  minimum: Array<number>;
  AdminCode: any;
  html: string;
  public scale = 0.6;
  sMail: any;
  constructor(
    public objGlobalDefault: ApiUrlService,
    private cd: ChangeDetectorRef,
    private packageService: PackageService
  ) {}

  ngOnInit() {
    this.AdminCode = this.objGlobalDefault.Code;
    this.GetTemplate();
  }
  GetTemplate() {
    debugger;
    this.packageService
      .GetPackageTemplate(this.Packages.Packageid.toString())
      .subscribe((res: any) => {
        // if (res.retCode == 1) {
        //   let frame = $("#Package_frame")
        //     .contents()
        //     .find("html")
        //     .html(res.sMail);
        //   this.cd.detectChanges();
        // }
      });
  }
}
