import { Component, OnInit, Input } from '@angular/core';

declare function Slider():any;
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {
  @Input() Images:any
  constructor() { }

  ngOnInit() {
    this.Images =[];
    setInterval(d=>{
      Slider();
    },1000)
  }
 
  
  ngViewInit(){
    Slider();
  }


}
