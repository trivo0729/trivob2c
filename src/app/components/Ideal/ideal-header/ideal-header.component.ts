import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { GenralService } from "src/app/services/genral.service";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { ApiUrlService } from "src/app/services/api-url.service";
import { CommonCookieService } from "src/app/services/commons/common-cookie.service";
import { UserComponent } from "../../user/user.component";
import { SignUpComponent } from "../../user/sign-up/sign-up.component";
import { Userdetails } from "src/app/modals/user/userdetails";

@Component({
  selector: "app-ideal-header",
  templateUrl: "./ideal-header.component.html",
  styleUrls: ["./ideal-header.component.css"],
})
export class IdealHeaderComponent implements OnInit {
  login: Userdetails = new Userdetails();
  constructor(
    public dialog: MatDialog,
    public genralService: GenralService,
    private router: Router,
    public userService: UserService,
    public objGlobalService: ApiUrlService,
    private cookie: CommonCookieService
  ) {}

  ngOnInit() {
    debugger;
    this.login = new Userdetails();
    this.login.retCode = 0;
    console.log(this.login.retCode);
    this.GetLoginDetails();
  }
  openDialog() {
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(UserComponent);
    dialogRef.afterClosed().subscribe((result) => {
      this.GetLoginDetails();
    });
  }
  RegisterDialog() {
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(SignUpComponent, {
      height: "auto",
      width: "600px",
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  GetLoginDetails() {
    debugger;
    this.login = new Userdetails();
    if (this.cookie.checkcookie("login")) {
      this.login = JSON.parse(this.cookie.getcookie("login"));
    } else {
      this.login.retCode == 0;
      //this.router.navigate["/home"];
    }
  }

  signout() {
    debugger;
    this.userService.clearSession("LoginDetails");
    if (this.cookie.checkcookie("login")) {
      this.cookie.deletecookie("login");
    }
    window.location.href = "/home";
  }
}
