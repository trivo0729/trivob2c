import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ApiUrlService } from "src/app/services/api-url.service";
import { AlertService } from "src/app/services/alert.service";
declare var Razorpay: any;
@Component({
  selector: "app-ideal-payment",
  templateUrl: "./ideal-payment.component.html",
  styleUrls: ["./ideal-payment.component.css"],
})
export class IdealPaymentComponent implements OnInit {
  paymentfrm: FormGroup;
  submitted: boolean;
  rzp1: any;
  options: any;
  constructor(
    private formBuilder: FormBuilder,
    private apiUrl: ApiUrlService,
    private alert: AlertService
  ) {}
  ngOnInit() {
    this.submitted = false;
    this.paymentfrm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required]],
      mobile: ["", [Validators.required]],
      address: ["", [Validators.required]],
      amount: ["", [Validators.required]],
    });
  }

  get f() {
    return this.paymentfrm.controls;
  }

  public CheckValidation(): void {
    this.submitted = true;
    // stop here if form is invalid
    if (this.paymentfrm.invalid) {
      return;
    } else {
      this.RazorPay();
    }
  }

  RazorPay() {
    var total = this.paymentfrm.controls.amount.value;
    total = (parseFloat(total) * 100).toFixed(2).toString();
    total = parseInt(total);
    this.options = {
      key: this.apiUrl.RazorpayKey,
      currency: "INR",
      amount: total, // 2000 paise = INR 20
      name: this.apiUrl.CompanyName,
      description: "Transfer",
      image: this.apiUrl.logo,
      handler: function (response) {
        window.alert("Payment Success");
        window.location.href = "http://idealtours.in";
      },
      prefill: {
        name: this.paymentfrm.controls.name.value,
        email: this.paymentfrm.controls.email.value,
      },
      notes: {
        address: this.paymentfrm.controls.address.value,
      },
      theme: {
        color: "#FF8000",
      },
    };
    this.rzp1 = new Razorpay(this.options);
    this.rzp1.open();
  }
}
