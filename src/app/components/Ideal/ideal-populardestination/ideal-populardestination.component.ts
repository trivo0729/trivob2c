import { Component, OnInit, Input } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-ideal-populardestination',
  templateUrl: './ideal-populardestination.component.html',
  styleUrls: ['./ideal-populardestination.component.css']
})
export class IdealPopulardestinationComponent implements OnInit {
  Packages : any[];
 @Input() Name : string;
  constructor(private httpClient: HttpClient, private session :SessionService,) { }

  ngOnInit() {
    debugger;
    var Name = this.session.get("Name");
    if(Name == undefined || Name=="")
        this.session.set("Name",JSON.stringify({"Name":"AUSTRALIAN WONDER"}))
     this.Name = this.session.get("Name").Name;
    this.load();
  }
  load() {
    debugger;
    var temp = [];
    this.Packages = [];
    var arrpackages = [];
    var arritinerary =[];
    this.getHotelsfromjson().subscribe((res) => {
      temp = res.Packages;
      temp.filter((d) => {
        if (d.Name == this.Name) this.Packages.push(d);
      });
      arrpackages = this.Packages;
      arritinerary = this.Packages[0].Itinerary;
    });
  }
  getHotelsfromjson(): Observable<any> {
    return this.httpClient.get("./assets/json/package.json");
  }
}
