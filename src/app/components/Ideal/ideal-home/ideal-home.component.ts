import { Component, OnInit } from '@angular/core';
import { SessionService } from 'src/app/services/session.service';

@Component({
  selector: 'app-ideal-home',
  templateUrl: './ideal-home.component.html',
  styleUrls: ['./ideal-home.component.css']
})
export class IdealHomeComponent implements OnInit {
  Name:string;
  bHome :boolean = false;
  constructor(private session :SessionService,) { }

  ngOnInit() {
    var Name = this.session.get("Name");
    if(Name == undefined || Name=="")
        this.session.set("Name",JSON.stringify({"Name":"AUSTRALIAN WONDER"}))
     this.Name = this.session.get("Name").Name;

    if(window.location.pathname.length ==1 || window.location.pathname.includes("home"))
    this.bHome = true;
    else
     this.bHome = false;
  }

}
