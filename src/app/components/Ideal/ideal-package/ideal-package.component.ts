import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { PackageService } from "src/app/services/packages/package.service";
import {
  Package,
  Themes,
  Categories,
} from "src/app/modals/packages/package.modal";
import { UserService } from "src/app/services/user.service";
import { ActivatedRoute } from "@angular/router";
import { NgbPaginationConfig } from "@ng-bootstrap/ng-bootstrap";
import { NgxUiLoaderService } from "ngx-ui-loader";
@Component({
  selector: "app-ideal-package",
  templateUrl: "./ideal-package.component.html",
  styleUrls: ["./ideal-package.component.css"],
  providers: [NgbPaginationConfig],
})
export class IdealPackageComponent implements OnInit {
  AdminID: number;
  Type: string;
  City: string;
  img: string;
  ID: any;
  PackageName: string;
  TempPackages: Package[];
  Packages: Package[];
  page: number = 1;
  Themes: Themes[];
  Categories: Categories[];
  PackagePrice: Array<number>;
  min: any;
  max: any;
  selectedValue: string;
  TempPrice: string;
  minimum: Array<number>;
  userdata: any;
  response: boolean;
  pageSize = 15;

  constructor(
    public packageService: PackageService,
    private userService: UserService,
    private cd: ChangeDetectorRef,
    private router: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
  ) { }

  ngOnInit() {
    this.ngxService.start();
    this.response = false;
    this.Packages = [];
    debugger;
    this.Type = this.router.snapshot.queryParamMap.get("Type");
    this.City = this.router.snapshot.queryParamMap.get("City");
    this.ID = this.router.snapshot.queryParamMap.get("PackageID");
    this.ngxService.start();
    if (
      (this.Type == "" || this.Type == null) &&
      (this.City == "" || this.City == null) &&
      (this.ID == "" || this.ID == null)
    ) {
      this.packageService
        .getPackage(this.userService.getUserdata())
        .subscribe((res: Package[]) => {
          this.ngxService.stop();
          this.response = true;
          console.log(res);
          this.Packages = res;
          this.TempPackages = res;
          this.cd.detectChanges();
          if (this.Packages[0].Images.length != 0)
            this.img = this.Packages[0].Images[0].Url;
        });
    }
    if (this.Type != "" && this.Type != null) {
      this.packageService
        .SearchPackagesByType(this.userService.getUserdata(), this.Type)
        .subscribe((res: Package[]) => {
          this.ngxService.stop();
          this.response = true;
          this.Packages = res;
          this.TempPackages = res;
          console.log(this.Packages);
          this.cd.detectChanges();
          if (this.Packages[0].Images.length != 0)
            this.img = this.Packages[0].Images[0].Url;
          this.setPrice();
        });
    }
    if (this.City != "" && this.City != null) {
      this.packageService
        .SearchPackagesByCity(this.City, this.userService.getUserdata())
        .subscribe((res: Package[]) => {
          this.ngxService.stop();
          this.response = true;
          this.Packages = res;
          this.TempPackages = res;
          console.log(this.Packages);
          this.cd.detectChanges();
          if (this.Packages[0].Images.length != 0)
            this.img = this.Packages[0].Images[0].Url;
          this.setPrice();
        });
    }
    if (this.ID != "" && this.ID != null) {
      this.packageService.getPackageDetail(this.ID).subscribe((res: any) => {
        this.ngxService.stop();
        this.response = true;
        this.ngxService.stop();
        this.Packages = res;
        this.TempPackages = res;
        this.cd.detectChanges();
        if (this.Packages[0].Images.length != 0)
          this.img = this.Packages[0].Images[0].Url;
        this.setPrice();
      });
    }
  }
  setPrice() {
    try {
      debugger;
      this.Packages = [];
      this.TempPackages.forEach((arrPackage) => {
        this.minimum = [];
        if (arrPackage.Category.length != 0) {
          if (arrPackage.Category[0].Double != 0) {
            this.minimum.push(arrPackage.Category[0].Double);
          }
          if (arrPackage.Category[0].Single != 0) {
            this.minimum.push(arrPackage.Category[0].Single);
          }
          if (arrPackage.Category[0].Triple != 0) {
            this.minimum.push(arrPackage.Category[0].Triple);
          }
          if (arrPackage.Category[0].Quad != 0) {
            this.minimum.push(arrPackage.Category[0].Quad);
          }
          if (arrPackage.Category[0].Quint != 0) {
            this.minimum.push(arrPackage.Category[0].Quint);
          }
          if (this.minimum != undefined && this.minimum.length != 0)
            arrPackage.MinPrice = this.minimum.reduce((a, b) => Math.min(a, b));
        } else {
          arrPackage.MinPrice = 0;
        }

        //this.min =
        this.Packages.push(arrPackage);
      });
      this.TempPackages = this.Packages;
    } catch (e) {
      console.log(e.message);
      this.Packages = this.TempPackages;
    }
  }
}
