import { Component, OnInit } from '@angular/core';

import { ApiUrlService } from 'src/app/services/api-url.service';
import { SessionService } from 'src/app/services/session.service';
@Component({
  selector: 'app-ideal-footer',
  templateUrl: './ideal-footer.component.html',
  styleUrls: ['./ideal-footer.component.css']
})
export class IdealFooterComponent implements OnInit {
  bHome: boolean = true;
  constructor(
    private session :SessionService,
  ) { }

  ngOnInit() {
    debugger
    console.log( window.location.pathname); //  /routename
    if(window.location.pathname.length ==1 || window.location.pathname.includes("home"))
       {
        this.bHome = true;
        this.session.set("Name",JSON.stringify({"Name":"AUSTRALIAN WONDER"}))
       }
    else
          this.bHome = false;
  }
 
  setDeatination(detiny:string){
    debugger;
    try{
      this.session.set("Name",JSON.stringify({"Name": detiny}))
      window.location.href="/popular_destination";
    }catch(e){}
  }
}
