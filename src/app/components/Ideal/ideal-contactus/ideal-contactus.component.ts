import { Component, OnInit } from '@angular/core';
import { ApiUrlService } from 'src/app/services/api-url.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import {Contact} from 'src/app/modals/contact.modal'; 
import { ContactService } from 'src/app/services/contact.service';
import { AlertService } from 'src/app/services/alert.service';
declare function MapDesign(lat: any, lng: any, logo: string, address: string): any;
@Component({
  selector: 'app-ideal-contactus',
  templateUrl: './ideal-contactus.component.html',
  styleUrls: ['./ideal-contactus.component.css']
})
export class IdealContactusComponent implements OnInit {
  contactfrm: FormGroup;
  submitted = false;
  contact: Contact;
  AdminCode: any;
  img: any;
  

  constructor(
    private formBuilder: FormBuilder,
    private ContactService: ContactService,
    private objGlobalService: ApiUrlService,
    private Alert: AlertService
  ) { 

  }

  ngOnInit() {
    debugger;
    this.AdminCode = this.objGlobalService.Code;
    this.contact = new Contact();
    try {
      MapDesign(this.objGlobalService.lat, this.objGlobalService.lng, this.objGlobalService.logo, this.objGlobalService.address);

    } catch (error) {

    }

    this.contact.AdminID = this.objGlobalService.AdminID;
    this.contactfrm = this.formBuilder.group({
      Firstname: ['', Validators.required],
      Lastname: ['', Validators.required],
      Email: ['', [Validators.required, Validators.email]],
      Phone: ['', Validators.required],
      Message: ['', [Validators.required]],
    });

  }


  get f() { return this.contactfrm.controls; }


  public CheckValidation(): void {
    debugger
    this.submitted = true;
    // stop here if form is invalid
    if (this.contactfrm.invalid) {
      return;
    }
    else {
      this.SendContactinfo();
    }
  }
  SendContactinfo() {
    debugger;
    this.ContactService.sendContactinfo(this.contact).subscribe((res: any) => {
      if (res.retCode == 1) {
        this.Alert.succsess('Your request sent! ', 'We will get in touch with you soon.', 'success', function () {
          window.location.href = 'home'
        });
        this.submitted = false;
        this.contactfrm.reset();
      }
      else {
        this.Alert.succsess('Sorry!', 'request Not sent', 'warning', function () {
        });
      }
    })
  }



}
