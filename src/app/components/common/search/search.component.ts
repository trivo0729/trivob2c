import { Component, OnInit } from '@angular/core';
import { Traveller, objTraveller, Childs } from 'src/app/modals/hotel/search.modal';
import * as moment from 'moment';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  public listItems: Array<string> = [
    'Baseball', 'Basketball', 'Cricket', 'Field Hockey',
    'Football', 'Table Tennis', 'Tennis', 'Volleyball'
];

public value = ['Basketball', 'Cricket'];
  constructor(
    
  ) { }

  Traveller: Traveller[];
  objTraveller: objTraveller;
  public Ages: Array<any>;
  Childs: Childs[];
  ngOnInit() {
    this.Traveller = [];
    this.Childs = [];
    this.Ages = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    this.objTraveller = new objTraveller();
    this.objTraveller.Location = '';
    var start = new Date();
    var end = new Date();
    end.setDate(end.getDate() + 1);
    this.objTraveller.Date = {
      start: moment(start),
      end: moment(end),
    }
    this.Traveller.push({ adults: 1, childs: this.Childs });
  }

}
