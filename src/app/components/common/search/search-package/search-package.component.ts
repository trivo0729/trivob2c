import { Component, OnInit } from '@angular/core';
import { PackageCities, PackageNames } from 'src/app/modals/tours.modals';
import { FormControl } from '@angular/forms';
import { PackageService } from 'src/app/services/packages/package.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/services/alert.service';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';
import { Package } from 'src/app/modals/packages/package.modal';

@Component({
  selector: 'app-search-package',
  templateUrl: './search-package.component.html',
  styleUrls: ['./search-package.component.css']
})
export class SearchPackageComponent implements OnInit {
  PackageCity: PackageCities[];
  filterCities: Observable<PackageCities[]>;
  cityCtrl = new FormControl();
  Package: Package[];
  PackageID: any;
  filterpackageName: Observable<PackageNames[]>;
  NameCtrl = new FormControl();
  arrPackagname: string[];
  PackageName: PackageNames[];
  arrpackageID: any[];


  constructor(
    private router: Router, private packageService: PackageService,
    public alert: AlertService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.packageService.getPackageLocation().subscribe((res: PackageCities[]) => {
      debugger;
      this.PackageCity = res;
      this.filterCities = this.cityCtrl.valueChanges
        .pipe(
          startWith(''),
          map(CityName => CityName ? this._filterCities(CityName) : this.PackageCity.slice())
        );
    });
  }

  private _filterCities(value: string): PackageCities[] {
    const filterValue = value.toLowerCase();
    return this.PackageCity.filter(CityName => CityName.City.toLowerCase().indexOf(filterValue) === 0);
  }

  getPackages(City: string) {
    debugger;
    if (City != "") {
      this.router.navigate(['/packages'], { queryParams: { City: City } });
    }
    else {
      debugger;
      this.alert.confirm('Please Enter City Name', 'warning')
    }
  }

  OnPackageSelect(value: any) {
    debugger;
    this.PackageID = value.PackageID;

  }

  getpackagebyname(Name: string) {
    debugger;
    if (Name != "") {
      this.router.navigate(['/packages'], { queryParams: { PackageID: this.PackageID } });
    }
    else {
      debugger;
      this.alert.confirm('Please Enter Package Name', 'warning')
    }


  }

}

