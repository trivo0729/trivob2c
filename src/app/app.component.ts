import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { UserComponent } from "./components/user/user.component";
import { GenralService } from "./services/genral.service";
import { Router } from "@angular/router";
import { UserService } from "./services/user.service";
import { SessionService } from "./services/session.service";
import { SignUpComponent } from "./components/user/sign-up/sign-up.component";
import { FormGroup, ValidatorFn } from "@angular/forms";
import { ApiUrlService } from "./services/api-url.service";
import { MetaServicesService } from "./services/meta-services.service";
import { Userdetails } from "./modals/user/userdetails";
import { CommonCookieService } from "./services/commons/common-cookie.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "b2c";
  alertAt = 30;
  startTimer = false;
  Userdetails: Userdetails = new Userdetails();
  public Currency: string = "AED";
  CustomerName: string = null;
  CustomerEmail: string = null;
  AdminCode: any;
  constructor(
    public dialog: MatDialog,
    public genralService: GenralService,
    private router: Router,
    public userService: UserService,
    private sessionService: SessionService,
    public objGlobalService: ApiUrlService,
    public meta: MetaServicesService,
    private cookie: CommonCookieService
  ) {}

  openDialog() {
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(UserComponent);
    dialogRef.afterClosed().subscribe((result) => {
      this.GetLoginDetails();
    });
  }
  RegisterDialog() {
    this.dialog.closeAll();
    const dialogRef = this.dialog.open(SignUpComponent, {
      height: "auto",
      width: "600px",
    });
    dialogRef.afterClosed().subscribe((result) => {});
  }

  public login: Userdetails = new Userdetails();

  ngOnInit() {
    this.login = new Userdetails();
    this.AdminCode = this.objGlobalService.Code;
    this.meta.updateTitle();
    this.GetLoginDetails();
  }
  SetCurrency(sCurrency: string) {
    this.Currency = sCurrency;
  }

  signout() {
    debugger;
    this.userService.clearSession("LoginDetails");
    if (this.cookie.checkcookie("login")) {
      this.cookie.deletecookie("login");
    }
    window.location.href = "/home";
  }

  GetLoginDetails() {
    this.login = new Userdetails();
    if (this.cookie.checkcookie("login")) {
      this.login = JSON.parse(this.cookie.getcookie("login"));
    } else {
      this.login.retCode == 0;
      this.router.navigate["/home"];
    }
  }
}

export function equalValueValidator(
  targetKey: string,
  toMatchKey: string
): ValidatorFn {
  return (group: FormGroup): { [key: string]: any } => {
    const target = group.controls[targetKey];
    const toMatch = group.controls[toMatchKey];
    if (target.touched && toMatch.touched) {
      const isMatch = target.value === toMatch.value;
      // set equal value error on dirty controls
      if (!isMatch && target.valid && toMatch.valid) {
        toMatch.setErrors({ equalValue: targetKey });
        const message = targetKey + " != " + toMatchKey;
        return { equalValue: message };
      }
      if (isMatch && toMatch.hasError("equalValue")) {
        toMatch.setErrors(null);
      }
    }

    return null;
  };
}
