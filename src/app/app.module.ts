import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { IdealHeaderComponent } from "./components/Ideal/ideal-header/ideal-header.component";
import { IdealFooterComponent } from "./components/Ideal/ideal-footer/ideal-footer.component";
import { IdealHomeComponent } from "./components/Ideal/ideal-home/ideal-home.component";
import { IdealAboutusComponent } from "./components/Ideal/ideal-aboutus/ideal-aboutus.component";
import { IdealPackageComponent } from "./components/Ideal/ideal-package/ideal-package.component";
import { IdealSliderComponent } from "./components/Ideal/ideal-slider/ideal-slider.component";
import { IdealContactusComponent } from "./components/Ideal/ideal-contactus/ideal-contactus.component";
import { IdealPaymentComponent } from "./components/Ideal/ideal-payment/ideal-payment.component";
import { IdealPopulardestinationComponent } from "./components/Ideal/ideal-populardestination/ideal-populardestination.component";
import { IdealCertificatesComponent } from "./components/Ideal/ideal-certificates/ideal-certificates.component";
import { HotelSerachComponent } from "./components/hotel/hotel-serach/hotel-serach.component";
import { HotelListComponent } from "./components/hotel/hotel-list/hotel-list.component";
import { HotelDetailsComponent } from "./components/hotel/hotel-details/hotel-details.component";
import { HotelBookingComponent } from "./components/hotel/hotel-booking/hotel-booking.component";
import { HotelConfirmationComponent } from "./components/hotel/hotel-confirmation/hotel-confirmation.component";
import { HotelFilterComponent } from "./components/hotel/hotel-filter/hotel-filter.component";
import { HotelInvoiceComponent } from "./components/hotel/hotel-invoice/hotel-invoice.component";
import { HotelMapComponent } from "./components/hotel/hotel-map/hotel-map.component";
import { HotelTermNConditionsComponent } from "./components/hotel/hotel-term-n-conditions/hotel-term-n-conditions.component";
import { HotelVoucherComponent } from "./components/hotel/hotel-voucher/hotel-voucher.component";
import { RateGroupComponent } from "./components/hotel/rate-group/rate-group.component";
import { SearchPanelComponent } from "./components/hotel/search-panel/search-panel.component";
import { TravellerComponent } from "./components/hotel/traveller/traveller.component";
import { TravellerDetailsComponent } from "./components/hotel/traveller-details/traveller-details.component";
import { PackageSearchComponent } from "./components/package/package-search/package-search.component";
import { PackageBookingComponent } from "./components/package/package-booking/package-booking.component";
import { PackageDetailsComponent } from "./components/package/package-details/package-details.component";
import { PackageDocumentsComponent } from "./components/package/package-documents/package-documents.component";
import { PackageGridComponent } from "./components/package/package-grid/package-grid.component";
import { PackageListComponent } from "./components/package/package-list/package-list.component";
import { FlightComponent } from "./components/flight/flight/flight.component";
import { FlightBookingComponent } from "./components/flight/flight-booking/flight-booking.component";
import { FlightConfirmComponent } from "./components/flight/flight-confirm/flight-confirm.component";
import { FlightFilterComponent } from "./components/flight/flight-filter/flight-filter.component";
import { FlightInvoiceComponent } from "./components/flight/flight-invoice/flight-invoice.component";
import { FlightListComponent } from "./components/flight/flight-list/flight-list.component";
import { FlightTicketComponent } from "./components/flight/flight-ticket/flight-ticket.component";
import { SearchFlightComponent } from "./components/flight/search-flight/search-flight.component";
import { ConfirmationDialogComponent } from "./components/common/confirmation-dialog/confirmation-dialog.component";
import { UserComponent } from "./components/user/user.component";
import { AboutusComponent } from "./components/aboutus/aboutus.component";
import { ContactusComponent } from "./components/contactus/contactus.component";
import { HeaderComponent } from "./components/common/header/header.component";
import { FooterComponent } from "./components/common/footer/footer.component";
import { HttpModule } from "@angular/http";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { NotfoundComponent } from "./components/common/notfound/notfound.component";
import { SearchComponent } from "./components/common/search/search.component";
import { SearchHotelComponent } from "./components/common/search/search-hotel/search-hotel.component";
import { SearchPackageComponent } from "./components/common/search/search-package/search-package.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import {
  AuthServiceConfig,
  FacebookLoginProvider,
  GoogleLoginProvider,
  LinkedinLoginProvider,
  SocialLoginModule,
} from "angular-6-social-login";
import {
  MatAutocompleteModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatListModule,
  MatSelectModule,
  MAT_DATE_FORMATS,
  NativeDateModule,
  MatRippleModule,
  MatIconModule,
  MatButtonModule,
  MatButtonToggleModule,
  MAT_DATE_LOCALE,
  MatRadioModule,
  MatSliderModule,
  MatCardModule,
  MatDialogModule,
  MatTableModule,
  MatDialogRef,
  MatTabsModule,
  MatCheckboxModule,
  MatDividerModule,
  MatSlideToggleModule,
  MatTreeModule,
  MAT_RADIO_GROUP_CONTROL_VALUE_ACCESSOR,
  MatTooltipModule,
  MatChipsModule,
  MatExpansionModule,
} from "@angular/material";
import { Daterangepicker } from "ng2-daterangepicker";
import { ToastrModule } from "ngx-toastr";
import { SweetAlert2Module } from "@sweetalert2/ngx-sweetalert2";
//import { AgmCoreModule } from "@agm/core";
import {
  NgbAlertModule,
  NgbModule,
  NgbPaginationModule,
} from "@ng-bootstrap/ng-bootstrap";

import { NgxSkeletonLoaderModule } from "ngx-skeleton-loader";
import { StarRatingsComponent } from "./components/common/star-ratings/star-ratings.component";
import { ModifySearchComponent } from "./components/common/search/modify-search/modify-search.component";
import { DatePipe } from "@angular/common";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { FlightModifySearchComponent } from "./components/flight/flight-modify-search/flight-modify-search.component";
import { SliderComponent } from "./components/package/slider/slider.component";
import { ForgetPasswordComponent } from "./components/user/forget-password/forget-password.component";
import { SignUpComponent } from "./components/user/sign-up/sign-up.component";
import { ConfirmComponent } from "./components/common/confirm/confirm.component";
import { HotellistLoderComponent } from "./components/common/hotel/hotellist-loder/hotellist-loder.component";
import { HotelfilterLoderComponent } from "./components/common/hotel/hotelfilter-loder/hotelfilter-loder.component";
import { BookingReportComponent } from "./components/user/booking-report/booking-report.component";
import { FlightBookingsComponent } from "./components/user/booking-report/flight-bookings/flight-bookings.component";
import { HotelBookingsComponent } from "./components/user/booking-report/hotel-bookings/hotel-bookings.component";
import { TourBookingsComponent } from "./components/user/booking-report/tour-bookings/tour-bookings.component";
import { FlightBookingDetailsComponent } from "./components/user/booking-details/flight-booking-details/flight-booking-details.component";
import { BookingDetailsComponent } from "./components/user/booking-details/booking-details.component";
import { PDFExportModule } from "@progress/kendo-angular-pdf-export";
import { CookieService } from "ngx-cookie-service";
import { ChangepasswordComponent } from "./components/user/changepassword/changepassword.component";
import { ProfileComponent } from "./components/user/profile/profile.component";
import { PackageBookingsComponent } from "./components/user/booking-report/package-bookings/package-bookings.component";
import { BookingsComponent } from "./components/user/bookings/bookings.component";
import { SightseeingBookingDetailsComponent } from "./components/user/booking-details/sightseeing-booking-details/sightseeing-booking-details.component";
export function getAuthServiceConfigs() {
  // click=  "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com"
  // Cennin=  "165521631707-0hkt0646vk1u99b5q28okrjbht8lsfpi.apps.googleusercontent.com"

  let config = new AuthServiceConfig([
    {
      id: FacebookLoginProvider.PROVIDER_ID,
      // cennin fb 481632676030440
      // Hi5fly fb 854656805008809
      provider: new FacebookLoginProvider("854656805008809"),
    },
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      //provider: new GoogleLoginProvider("")
      // cennin google 165521631707-0hkt0646vk1u99b5q28okrjbht8lsfpi.apps.googleusercontent.com
      // Hi5Fly google 89635597811-qc7bqmuhgtlmbmrafs62237aapi3dees.apps.googleusercontent.com
      provider: new GoogleLoginProvider(
        "89635597811-407262n7ibn33mu8k02qqe6jk4m4kos9.apps.googleusercontent.com"
      ),
    },
  ]);
  return config;
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    IdealHeaderComponent,
    IdealFooterComponent,
    IdealHomeComponent,
    IdealAboutusComponent,
    IdealPackageComponent,
    IdealSliderComponent,
    IdealContactusComponent,
    IdealPaymentComponent,
    IdealPopulardestinationComponent,
    IdealCertificatesComponent,
    HotelSerachComponent,
    HotelListComponent,
    HotelDetailsComponent,
    HotelBookingComponent,
    HotelConfirmationComponent,
    HotelFilterComponent,
    HotelInvoiceComponent,
    HotelMapComponent,
    HotelTermNConditionsComponent,
    HotelVoucherComponent,
    RateGroupComponent,
    SearchPanelComponent,
    TravellerComponent,
    TravellerDetailsComponent,
    PackageSearchComponent,
    PackageBookingComponent,
    PackageDetailsComponent,
    PackageDocumentsComponent,
    PackageGridComponent,
    PackageListComponent,
    FlightComponent,
    FlightBookingComponent,
    FlightConfirmComponent,
    FlightFilterComponent,
    FlightInvoiceComponent,
    FlightListComponent,
    FlightTicketComponent,
    SearchFlightComponent,
    ConfirmationDialogComponent,
    UserComponent,
    ForgetPasswordComponent,
    SignUpComponent,
    ConfirmationDialogComponent,
    ConfirmComponent,
    AboutusComponent,
    ContactusComponent,
    HeaderComponent,
    FooterComponent,
    NotfoundComponent,
    SearchComponent,
    SearchHotelComponent,
    SearchPackageComponent,
    FlightModifySearchComponent,
    BookingReportComponent,
    FlightBookingsComponent,
    HotelBookingsComponent,
    TourBookingsComponent,
    FlightBookingDetailsComponent,
    BookingDetailsComponent,
    SliderComponent,
    StarRatingsComponent,
    ModifySearchComponent,
    FlightModifySearchComponent,
    HotellistLoderComponent,
    HotelfilterLoderComponent,
    ChangepasswordComponent,
    ProfileComponent,
    PackageBookingsComponent,
    TourBookingsComponent,
    BookingsComponent,
    SightseeingBookingDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    MatAutocompleteModule,
    Daterangepicker,
    MatRadioModule,
    MatCheckboxModule,
    NgxUiLoaderModule,
    SocialLoginModule,
    MatSlideToggleModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    ToastrModule.forRoot(), // ToastrModule added
    // AgmCoreModule.forRoot({
    //   apiKey: "AIzaSyDLvhAriXSRStAxraxjCp1GtClM4slLh-k"
    // }),
    NgbPaginationModule,
    NgbAlertModule,
    NgbModule,
    MatDialogModule,
    MatCheckboxModule,
    MatSelectModule,
    MatExpansionModule,
    NgxSkeletonLoaderModule,
    PDFExportModule,
    SweetAlert2Module.forRoot(),
  ],
  entryComponents: [
    UserComponent,
    ForgetPasswordComponent,
    SignUpComponent,
    ConfirmationDialogComponent,
    ConfirmComponent,
    HotelTermNConditionsComponent,
  ],
  providers: [
    DatePipe,
    CookieService,
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
