import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { AboutusComponent } from "./components/aboutus/aboutus.component";
import { ContactusComponent } from "./components/contactus/contactus.component";
import { PackageListComponent } from "./components/package/package-list/package-list.component";
import { PackageDetailsComponent } from "./components/package/package-details/package-details.component";
import { PackageBookingComponent } from "./components/package/package-booking/package-booking.component";
import { HotelListComponent } from "./components/hotel/hotel-list/hotel-list.component";
import { HotelBookingComponent } from "./components/hotel/hotel-booking/hotel-booking.component";
import { SearchComponent } from "./components/common/search/search.component";
import { SearchHotelComponent } from "./components/common/search/search-hotel/search-hotel.component";
import { HotelSerachComponent } from "./components/hotel/hotel-serach/hotel-serach.component";
import { FlightListComponent } from "./components/flight/flight-list/flight-list.component";
import { FlightComponent } from "./components/flight/flight/flight.component";
import { FlightBookingComponent } from "./components/flight/flight-booking/flight-booking.component";
import { BookingReportComponent } from "./components/user/booking-report/booking-report.component";
import { FlightBookingDetailsComponent } from "./components/user/booking-details/flight-booking-details/flight-booking-details.component";
import { BookingDetailsComponent } from "./components/user/booking-details/booking-details.component";
import { FlightConfirmComponent } from "./components/flight/flight-confirm/flight-confirm.component";
import { HotelConfirmationComponent } from "./components/hotel/hotel-confirmation/hotel-confirmation.component";
import { ProfileComponent } from "./components/user/profile/profile.component";
import { ChangepasswordComponent } from "./components/user/changepassword/changepassword.component";
import { BookingsComponent } from "./components/user/bookings/bookings.component";
import { IdealPaymentComponent } from "./components/Ideal/ideal-payment/ideal-payment.component";
import {IdealPopulardestinationComponent} from "./components/Ideal/ideal-populardestination/ideal-populardestination.component";
const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
    data: {
      title: "Home",
      description: "",
    },
  },
  {
    path: "home",
    component: HomeComponent,
    data: {
      title: "Home",
      description: "",
    },
  },
  {
    path: "aboutus",
    component: AboutusComponent,
    data: {
      title: "About Us",
      description: "",
    },
  },
  {
    path: "contactus",
    component: ContactusComponent,
    data: {
      title: "Contact us",
      description: "",
    },
  },
  {
    path: "packages",
    component: PackageListComponent,
    data: {
      title: "Packages",
      description: "",
    },
  },
  {
    path: "packagesdetails",
    component: PackageDetailsComponent,
    data: {
      title: "Package Details",
      description: "",
    },
  },
  {
    path: "packagebooking",
    component: PackageBookingComponent,
    data: {
      title: "Package Booking",
      description: "",
    },
  },
  {
    path: "hotels",
    component: HotelListComponent,
    data: {
      title: "hotels",
      description: "",
    },
  },
  {
    path: "hotel-booking",
    component: HotelBookingComponent,
    data: {
      title: "Hotel Booking",
      description: "",
    },
  },
  {
    path: "search",
    component: SearchComponent,
    data: {
      title: "Search",
      description: "",
    },
  },
  {
    path: "search-hotel",
    component: HotelSerachComponent,
    data: {
      title: "Search",
      description: "",
    },
  },

  /* Flight Routings*/
  {
    path: "flight-list",
    component: FlightListComponent,
    data: {
      title: "flights",
      description: "",
    },
  },
  {
    path: "flight-search",
    component: FlightComponent,
    data: {
      title: "flight-search",
      description: "",
    },
  },
  {
    path: "flight-booking",
    component: FlightBookingComponent,
    data: {
      title: "flight booking",
      description: "",
    },
  },
  {
    path: "flight-confirm",
    component: FlightConfirmComponent,
    data: {
      title: "flight booking confirmation",
      description: "",
    },
  },

  {
    path: "hotel-list",
    component: HotelListComponent,
    data: {
      title: "Hotel List",
      description: "",
    },
  },

  {
    path: "hotel-confirmation",
    component: HotelConfirmationComponent,
    data: {
      title: "Hotel Confirmation",
      description: "",
    },
  },
  // * bookings* //

  {
    path: "booking-report",
    component: BookingReportComponent,
    data: {
      title: "Bookings",
      description: "",
    },
  },
  {
    path: "flight-booking-details",
    component: FlightBookingDetailsComponent,
    data: {
      title: "flight booking details",
      description: "",
    },
  },
  {
    path: "booking-details",
    component: BookingDetailsComponent,
    data: {
      title: "hotel booking details",
      description: "",
    },
  },
  {
    path: "profile",
    component: ProfileComponent,
    data: {
      title: "User Profile",
      description: "",
    },
  },
  {
    path: "change-password",
    component: ChangepasswordComponent,
    data: {
      title: "change password",
      description: "",
    },
  },
  {
    path: "bookings",
    component: BookingsComponent,
    data: {
      title: "Bookings",
      description: "",
    },
  },
  {
    path: "payment",
    component: IdealPaymentComponent,
    data: {
      title: "Payment",
      description: "",
    },
  }, 
  {
    path: "popular_destination",
    component: IdealPopulardestinationComponent,
    data: {
      title: "Ideal Popular Destination",
      description: "",
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
