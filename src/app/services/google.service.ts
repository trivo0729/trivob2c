import { Injectable } from '@angular/core';
declare const google: any;
declare function getInfoBox(item): any;
declare function closeInfoBox(): any;
//import * as $ from 'jquery';
import { Tours } from '../modals/tours.modals';
@Injectable({
  providedIn: 'root'
})
export class GoogleService {
  constructor() { }
  mapObject: any;
  markersData: any;
  markers: [];
  loadmap(tours: Tours[]) {
    try {
      this.GetMapData(tours);
      var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(25.2048, 55.2708),
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.LARGE,
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        scrollwheel: false,
        scaleControl: false,
        scaleControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        streetViewControl: true,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        },
      };
      var
        marker;
      var mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
      //for (var key in this.markersData)
      this.markersData.forEach(function (item) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
          map: mapObject,
          icon: '../../../assets/img/pins/Sightseeing.png',
        });
        google.maps.event.addListener(marker, 'click', (function () {
          closeInfoBox();
          getInfoBox(item).open(mapObject, this);
          mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
        }));
      });
    } catch (e) {
      console.log(e);
    }
  }

  loadsinglemap(tours: Tours[]) {
    try {
      this.GetMapData(tours);
      var mapOptions = {
        zoom: 10,
        center: new google.maps.LatLng(parseFloat(tours[0].LocationDetail.Latitude), parseFloat(tours[0].LocationDetail.Longitutde)),
        // center: new google.maps.LatLng(25.2048, 55.2708),
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.LARGE,
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        scrollwheel: false,
        scaleControl: false,
        scaleControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        streetViewControl: true,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        },
      };
      var
        marker;
      var mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
      //for (var key in this.markersData)
      this.markersData.forEach(function (item) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
          map: mapObject,
          icon: '../../../assets/img/pins/Sightseeing.png',
        });
        google.maps.event.addListener(marker, 'click', (function () {
          closeInfoBox();
          getInfoBox(item).open(mapObject, this);
          mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
        }));
      });
    } catch (e) {
      console.log(e);
    }
  }

  GetMapData(tours: Tours[]) {
    try {
      this.markersData = new Array();
      console.log(tours);
      tours.forEach(tour => {
        this.markersData.push({
          id: tour.ActivityID,
          name: tour.LocationDetail.LocationName,
          get_directions_start_address: tour.LocationDetail.LocationName,
          location_latitude: tour.LocationDetail.Latitude,
          location_longitude: tour.LocationDetail.Longitutde,
          map_image_url: tour.ListImage.Url,
          name_point: tour.Name,
          description_point: "",// tour.Description,
          url_point: 'single_tour.html',
          phone: '+971-(0)564660675'
        });
      });
    } catch (e) { }
  }

}
