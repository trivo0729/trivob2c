import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SessionService } from "../session.service";
import { ApiUrlService } from "../api-url.service";
import { HttpHeaders, HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ReportService {
  AdminID: number;
  Url: string;
  constructor(
    private httpClient: HttpClient,
    private ServiceUrl: ApiUrlService,
    private sessionService: SessionService
  ) {
    this.Url = this.ServiceUrl.HotelReportUrl;
    this.AdminID = this.ServiceUrl.AdminID;
  }

  /*Reporting Start*/

  getHotelBooking(UserId: number, Email: any): any {
    return this.httpClient.post<any>(
      this.Url + "HotelBookingList",
      {
        UserId: UserId,
        Email: Email,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  // getHotelBooking(): any {
  //   return this.httpClient.get("./assets/json/hotel-bookings.json");
  // }

  GetHotelInvoice(UserId: any, ReservationID: any): Observable<any> {
    return this.httpClient.post<any>(
      this.Url + "GetHotelInvoice",
      {
        UserId: UserId,
        ReservationID: ReservationID,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  GetHotelVoucher(UserId: any, ReservationID: any): Observable<any> {
    return this.httpClient.post<any>(
      this.Url + "GetHotelVoucher",
      {
        UserId: UserId,
        ReservationID: ReservationID,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  getPackageBooking(Id: number): any {
    return this.httpClient.post<any>(
      this.ServiceUrl.PackageReportUrl + "PackageBookingList",
      {
        UserId: Id,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  /*Activity Reporting Start*/

  getActivityBooking(ReservationID: number, Uid: number): any {
    return this.httpClient.post<any>(
      this.ServiceUrl.TourUrl + "/GetActivityBooking",
      {
        ReservationID: ReservationID,
        Uid: Uid,
        ParentID: this.ServiceUrl.AdminID,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }
  /* Flight Booking Start */
  getflightBooking(Id: number, Email: any) {
    return this.httpClient.post<any>(
      this.ServiceUrl.FlightUrl + "/FlightBookingList",
      {
        UserId: Id,
        Email: Email,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  // getflightBooking() {
  //   return this.httpClient.get("./assets/json/flight-bookings.json");
  // }

  getflightBookingdetails() {
    return this.httpClient.get("./assets/json/FlightBookingDetails.json");
  }
}
