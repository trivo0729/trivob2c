import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { ApiUrlService } from '../api-url.service';
import { UserService } from '../user.service';
import { MenuService } from './menu.service';
import { CommonCookieService } from './common-cookie.service';

@Injectable({
  providedIn: 'root'
})

export class RouteAuthorizationService implements CanActivate {
  UsetId: number;
  isAuth: boolean;
  constructor(private objGlobalService: ApiUrlService,
    private userService: UserService,
    private menuService: MenuService ,
    private cookie : CommonCookieService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    debugger;
    // this.isAuth = this.authService.getIsAuth();
    // if (!isAuth) {
    //   this.router.navigate(['/login']);
    // }
    return true;
  }

  getManu() {
    debugger;
    this.menuService.getMenu(this.getUserId()).subscribe((res: any) => {
      console.log(res);
    }, error => {
      console.log(error);
    });
  }

  getUserId() {
    debugger;
    if (this.cookie.checkcookie('login')) {
      let data = JSON.parse(this.cookie.getcookie('login'));
      if (data !== null) {
        if (data.LoginDetail.UserType === "B2B") {
          this.UsetId = data.LoginDetail.sid;
        }
        if (data.LoginDetail.UserType === "B2C") {
          this.UsetId = this.objGlobalService.UserId;
        }
      }
    }
    else {
      this.UsetId = this.objGlobalService.UserId;
    }
    return this.UsetId;
  }
}
