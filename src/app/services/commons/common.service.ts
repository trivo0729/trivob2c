import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Wishlist } from 'src/app/modals/common/wishlist.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from '../session.service';
import { ApiUrlService } from '../api-url.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  Parent: number;
  Url: string;

  constructor(private httpClient: HttpClient, private sessionService: SessionService,
    private ServiceUrl: ApiUrlService) {
    this.Url = this.ServiceUrl.CartUrl;
    this.Parent = this.ServiceUrl.AdminID;
  }

  /*Addto Wish*/
  AddtoWishList(arrWish: Wishlist): Observable<any> {
    return this.httpClient.post<Wishlist>(this.Url + "AddtoWishList",
      {
        arrWish
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Remove Wish*/
  RemoveWish(ProductType: number, ProductID: number, UserName: string): Observable<any> {
    return this.httpClient.post<Wishlist>(this.Url + "RemoveWish",
      {
        ProductType,
        ProductID,
        UserName
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /* Wishs*/
  CheckWish(UserName: string): Observable<any> {
    return this.httpClient.post<Wishlist>(this.Url + "CheckExpreWish",
      {
        UserName
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getTimeZone() {
    debugger
    return this.httpClient.get("./assets/json/time-zone.json");
  }

  getPaymentgateway() {
    return this.httpClient.post<Wishlist>(this.ServiceUrl.PayUrl + "GetPaymentPaymentGetWay",
      {
        ParentID: this.ServiceUrl.AdminID
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  PaymentInstamojo(name: any, email: any, MobileNo: any, Amount: any) {
    return this.httpClient.post<Wishlist>(this.ServiceUrl.PayUrl + "PaymentInstamojo",
      {
        ParentrID: this.ServiceUrl.AdminID,
        name: name,
        email: email,
        MobileNo: MobileNo,
        Amount: Amount,
        RedirectUrl: this.ServiceUrl.RedirectUrl
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

}
