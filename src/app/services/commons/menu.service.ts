import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from '../api-url.service';
import { Observable } from 'rxjs';
import { SessionService } from '../session.service';


@Injectable({
  providedIn: 'root'
})
export class MenuService {
  Url: string;
  UsetId: number;
  constructor(private http: HttpClient, private apiUrl: ApiUrlService, private sessionService: SessionService, ) {
    this.Url = this.apiUrl.MenuAuthUrl;
    this.UsetId = this.apiUrl.UserId;
  }

  getMenu(UsetId: number): Observable<any> {
    return this.http.post<any>(this.Url + '_getMenus',
      {
        uid: UsetId,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  // * set menu in session * //

  setMenu(data: any) {
    try {
      this.sessionService.set("_Menus", JSON.stringify(data));
    } catch (e) { }
  }

  get_Menu() {
    return this.sessionService.get("_Menus");
  }
}
