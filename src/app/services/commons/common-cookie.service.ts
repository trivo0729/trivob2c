import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CommonCookieService {
  constructor(private cookieService: CookieService) { }

  checkcookie(name: string) {
    const cookieExists: boolean = this.cookieService.check(name);
    return cookieExists;
  }

  setcookie(name: string, value: string, expires: any) {
    this.cookieService.set(name, value, expires);
  }

  getcookie(name: string) {
    return this.cookieService.get(name);
  }

  deletecookie(name: string) {
    this.cookieService.delete(name);
  }


}
