import { Injectable } from "@angular/core";
import { ConfirmationDialogComponent } from "src/app/components/common/confirmation-dialog/confirmation-dialog.component";
import { MatDialog } from "@angular/material";

@Injectable({
  providedIn: "root"
})
export class DialogService {
  constructor(private dialog: MatDialog) {}

  openConfirmDialog(msg: string) {
    return this.dialog.open(ConfirmationDialogComponent, {
      width: "450px",
      panelClass: "confirm-dialog-container",
      disableClose: true,
      position: { top: "50px" },
      data: {
        message: msg
      }
    });
  }
}
