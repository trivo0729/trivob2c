import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from './api-url.service';
import { Contact } from '../modals/contact.modal';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  Url: string; 
  data: Contact;
  constructor(private httpClient: HttpClient,
    private ServiceUrl: ApiUrlService){
    this.Url= this.ServiceUrl.ContactUrl;
   }

  sendContactinfo(data:any){
    debugger
    return this.httpClient.post(this.Url+"ContactMail",
    {
      AdminID: data.AdminID,
      Firstname: data.Firstname,
      Lastname : data.Lastname,
      Email:data.Email,
      Phone: data.Phone,
      Message: data.Message
    },{headers: new HttpHeaders ({'Content-Type':'application/json'})}); 
  }
}
