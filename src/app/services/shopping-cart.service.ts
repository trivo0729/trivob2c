import { Injectable } from '@angular/core';
import { SessionService } from './session.service';
import { Cart, Product } from '../modals/common/cart.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiUrlService } from './api-url.service';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {
  products: any[];
  Total:number;
  Currency:string;
  Url:string;
  Parent:Number;
  constructor(
    private sessionService:SessionService,
    private httpClient: HttpClient,
    private ServiceUrl: ApiUrlService){
      this.Url= this.ServiceUrl.CartUrl;
      this.Parent= this.ServiceUrl.AdminID;
      this.products = [];
    }
   

  /*Get All Tours*/ 
  addCart(arrCart:Cart,arrProduct:Product[]): Observable<any>{
  return this.httpClient.post<any>( this.Url+"AddtoCart",
  {
    arrCart,
    arrProduct
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

remove(CartId: number,ProductID:Number) : Observable<any>{
  debugger;
  return this.httpClient.post<any>( this.Url+ "RemoveFromCart",
  {
    CartId,
    ProductID
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

clear() {
  this.products = [];
}

get(CartId:Number) : Observable<any>{
  return this.httpClient.post<any>( this.Url+  "GetCartDetail ",
  {
    CartId,
  },{ headers: new HttpHeaders({'Content-Type': 'application/json'})});
}

setTotal()
  {
    try{
      this.Total = 0.00;
      this.Currency = "";
      this.products.forEach(sProduct => {
        if(sProduct.Type=="Tour")
        {
          this.Total += parseFloat(sProduct.PaxRate.Total);
          this.Currency = sProduct.PaxRate.Currency;
         
        }
      });
    }
    catch(ex){}
    return this.Currency + "-" + this.Total;
  }

}
