import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionService } from './session.service';
import { Review } from '../modals/reviews/review.model';
import { ApiUrlService } from './api-url.service';
@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  Parent: number = 232;
  Url: string;
  constructor(private httpClient: HttpClient, private sessionService: SessionService, private ServiceUrl: ApiUrlService) {
    this.Url = this.ServiceUrl.TourUrl;
    this.Parent = this.ServiceUrl.AdminID;
  }
  /*Get Reviews*/
  GetReviews(ProductID: number, ProducType: string): Observable<any> {
    return this.httpClient.post<any>(this.Url + "GetReviews",
      {
        ProductID,
        ProducType
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  /*Save  Reviews*/
  SaveReviews(arrReviews: Review): Observable<any> {
    debugger;
    return this.httpClient.post<any>(this.Url + "/SaveReviews",
      {
        arrProduct: arrReviews
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }
}
