import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import Swal from "sweetalert2";
@Injectable({
  providedIn: "root",
})
export class AlertService {
  constructor() {}

  confirm(message: string, Type: string) {
    debugger;
    if (Type == "error" || Type == "warning" || Type == "success")
      Swal.fire({
        type: Type,
        position: "top-end",
        title: message,
        showConfirmButton: false,
        toast: true,
        timer: 4000,
      });
    else
      Swal.fire({
        title: message,
        position: "top-end",
      });
  }

  succsess(title: string, message: string, type: string, done: any) {
    if (
      type == "success" ||
      type == "error" ||
      type == "warning" ||
      type == "info" ||
      type == "question"
    )
      Swal.fire({
        type: type,
        title: title,
        text: message,
      }).then((result) => {
        if (result.value) {
          done();
        }
      });
  }

  htmlpopup(title: string, type: string, html: any, done: any) {
    if (
      type == "success" ||
      type == "error" ||
      type == "warning" ||
      type == "info" ||
      type == "question"
    )
      Swal.fire({
        title: title,
        type: type,
        html: html,
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok",
        reverseButtons: true,
      }).then((result) => {
        if (result.value) {
          done();
        }
      });
  }
}
