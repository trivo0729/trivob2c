import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class ApiUrlService {
  arrAdminDetails: any[] = [
    {
      logo: "../assets/img/logo/trivo.png",
      AdminID: 127,
      domain: "trivo.in",
      CompanyName: "Trivo IT Solution Pvt Ltd",
      RazorpayKey: "rzp_live_gr2cFDqockHXQK",
      Code: "TRIVO",
      // UserId: 51095,
      // UserName: "sufiyan.khalid@trivo.in",
      // Password: '036E3285',
      UserId: 12725,
      UserName: "sheikh.quddus@gmail.com",
      Password: "RezLive@789",
      // UserId: 316,
      // UserName: "kashifkhan4847@yahooo.com",
      // Password: '123',
      Contact: "+91-880-6409-826",
      email: "contact@trivo.in",
      facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
      google:
        "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
      lat: 21.1485349,
      lng: 79.1161854,
      UserType: "",
      address:
        "<p>5th Floor Vishnu Complex,<br> Opposite Rahate Hospital CA Road,<br> Nagpur, Maharashtra 440008</p>",
      website: "http://b2b.trivo.in",
      RedirectUrl: "http://localhost:4200/booking-confirmation",
    },
    {
      logo: "../assets/img/logo/Cut.png",
      AdminID: 232,
      domain: "Clickurtrip.com",
      CompanyName: "ClickurTrip PVT. LTD.",
      RazorpayKey: "rzp_live_gr2cFDqockHXQK",
      Code: "CUT",
      UserId: 21755,
      UserName: "kashifkhan4847@gmail.com",
      Password: "123",
      Contact: "+91-712-2779922",
      email: "support@clickurtrip.com",
      facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
      google:
        "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
      lat: 21.148685,
      lng: 79.116173,
      website: "http://b2b.clickurtrip.com",
      address:
        "<p>ClickUrTrip.com Pvt. Ltd.<br>2nd Floor, Vishnu Complex, CA Road<br>Opp.Rahate Hospital, Juni Mangalwari<br>Nagpur - 440008, (M.H.), INDIA<br>+91-712-2779922</p>",
      RedirectUrl: "",
    },
    {
      logo: "../assets/img/logo/Cenin_sticky.png",
      AdminID: 42744,
      domain: "cenintravels.com",
      CompanyName: "CENNIN TOURS & Travels Pvt Ltd",
      RazorpayKey: "",
      Code: "CENNIN",
      UserId: 47810,
      UserName: "agent@idealtravels.com",
      Password: "0W5UNS63",
      Contact: "+91-902-8004-601/07",
      email: "info@cenintravels.com",
      lat: 21.154188,
      lng: 79.086524,
      facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
      google:
        "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
      address: "<p>7, Kingsway,<br> Nagpur, India - 440001</p>",
      website: "http://www.cenintravels.com",
      landline: "+91-712-6604-601/2",
      UserType: "",
      RedirectUrl: "",
    },
    {
      logo: "../assets/img/logo/travelsphere_main.png",
      AdminID: 38612,
      domain: "trivo.in",
      CompanyName: "Travelersphere",
      RazorpayKey: "",
      Code: "TSP",
      UserId: 316,
      UserName: "kashifkhan4847@yahooo.com",
      Password: "123",
      Contact: "00-4412499",
      address: "",
      lat: 0,
      lng: 0,
      email: "info@travelersphere.com",
      facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
      website: "http://www.cenintravels.com",
      google:
        "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
      landline: "00-4412499",
      UserType: "b2c",
      RedirectUrl: "",
    },
    {
      logo: "../assets/img/logo/exotics.png",
      AdminID: 50096,
      domain: "exoticsroutes.com",
      CompanyName: "Exotics Routes",
      RazorpayKey: "",
      Code: "EXCO",
      UserId: 52104,
      UserName: "ronak@unitedairtravel.com",
      Password: "E07BVU6A",
      Contact: "0044-7715369626",
      address:
        "UNITED AIR TRAVEL LIMITED<br> Kemp House 152-160 City Road,<br>London EC1V 2NX",
      lat: 51.52742,
      lng: -0.08881,
      UserType: "",
      email: "info@unitedairtravel.com",
      facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
      google:
        "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
      website: "http://exoticsroutes.com/",
      landline: "0044-7715369626",
      RedirectUrl: "",
    },
    // {
    //   logo: '../assets/img/Meetme/logo.png',
    //   AdminID: 47810,                            // For Meetme Demo
    //   domain: "meetmeingujarat.in",
    //   CompanyName: "meetmeingujarat",
    //   RazorpayKey: "",
    //   Code: 'MEETME',
    //   UserId: 47810,
    //   UserName: "info@cenintravels.com",
    //   Password: 'CE5OUC43',
    //   Contact: "079 27710134",
    //   email: "droyaleholiday@gmail.com",
    //   lat: 23.029213,
    //   lng: 72.570387,
    //   facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
    //   google: '429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com',
    //   address: '<p>Regd. Office Address 06 Tulsi bunglow,<br> science city road <br> sola village Ahmedabad 380060.</p>'
    // }
    {
      logo: "../assets/img/Hi5Fly/Hi5Fly.png",
      AdminID: 42768,
      domain: "hi5fly.com",
      CompanyName: "HI5FLY",
      RazorpayKey: "",
      Code: "HIFI",
      UserId: 52128,
      UserName: "reservations@hi5fly.com",
      Password: "66BM311F",
      Contact: "91 799 799 1000",
      address: "HI5FLY DIAMOND HILLS COLONY TOLICHOWKI HYDERABAD 500008.INDIA.",
      lat: 51.52742,
      lng: -0.08881,
      email: "reservations@hi5fly.com",
      facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
      google:
        "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
      website: "https://hi5fly.com/",
      landline: "91 7997991000",
      RedirectUrl: "https://hi5fly.com/booking-confirmation",
      // RedirectUrl: 'http://localhost:4200/booking-confirmation',
    },
    // ideal Jungle Safari
    // {
    //   logo: "../assets/img/ideal/logo.png",
    //   AdminID: 42744,
    //   domain: "idealjunglesafaris.com",
    //   CompanyName: "Ideal Jungle Safari",
    //   RazorpayKey: "rzp_live_M18B25XfgvEoqf",
    //   Code: "IDEALJ",
    //   UserId: 42744,
    //   UserName: "agent@idealtravels.com",
    //   Password: "0W5UNS63",
    //   Contact: "91 07030167865",
    //   address:
    //     "113, Third Floor, Golden Palace,Near Sudama Theatre WHC Road,Dharampeth Nagpur, 440010",
    //   lat: 51.52742,
    //   lng: -0.08881,
    //   email: "idealjunglesafaris@gmail.com",
    //   facbook: "2cbc4ec6a24e7110a6631208b68fd13d",
    //   google:
    //     "429923422009-qj1l4n5p2hcieknb969h48ol0umkf9hl.apps.googleusercontent.com",
    //   website: "https://www.idealjunglesafaris.com/",
    //   landline: "0712-6649413/14",
    //   RedirectUrl: "",
    // },
    {
      logo: "../assets/images/logo.png",
      AdminID: 42744,
      domain: "idealtours.in",
      CompanyName: "Ideal Tour and Travels",
      RazorpayKey: "rzp_live_M18B25XfgvEoqf",
      Code: "IDEAL",
      UserId: 42744,
      UserName: "agent@idealtravels.com",
      Password: "0W5UNS63",
      Contact: "+91 7030361288",
      address:
        " Golden Palace, WHC Road,Near Sudama Theatre, Dharampeth,Nagpur, Maharashtra 440010",
      lat: 51.52742,
      lng: -0.08881,
      email: "vacations@uniglobeidealtours.in",
      facbook: "",
      google: "",
      website: "http://idealtours.in/",
      landline: "+91 7030361288",
      RedirectUrl: "",
    },
    {
      logo: "../assets/img/logo/tripser.png",
      AdminID: 52129,
      domain: "tripser.ae",
      CompanyName: "Tripser",
      RazorpayKey: "",
      Code: "TRIVO",
      UserId: 52129,
      UserName: "khurram.iqbal@hotmail.co.uk",
      Password: " ",
      Contact: "",
      address: "",
      lat: 0,
      lng: 0,
      email: "support@tripser.ae, support@tripser.uz, support@tripser.africa",
      facbook: "",
      google: "",
      website: "http://tripser.ae/",
      landline: "",
      RedirectUrl: "",
    },
  ];
  logo: string = "";
  domain: string = "";
  CompanyName: string = "";
  RazorpayKey: string = "";

  facebookkey: string = "";
  googlekey: string = "";

  readonly AdminID: number = 42744; // ClickurTrip = 232 , ideal safari=42744, Cennin =47810, Trivo =127 ,TravelSohere =38612, Tripser = 52129, Hi5Fly=42768
  lat: any = 0;
  lng: any = 0;
  address: string = "";
  Code: string = ""; // ClickUrTrip = CUT, Trivo  = TRIVO, Cennin travels= CENNIN, Excostic = EXCO , MeetMe = MEETME , IDEALJUNGLE = IDEALJ
  UserId: number = 0;
  UserName: string = "";
  Password: string = "";
  Contact: string = "";
  email: string = "";
  website: string = "";
  landline: string = "";
  sUserType: string = "";
  readonly ContactUrl: string =
    "https://trivo.org/Webservices/genral/Contact.asmx/";
  readonly PackageUrl: string =
    "https://trivo.org/Webservices/Packages/PackagesHandler.asmx/";
  readonly ExchnageUrl: string =
    "https://trivo.org/Webservices/genral/ExchnageHandler.asmx/";
  readonly locationUrl: string =
    "https://trivo.org/Webservices/genral/locationHandler.asmx/";
  readonly UserType: string[] = ["B2C"];
  readonly TourUrl: string =
    /*Live*/ "https://trivo.org/Webservices/Activity/Handler/GetActivityHandler.asmx/";
  //  /*Local*/'http://localhost:50079/Webservices/Activity/Handler/GetActivityHandler.asmx/';
  readonly CartUrl: string =
    /*Live*/ "https://trivo.org/Webservices/cart/CartHandler.asmx/";

  readonly HotelUrl: string =
    "https://trivo.org/Webservices/hotel/hotelhandler.asmx/";
  //readonly HotelUrl: string = 'http://localhost:50079/Webservices/hotel/hotelhandler.asmx/'

  //'https://trivo.org/Webservices/Hotel/HotelHandler.asmx/';
  readonly HotelReportUrl: string =
    "https://trivo.org/Webservices/Reporting/HotelReport/HotelReportingHandler.asmx/";

  /* Flight */
  // readonly FlightUrl: string = 'https://trivo.org/Webservices/Flight/Handler/FlightsHandler.asmx/';
  readonly FlightUrl: string =
    "https://trivo.org/Webservices/Flight/Handler/FlightsHandler.asmx/";

  readonly PackageReportUrl: string =
    "https://trivo.org/Webservices/Reporting/Package/PackageReportingHandler.asmx/";

  //'http://clickurtrip.net/Webservices/Hotel/HotelHandler.asmx/';

  readonly AuthorizeUrl: string =
    "https://trivo.org/Webservices/Authorize/service/loginHandler.asmx/";
  readonly EmailUrl: string =
    "https://trivo.org/Webservices/EmailManager/EmailHandler.asmx/";
  readonly MenuAuthUrl: string =
    "https://trivo.org/Webservices/genral/MenuAuthenticateasmx.asmx/";
  readonly AccountUrl: string =
    "https://trivo.org/Webservices/Accounts/AccountHandler.asmx/";
  readonly PayUrl: string =
    "https://trivo.org/Webservices/genral/PaymentGetWaysHandler.asmx/";
  RedirectUrl: string = "";

  constructor() {
    var AdminDetails = this.arrAdminDetails.filter((d) => {
      if (d.AdminID == this.AdminID) {
        this.CompanyName = d.CompanyName;
        this.UserId = d.UserId;
        this.UserName = d.UserName;
        this.Password = d.Password;
        this.domain = d.domain;
        this.RazorpayKey = d.RazorpayKey;
        this.Code = d.Code;
        this.logo = d.logo;
        this.Contact = d.Contact;
        this.email = d.email;
        this.sUserType = d.UserType;
        this.facebookkey = d.facbook;
        this.googlekey = d.google;
        this.address = d.address;
        this.lat = d.lat;
        this.lng = d.lng;
        this.website = d.website;
        this.landline = d.landline;
        this.sUserType = d.UserType;
        this.RedirectUrl = d.RedirectUrl;
      }
    });
  }
}
