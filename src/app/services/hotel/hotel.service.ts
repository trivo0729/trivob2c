import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from '../api-url.service';
import { Observable } from 'rxjs';
import { HotelSearch, Hotels } from 'src/app/modals/hotel/hotel.modal';
import { SessionService } from '../session.service';
declare const google: any;
declare function getInfoBox(item): any;
declare function closeInfoBox(): any;

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  AdminID: number;
  Url: string;
  mapObject: any;
  markersData: any;
  markers: [];
  constructor(private httpClient: HttpClient,
    private ServiceUrl: ApiUrlService,
    private sessionService: SessionService,
  ) {
    this.Url = this.ServiceUrl.HotelUrl;
    this.AdminID = this.ServiceUrl.AdminID;
  }

  getHotelLocation(value: string): Observable<HotelSearch[]> {
    // GetDestinationList
    return this.httpClient.post<HotelSearch[]>(this.Url + "GetDestination",
      {
        //name: value,
        sPlaceName: value,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getHotelsList(): Observable<any> {
    return this.httpClient.get("./assets/json/hotelData.txt");
  }

  getHotels(Search: any): Observable<any> {
    return this.httpClient.post<any>(this.Url + "SearchHotel",
      {
        arrSearch: Search,
        ParentId: this.AdminID
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  CheckHoteSession(TockenID: string): any {
    return this.httpClient.post<any>(this.Url + "CheckHoteSession",
      {
        TockenID: TockenID
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getHotelsfromjson(): Observable<any> {
    return this.httpClient.get("./assets/json/SearchHotelresforDiffOcc.json");
  }

  getratreq() {
    return this.httpClient.get("./assets/json/RateGroupRequest.json");
  }

  getrateBlocks() {
    return this.httpClient.get("./assets/json/BlockRoom.json");
  }

  getobjSearch(): Observable<any> {
    return this.httpClient.get("./assets/json/SearchHotelreq.json");
  }

  getRategroup(arrRateGroup: any): Observable<any> {
    return this.httpClient.post<any>(this.Url + "SearchRateGroup",
      {
        arrRateGroup: arrRateGroup,
        ParentId: this.AdminID,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }


  _blockRoom(arrBlock: any): Observable<any> {
    return this.httpClient.post<any>(this.Url + "BlockRoom",
      {
        arrBlock: arrBlock,
        ParentId: this.AdminID,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  getHotelImages(TokenID: string, ResultIndex: number): Observable<any> {
    return this.httpClient.post<any>(this.Url + "GetHotelImages", {
      TokenID: TokenID,
      ResultIndex: ResultIndex
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) })

  }

  BookingConfirmation(arrConfirm: any): Observable<any> {
    return this.httpClient.post<any>(this.Url + "BookingConfirmation",
      {
        arrConfirm: arrConfirm,
        ParentId: this.AdminID,
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  ConfirmBooking(ConfirmationID: string, TokenID: string, arrParams: any): Observable<any> {
    return this.httpClient.post<any>(this.Url + "ConfirmBooking",
      {
        TokenID: TokenID,
        ConfirmationID: ConfirmationID,
        ParentId: this.AdminID,
        arrParams: arrParams
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }


  BookingCancel(arrCancel: any): Observable<any> {
    return this.httpClient.post<any>(this.Url + "BookingCancel",
      {
        arrCancel: arrCancel
      }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }



  /*Hotel Search Sessoin*/

  setHotelSerachParams(SearchParams: any) {
    try {
      this.sessionService.set("SearchhotelParams", JSON.stringify(SearchParams));
    } catch (e) { }
  }

  setSelectedlocation(address: any) {
    try {
      this.sessionService.set("Sel_location", JSON.stringify(address));
    } catch (e) { }
  }

  getSelectedlocation() {
    return this.sessionService.get("Sel_location");
  }

  getHotelSerachParams() {
    return this.sessionService.get("SearchhotelParams");
  }

  setBlockRoomDetails(Params: any) {
    this.sessionService.set("BlockRoomDetails", JSON.stringify(Params));
  }

  getBlockRoomDetails() {
    return this.sessionService.get("BlockRoomDetails");
  }

  setarrBlock(data: any) {
    this.sessionService.set("arrBlock", JSON.stringify(data));
  }

  getarrBlock() {
    return this.sessionService.get("arrBlock");
  }

  setBooking(data: any) {
    this.sessionService.set("Booking", JSON.stringify(data));
  }

  getBooking() {
    return this.sessionService.get("Booking");
  }


  GetDistance(lat1: any, lon1: any, lat2: any, lon2: any, unit: string) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    }
    else {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      if (dist > 1) {
        dist = 1;
      }
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") { dist = dist * 1.609344 }
      if (unit == "N") { dist = dist * 0.8684 }
      return dist.toFixed(2);
    }
  }


  /*Map For Hotel*/

  loadHotelmap(hotels: Hotels[]) {
    debugger;
    try {
      this.GetHotelMapData(hotels);

      var mapOptions = {
        zoom: 12,
        center: new google.maps.LatLng(hotels[0].Latitude, hotels[0].Langitude),
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
          style: google.maps.ZoomControlStyle.LARGE,
          position: google.maps.ControlPosition.TOP_RIGHT
        },
        scrollwheel: false,
        scaleControl: false,
        scaleControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        streetViewControl: true,
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_TOP
        },
      };
      var
        marker;
      var mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
      //for (var key in this.markersData)
      this.markersData.forEach(function (item) {
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
          map: mapObject,
          icon: '../../../assets/img/pins/Hotels.png',
        });
        google.maps.event.addListener(marker, 'click', (function () {
          closeInfoBox();
          getInfoBox(item).open(mapObject, this);
          mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
        }));
      });

    } catch (e) {
      console.log(e);
    }
  }

  GetHotelMapData(hotels: Hotels[]) {
    try {
      this.markersData = new Array();
      hotels.forEach(hotel => {
        this.markersData.push({
          id: hotel.HotelId,
          name: hotel.HotelName,
          get_directions_start_address: hotel.Address,
          location_latitude: hotel.Latitude,
          location_longitude: hotel.Langitude,
          map_image_url: hotel.Image[0].Url,
          name_point: hotel.HotelName,
          description_point: "",// tour.Description,
          url_point: 'javascript:void();',
          phone: ''
        });
      });
    } catch (e) { }
  }

  Singlemap(hotel: any) {
    this.markersData = new Array();
    this.markersData.push({
      id: hotel.HotelId,
      name: hotel.HotelName,
      get_directions_start_address: hotel.Address,
      location_latitude: hotel.Latitude,
      location_longitude: hotel.Langitude,
      map_image_url: hotel.Image[0].Url,
      name_point: hotel.HotelName,
      description_point: "",// tour.Description,
      url_point: 'javascript:void();',
      phone: ''
    });
    var mapOptions = {
      zoom: 15,
      center: new google.maps.LatLng(hotel.Latitude, hotel.Langitude),
      mapTypeId: google.maps.MapTypeId.ROADMAP,

      mapTypeControl: false,
      mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        position: google.maps.ControlPosition.LEFT_CENTER
      },
      panControl: false,
      panControlOptions: {
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      zoomControl: true,
      zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.TOP_RIGHT
      },
      scrollwheel: false,
      scaleControl: false,
      scaleControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER
      },
      streetViewControl: true,
      streetViewControlOptions: {
        position: google.maps.ControlPosition.LEFT_TOP
      },
    };
    var
      marker;
    var mapObject = new google.maps.Map(document.getElementById('map'), mapOptions);
    //for (var key in this.markersData)
    this.markersData.forEach(function (item) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
        map: mapObject,
        icon: '../../../assets/img/pins/Hotels.png',
      });
      google.maps.event.addListener(marker, 'click', (function () {
        closeInfoBox();
        getInfoBox(item).open(mapObject, this);
        mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
      }));
    });
  }
}
