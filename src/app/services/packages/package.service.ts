import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Package, ArrBooking } from "src/app/modals/packages/package.modal";
import { Observable } from "rxjs";
import { ApiUrlService } from "src/app/services/api-url.service";
import { PackageCities, arrUserDetails } from "src/app/modals/tours.modals";
@Injectable({
  providedIn: "root",
})
export class PackageService {
  AdminID: number;
  Url: string;
  data: ArrBooking;
  arrUserDetails: arrUserDetails;
  constructor(
    private httpClient: HttpClient,
    private ServiceUrl: ApiUrlService
  ) {
    this.Url = this.ServiceUrl.PackageUrl;
    this.AdminID = this.ServiceUrl.AdminID;
    this.arrUserDetails = new arrUserDetails();
    this.arrUserDetails.userName = this.ServiceUrl.UserName;
    this.arrUserDetails.password = this.ServiceUrl.Password;
    this.arrUserDetails.parentID = this.ServiceUrl.AdminID;
  }
  /*Package Location*/
  getPackageLocation(): Observable<PackageCities[]> {
    return this.httpClient.post<PackageCities[]>(
      this.Url + "GetPackageCity",
      {
        AdminID: this.AdminID,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }
  getPackage(arrUserDetails: any): Observable<Package[]> {
    return this.httpClient.post<Package[]>(
      this.Url + "SearchPackages",
      {
        arrUserDetails: arrUserDetails,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  SearchPackagesByType(
    arrUserDetails: any,
    Type: string
  ): Observable<Package[]> {
    return this.httpClient.post<Package[]>(
      this.Url + "SearchPackagesByType",
      {
        arrUserDetails: arrUserDetails,
        Type: Type,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  SearchPackagesByCity(
    City: string,
    arrUserDetails: any
  ): Observable<Package[]> {
    return this.httpClient.post<Package[]>(
      this.Url + "SearchPackagesByCity",
      {
        arrUserDetails: arrUserDetails,
        City: City,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  getPackageDetail(ID: string): Observable<Package> {
    return this.httpClient.post<Package>(
      this.Url + "PackageDetail",
      {
        ID: ID,
        AdminId: this.AdminID,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  GetPackageTemplate(ID: string): Observable<Package> {
    return this.httpClient.post<Package>(
      this.Url + "GetPackageTemplate",
      {
        ID: ID,
        ParentID: this.AdminID,
        arrRequest: new Array(),
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  }

  BookPackage(data: ArrBooking) {
    return this.httpClient.post(
      this.Url + "BookPackage",
      {
        arrBooking: data,
      },
      { headers: new HttpHeaders({ "Content-Type": "application/json" }) }
    );
  } 

  
}
