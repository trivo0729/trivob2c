import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';
import { ApiUrlService } from './api-url.service';
@Injectable({
  providedIn: 'root'
})
export class MetaServicesService {

  constructor(  
    private titleService: Title,
    private meta: Meta,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private objGlobalDefault : ApiUrlService
    ){}
  updateMetaInfo(content, author, category) {
    this.meta.updateTag({ name: 'description', content: content });
    this.meta.updateTag({ name: 'author', content: author });
    this.meta.updateTag({ name: 'keywords', content: category });
}

updateTitle(title?: string) {
  debugger
    if (!title) {
        this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                map(() => this.activatedRoute),
                map((route) => {
                    while (route.firstChild) { route = route.firstChild; }
                    return route;
                }),
                filter((route) => route.outlet === 'primary'),
                mergeMap((route) => route.data)).subscribe((event) => {
                    this.titleService.setTitle(event['title'] + ' | ' + this.objGlobalDefault.CompanyName);
                });
    } else {
        this.titleService.setTitle(title + ' | Trivo IT Solutions Pvt. Ltd.');
    }
}
}
