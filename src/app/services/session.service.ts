import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }

  set(key: string, value: string) {
    localStorage.setItem(key, value)
  }
  get(key: string) {
    try {
      var arrData: any;
      arrData = JSON.parse(localStorage.getItem(key));
      return arrData;
    }
    catch (ex) {

    }
  }
  clear(key: string) {
    try {
      localStorage.removeItem(key);
    } catch (e) { }
  }
}
