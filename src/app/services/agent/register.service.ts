import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from '../api-url.service';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient, private service: ApiUrlService) { }

  OnagentRegistration(arrLogin: any) {
    return this.http.post<any>(this.service.AuthorizeUrl + 'Registration',
      {
        arrLogin: arrLogin
      }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
  }

  UpdateProfile(arrLogin: any) {
    console.log(arrLogin);
    return this.http.post<any>(this.service.AuthorizeUrl + 'UpdateProfile',
      {
        arrLogin: arrLogin
      }, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    })
  }
}
