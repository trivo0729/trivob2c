import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUrlService } from '../api-url.service';
import { SessionService } from '../session.service';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private http: HttpClient, private url: ApiUrlService, private sessionService: SessionService) {

  }

  getAirports(name: string): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'GetCityList', {
      name: name
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  SetFligthSearchParam(SearchParams: any) {
    try {
      this.sessionService.set("SearchFlightParams", JSON.stringify(SearchParams));
    }
    catch (e) {

    }
  }

  getFlightSerachParams() {
    return this.sessionService.get("SearchFlightParams");
  }

  searchFlight(Search: any): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'SearchFlights', {
      objSearch: Search,
      ParentId: this.url.AdminID
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  // searchFlight(Search: any, journeyType: any): Observable<any> {
  //   if (journeyType === "1") {
  //     return this.http.get<any>('./assets/json/oneway-response.json');
  //   } else if (journeyType === "3") {
  //     return this.http.get<any>('./assets/json/multi-city.json');
  //   }

  //   else if (journeyType === "2") {
  //     return this.http.get<any>('./assets/json/roundway-Domflights.json');
  //   }
  // }

  // else if (journeyType === "2") {
  //   return this.http.get<any>('./assets/json/roundway-international.json');
  // }


  getFareRules(ResultIndex: any, TokenId: any): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'GetFareRules', {
      ResultIndex: ResultIndex,
      TokenId: TokenId

    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  GetFareQuote(ResultIndex: any, TokenId: any): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'GetFareQuote', {
      ResultIndex: ResultIndex,
      TokenId: TokenId
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  GetFlightDetails(ResultIndex: any, TokenId: any): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'GetFlights', {
      ResultIndexes: ResultIndex,
      TokenId: TokenId
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
    );
  }

  ConfirmFlights(ResultIndex: any, TotalBaggAmount: any, TotalMealAmount: any, TokenId: any, bCardPayment): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'ConfirmFlights', {
      arrResultIndex: ResultIndex,
      TotalBaggAmount: TotalBaggAmount,
      TotalMealAmount: TotalMealAmount,
      TokenId: TokenId,
      bCardPayment: bCardPayment
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
    );
  }

  BookingFlights(objFlightBooking: any, GstNo: any, GstCompanyName: any, GstCompanyContact: any, GstCompanyAddress: any, GstCompanyEmail: any, TokenId: any, arrParam: any): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'BookingFlights', {
    objFlightBooking: objFlightBooking,
    GstNo: GstNo,
    GstCompanyName: GstCompanyName,
    GstCompanyContact: GstCompanyContact,
    GstCompanyAddress: GstCompanyAddress,
    GstCompanyEmail: GstCompanyEmail,
    TokenId: TokenId,
    arrParams: arrParam
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) }
    );
    }

  getFlightBookingDetails(UserId: any, ReservationID: any): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'FlightBookingDetails', {
      UserId: UserId,
      ReservationID: ReservationID
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }

  CancelTiketedBooking(BookingId): Observable<any> {
    return this.http.post<any>(this.url.FlightUrl + 'CancelTiketedBooking', {
      BookingId: BookingId
    }, { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) });
  }



  SetFligthBookingParams(data: any) {
    try {
      this.sessionService.set("FligthBookingParams", JSON.stringify(data));
    }
    catch (e) {

    }
  }

  getFligthBookingParams() {
    return this.sessionService.get("FligthBookingParams");
  }

  getTicket() {
    return this.http.get("./assets/json/FlightBookingDetails.json");
  }

  getAirlines() {
    return this.http.get("./assets/json/airlines.json");
  }


}
