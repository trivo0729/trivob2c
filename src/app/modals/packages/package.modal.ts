
export class Package {
    Packageid: number;
    PackageName: string;
    City: string;
    noDays: number;
    ValidFrom: any;
    ValidTo: any;
    Itinerary: any;
    Images: Image[];
    Category: Category[];
    Themes: any[];
    Description: string;
    TermsCondition: string;
    MinPrice: any;
    arrInclusions:any[];
    arrExclusions:any[];
    Locations:any[];
}

export class Category {
    CategoryID: any;
    Categoryname: string;
    Single: any;
    ChildPrice: any;
    ExtraAdult: any;
    InfantPrice: any;
    ChildWithBedPrice: any;
    ChildNoBedPrice: any;
    Double: any;
    Triple: any;
    Quad: any;
    Quint: any;
    TotalPrice: any;
    Charges: any;
    CancellationPolicy: string;
    Itinerary: any;
    Inclusions: any[];
    Exclusion: any[];
    PackageHotels: any[];
    ValidFrom: any;
    ValidUpto: any;
    sCurrency: any;
    PackageVehicle?: (PackageVehicle)[] | null;
    PackageActivity?: (PackageActivity)[] | null;
    PackageMeal?: (PackageMeal)[] | null;
    PackageTax?: (PackageTax)[] | null;
    PackageOther?: (PackageMeal)[] | null;
    PackageInclusion: PackageInclusion;
    PackageExclusion: PackageExclusion;
}

export interface PackageVehicle {
    vehicleID: number;
    VehicleName: string;
    VehiclePrice: number;
    Type: string;
    bIncluded: boolean;
}

export interface PackageInclusion {
    PackageVehicle?: (PackageVehicle)[] | null;
    PackageActivity?: (PackageActivity)[] | null;
    PackageMeal?: (PackageMeal)[] | null;
    PackageTax?: (PackageTax)[] | null;
    PackageOther?: (PackageOther)[] | null;
}

export interface PackageExclusion {
    PackageVehicle?: (PackageVehicle)[] | null;
    PackageActivity?: (PackageActivity)[] | null;
    PackageMeal?: (PackageMeal)[] | null;
    PackageTax?: (PackageTax)[] | null;
    PackageOther?: (PackageOther)[] | null;
}

export interface PackageActivity {
    PackageActivityID: number;
    PackageActivityName: string;
    AdultRate: number;
    Child1Rate?: null;
    Child2Rate?: null;
    InfantRate: number;
    bIncluded: boolean;
}

export interface PackageMeal {
    MealID: number;
    MealName: string;
    AdultRate?: null;
    ChildRate?: null;
    bIncluded: boolean;
}
export interface PackageOther {
    Name: string;
    Rate: number;
    Type: boolean;
    bIncluded: boolean;
}

export interface PackageTax {
    Name: string;
    Type: boolean;
    bIncluded: boolean;
    TaxID: number;
    TaxValue: number;
}

export class Image {
    Type: string;
    Url: string;
    Title: string;
    IsDefault: boolean;
    Count: number;
}

export class ArrBooking {
    nID: number;
    RefNo: string;
    PackageID: number;
    CategoryId: number;
    BookingDate: string;
    TravelDate: string;
    StartFrom: string;
    EndDate: string;
    noDays: number;
    Adults: number;
    Childs: number;
    Infant: number;
    AdultPrice: any;
    ChildBedsPrice: any;
    ChildWBed: any;
    LeadingPax: string;
    email: string;
    PhoneNumber: string;
    Nationality: string;
    Type: string;
    AddOn: string;
    ChildAges: string;
    uid: number;
    Status: string;
    ParentId: number;
    B2CId: string;
    CabName: string;
    CabID: number;
}

export class Themes {
    Count: number;
    Name: string;
    checked: boolean;
}
export class Categories {
    Count: number;
    Name: string;
    checked: boolean;
}

export class PackageTheme {
    Count: number;
    Name: string;
    checked: boolean;
    Package: Package[];
    PackageCount: number;
}

