export class PakageReport {
    Supplier?: null;
    TravelDate: string;
    PaxName: string;
    PackageName: string;
    PackageType?: null;
    CatID: string;
    Location: string;
    StartDate: string;
    EndDate: string;
    BookingDate?: null;
    Status: string;
    BookingId: number;
    RefrenceNo?: null;
    Adults: number;
    Child: number;
    ChildAges: string;
    Infant: number;
}
