export class BookingRequest {
    objFlightBooking: ObjFlightBooking;
    GstNo: string;
    GstCompanyName: string;
    GstCompanyContact: string;
    GstCompanyAddress: string;
    GstCompanyEmail: string;
    TokenId: string;
    arrParams?: [];
}

export class ObjFlightBooking {
    ResultIndexes?: (string)[] | null;
    ResultIndex: string;
    Passengers?: Passengers[];
    EndUserIp: string;
    TokenId: string;
    TraceId: string;
}

export class Passengers {
    Title: string;
    FirstName: string;
    LastName: string;
    PaxType: number;
    DateOfBirth: string;
    Gender: number;
    PassportNo: string;
    PassportExpiry: string;
    AddressLine1: string;
    AddressLine2: string;
    Fare: Fare;
    City: string;
    CountryCode: string;
    CountryName: string;
    Nationality: string;
    ContactNo: string;
    Email: string;
    IsLeadPax: boolean;
    FfAirlineCode: string;
    FfNumber: string;
    Baggage?: (null)[] | null;
    MealDynamic?: (null)[] | null;
    Meal?: any;
    GstCompanyAddress: string;
    GstCompanyContactNumber: string;
    GstCompanyName?: any;
    GstNumber?: any;
    GstCompanyEmail?: any;
}

export class Fare {
    Currency: string;
    BaseFare: any
    Tax: any;
    YqTax: any;
    AdditionalTxnFeePub: any;
    AdditionalTxnFeeOfrd: any;
    OtherCharges: any;
    Discount: any;
    PublishedFare: any;
    OfferedFare: any;
    TdsOnCommission: any;
    TdsOnPlb: any;
    TdsOnIncentive: any;
    ServiceFee: any;
}