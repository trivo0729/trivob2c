export class bookingReport {
    Sid: number;
    ReservationID: string;
    ReferenceCode?: string | null;
    GTAId?: string | null;
    Uid: number;
    HotelCode: string;
    HotelName: string;
    City: string;
    RoomCode: string;
    CheckIn: string;
    CheckOut: string;
    NoOfDays: number;
    TotalRooms: number;
    TotalFare: number;
    RoomRate?: number | null;
    SalesTax: number;
    Servicecharge: number;
    NoOfAdults: number;
    Children: number;
    CancelDate: string;
    ReservationDate: string;
    ReservationTime?: string | null;
    Status: string;
    BookingStatus: string;
    AgencyName: string;
    bookingname: string;
    DeadLine: string;
    AffilateCode: string;
    AgentRef: string;
    VoucherID: string;
    InvoiceID: string;
    LatitudeMGH: string;
    LongitudeMGH: string;
    Source: string;
    HoldTime: string;
}


export class Invoice {
    retCode: number;
    arrResrvation: ArrResrvation;
    AdminUser: AdminUser;
    AgentDetails: AgentDetails;
    arrPaxes: Guestlist[];
    arrRooms: Roomlist[];
    dtHotelAdd: HotelDetails;
    arrCurency: string;
    CancellationPolicy: string[];
    RateBreakups: RateBreakups[];
}

export class ArrResrvation {
    customer_Email: string;
    customer_name: string;
    VoucherNo: string;
    HotelConfirm?: null;
    Uid: number;
    Email: string;
    ContactPerson: string;
    CurrencyCode: string;
    SupplierName: string;
    SupplierMail: string;
    httplogo: string;
    SupplierID: string;
    HotelCode: string;
    HotelName: string;
    City: string;
    checkin: string;
    checkout: string;
    CancelDate?: null;
    Ammount: string;
    Nights: string;
    Passenger: string;
    InvoiceID: string;
    VoucherID: string;
    Bookingdate: string;
    Status: string;
    Type: string;
}
export class AdminUser {
    sid: number;
    adminID: number;
    Name: string;
    Code: string;
    Address: string;
    email: string;
    Mobile: string;
    phone: string;
    PinCode: string;
    Fax: string;
    sCountry?: null;
    Website: string;
    StateID: string;
    Countryname: string;
    City: string;
    ContactPerson?: null;
    PassWord?: null;
    Currency?: null;
}
export class AgentDetails {
    sid?: null;
    adminID: number;
    Name: string;
    Code: string;
    Address: string;
    email: string;
    Mobile: string;
    phone: string;
    PinCode: string;
    Fax: string;
    sCountry?: null;
    Website: string;
    StateID: string;
    Countryname: string;
    City: string;
    ContactPerson: string;
    PassWord: string;
    Currency: string;
}
export class Guestlist {
    sid: number;
    ReservationID: string;
    RoomCode: string;
    RoomNumber: string;
    Name: string;
    LastName: string;
    Age: number;
    PassengerType: string;
    IsLeading: boolean;
}
export class Roomlist {
    sid: number;
    ReservationID: string;
    RoomCode: string;
    RoomType: string;
    RoomNumber: string;
    BoardText: string;
    MealPlanID: string;
    TotalRooms: number;
    LeadingGuest: string;
    Adults: number;
    Child: number;
    ChildAge?: null;
    Remark: string;
    CutCancellationDate: string;
    SupplierNoChargeDate: string;
    CancellationAmount: string;
    CanServiceTax: string;
    CanAmtWithTax: string;
    RoomAmount: number;
    RoomServiceTax?: null;
    RoomAmtWithTax: number;
    SupplierCurrency?: null;
    ExchangeRate?: null;
    SupplierAmount: number;
    TaxDetails: string;
    FranchiseeTaxDetails: string;
    FranchiseeMarkup: number;
    TDS: number;
    Discount?: null;
    Commision: number;
    EssentialInfo: string;
}

export class RateBreakups {
    Date: string;
    Amount: any;
}


export class HotelDetails {
    Address: string;
    HotelName: string;
    Langitude: string;
    Latitude: string;
    Category: string;
    ContactNo?: any;
    PostalCode: string;
}



