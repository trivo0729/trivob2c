export class Category {
    Count: number;
    HotelCode: any;
    Name: string;
    Supplier: string;
    Checked: boolean;
}

export class Facility {
    Count: number;
    Name: string;
    Priority: any;
    icon: string;
    toolip: string;
    Checked: boolean;
}
