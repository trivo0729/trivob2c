export class Menulist {
    ID: number;
    Name: string;
    Path: string;
    priority: any;
    ChildItem: ChildItem[];
}

export interface ChildItem {
    ID: number;
    Name: string;
    Path: string;
    ChildItem: any[];
}